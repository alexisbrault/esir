# ESIR #

This repository contains a part of my pratical work done during my 3-years-IT studies at ESIR (engineering school).

## ESIR1 ##
### Semester 5 ###
* Programming (Java)
### Semester 6 ###
* Graph Algorithms (Python)
* Algorihtms and complexity (Java, Genetic algos, optimizing)
* Programming (C++)
## ESIR2 ##
### Semester 7 ###
* Proof programming (Isabelle, lemma)
* Data Mining (Java, Naives Bayes, learning algorithms)
* [C# Compiler](https://github.com/Kapcash/Compilation) (XText, Java, AST, Optimization)
* Network Introduction 
### Semester 8 ###
* [Nuage Magique](https://bitbucket.org/alexisbrault/le-nuage-magique) (Javascript : Angular4, NodeJS, GoogleAPI)
* [News Checker](https://bitbucket.org/YoannBoyere/projet_si) (NLP, NodeJS, MongoDB, Angular4)
* [Parallel program synchronization](https://bitbucket.org/alexisbrault/tp_s8_esir) (Barrier, Threads, Image processessing)
* [Advanced Database](https://bitbucket.org/alexisbrault/tp_s8_esir) (MongoDB, NOSQL)
* [Text Editor](https://bitbucket.org/alexisbrault/projet_mdi_s8) (UML, Java, Swing)
* [Front end technologies](https://bitbucket.org/alexisbrault/tp_s8_esir) (Angular4, TypeScript)
## ESIR3 ##
### Semester 9 ###
* [Big Data Algortihms] (Spark, Python, MapReduce, Clustering)  