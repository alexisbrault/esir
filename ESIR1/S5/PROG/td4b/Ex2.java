package td4b;

import types.ArbreBinaire;
import types.ArbreBinaireSimple;
import types.Couple;

public class Ex2 {
	
	static int cons(ArbreBinaire<Integer> a,int[] elem,char[] types,int indice){
		a.setValeur(elem[indice]);
		char type=types[indice];
		indice++;
		if(type=='n'|| type=='g')
			cons(a.filsGauche(),elem,type,indice);
	}

	static <T> ArbreBinaire<T> clone(ArbreBinaire<T> a){
		ArbreBinaire<T> result = new ArbreBinaireSimple<T>();
		if(a.estVide())
			return result;
		result.setValeur(a.getValeur());
		result.setGauche(clone(a.filsGauche()));
		result.setDroit(clone(a.filsDroit()));
		return result;
	}

	static double calcul(ArbreBinaire<String> a,ArbreBinaire<Couple<String,Double>> var){
		if(a.estFeuille())
			return evaluer(var,a.getValeur());
		double d1=calcul(a.filsGauche(),var);
		double d2=calcul(a.filsDroit(),var);
		switch (a.getValeur()) {
		case "+":return d1+d2;
		case "-":return d1-d2;
		case "*":return d1*d2;
		case "/":return d1/d2;
		default :return -1;
		}
	}

	private static double evaluer(ArbreBinaire<Couple<String, Double>> var,String valeur) {
		int cmp=valeur.compareTo(var.getValeur().premier());
		if(cmp==0) return var.getValeur().deuxieme();
		if(cmp<0) return evaluer(var.filsGauche(), valeur);
		return evaluer(var.filsDroit(), valeur);
	}

	static String aff (ArbreBinaire<String> a){
		if(a.estFeuille())
			return a.getValeur();
		return "("+aff(a.filsGauche())+" "+a.getValeur()+" "+aff(a.filsDroit())+")";
	}

	static String aff2 (ArbreBinaire<String> a){
		if(a.estFeuille())
			return a.getValeur();
		return "("+a.getValeur()+" "+aff2(a.filsGauche())+" "+aff2(a.filsDroit())+")";
	}

	public static void main(String[] args) {
		ArbreBinaire<String> racine = new ArbreBinaireSimple<String>();
		racine.setValeur("/");
		ArbreBinaire<String> sag = new ArbreBinaireSimple<String>();
		sag.setValeur("-");
		ArbreBinaire<String> sagsag = new ArbreBinaireSimple<String>();
		sagsag.setValeur("+");
		ArbreBinaire<String> sagsagsag = new ArbreBinaireSimple<String>();
		sagsagsag.setValeur("10");//a
		ArbreBinaire<String> sagsagsad = new ArbreBinaireSimple<String>();
		sagsagsad.setValeur("2");//b
		ArbreBinaire<String> sagsad = new ArbreBinaireSimple<String>();
		sagsad.setValeur("3");//c

		ArbreBinaire<String> sad = new ArbreBinaireSimple<String>();
		sad.setValeur("*");
		ArbreBinaire<String> sadsag = new ArbreBinaireSimple<String>();
		sadsag.setValeur("-");
		ArbreBinaire<String> sadsagsag = new ArbreBinaireSimple<String>();
		sadsagsag.setValeur("2");//d
		ArbreBinaire<String> sadsagsad = new ArbreBinaireSimple<String>();
		sadsagsad.setValeur("3");//c
		ArbreBinaire<String> sadsad = new ArbreBinaireSimple<String>();
		sadsad.setValeur("4");//e

		racine.setGauche(sag);
		racine.setDroit(sad);
		sag.setGauche(sagsag);
		sag.setDroit(sagsad);
		sagsag.setGauche(sagsagsag);
		sagsag.setDroit(sagsagsad);
		sad.setGauche(sadsag);
		sad.setDroit(sadsad);
		sadsag.setGauche(sadsagsag);
		sadsag.setDroit(sadsagsad);

		//System.out.println(calcul(racine));
		System.out.println(aff(racine));
		System.out.println(clone(racine));
	}

}
