package td4b;

import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;
import types.ArbreBinaire;
import types.ArbreBinaireSimple;

public class Ex1 {

	static <T> int nbElements(ArbreBinaire<T> arbre){
		if(arbre.estVide())
			return 0;
		return 1+nbElements(arbre.filsGauche())+nbElements(arbre.filsDroit());
	}

	static <T> String parcoursInfixe(ArbreBinaire<T> arbre){
		return parcoursInfixe(arbre,0);
	}

	static <T> String parcoursInfixe(ArbreBinaire<T> arbre,int profondeur){
		if(arbre.estVide())
			return "";
		profondeur++;
		return parcoursInfixe(arbre.filsGauche(),profondeur)+" "+(profondeur-1)+" "+arbre.getValeur()+" "+parcoursInfixe(arbre.filsDroit(),profondeur);
	}
	
	static <T> String parcoursPostfixe(ArbreBinaire<T> arbre){
		return parcoursPostfixe(arbre,0);
	}

	static <T> String parcoursPostfixe(ArbreBinaire<T> arbre,int profondeur){
		if(arbre.estVide())
			return "";
		profondeur++;
		return parcoursPostfixe(arbre.filsGauche(),profondeur)+" "+parcoursPostfixe(arbre.filsDroit(),profondeur)+" "+(profondeur-1)+" "+arbre.getValeur();
	}

	static <T> void listFeuilles(ArbreBinaire<T> a, List<T> res){
		if(a.estVide())
			return;
		if(a.estFeuille()){
			res.add(a.getValeur());
		}else{
			if(a.existeGauche())
				listFeuilles(a.filsGauche(), res);
			if(a.existeDroit())
				listFeuilles(a.filsDroit(), res);
		}
	}

	static <T extends Comparable<T>> T max(ArbreBinaire<T> a){
		Assert.assertFalse("Erreur : arbre vide " ,a.estVide());

		T max = a.getValeur();
		
		if(a.existeGauche())
			max = valMax(max,max(a.filsGauche()));
		if(a.existeDroit())
			max = valMax(max,max(a.filsDroit()));
		return max;
	}

	static <T extends Comparable<T>> T valMax(T v1, T v2){
		if(v1.compareTo(v2)>=0)
			return v1;
		return v2;
	}
	
	static <T extends Comparable<T>> ArbreBinaire<T> max2(ArbreBinaire<T> a){
		Assert.assertFalse("Erreur : arbre vide " ,a.estVide());

		ArbreBinaire<T> max = a;
		
		if(a.existeGauche())
			max = valMax(max,max2(a.filsGauche()));
		if(a.existeDroit())
			max = valMax(max,max2(a.filsDroit()));
		return max;
	}

	static <T extends Comparable<T>> ArbreBinaire<T> valMax(ArbreBinaire<T> v1, ArbreBinaire<T> v2){
		if(v1.getValeur().compareTo(v2.getValeur())>=0)
			return v1;
		return v2;
	}


	public static void main(String[] args) {
		//		ArbreBinaire<String> racine = new ArbreBinaireSimple<String>();
		//		racine.setValeur("Clavel");
		//		ArbreBinaire<String> Asimov = new ArbreBinaireSimple<String>();
		//		Asimov.setValeur("Asimov");
		//		ArbreBinaire<String> Buzzati = new ArbreBinaireSimple<String>();
		//		Buzzati.setValeur("Buzzati");
		//		ArbreBinaire<String> Poe = new ArbreBinaireSimple<String>();
		//		Poe.setValeur("Poe");
		//		
		//		ArbreBinaire<String> Irvin = new ArbreBinaireSimple<String>();
		//		Irvin.setValeur("Irvin");
		//		ArbreBinaire<String> VanVogt = new ArbreBinaireSimple<String>();
		//		VanVogt.setValeur("VanVogt");
		//		
		//		Asimov.setGauche(Buzzati);
		//		Asimov.setDroit(Poe);
		//		
		//		Irvin.setDroit(VanVogt);
		//		
		//		racine.setGauche(Asimov);
		//		racine.setDroit(Irvin);

		ArbreBinaire<String> racine = new ArbreBinaireSimple<String>();
		racine.setValeur("Calvel");
		ArbreBinaire<String> Asimov = new ArbreBinaireSimple<String>();
		Asimov.setValeur("Asimov");
		ArbreBinaire<String> Buzzati = new ArbreBinaireSimple<String>();
		Buzzati.setValeur("Buzzati");
		ArbreBinaire<String> Poe = new ArbreBinaireSimple<String>();
		Poe.setValeur("Poe");

		ArbreBinaire<String> Irvin = new ArbreBinaireSimple<String>();
		Irvin.setValeur("Irvin");
		ArbreBinaire<String> VanVogt = new ArbreBinaireSimple<String>();
		VanVogt.setValeur("VanVogt");

		Asimov.setGauche(Buzzati);
		Asimov.setDroit(Poe);

		Irvin.setDroit(VanVogt);

		racine.setGauche(Asimov);
		racine.setDroit(Irvin);

		System.out.println("Nb elements : "+nbElements(racine));
		System.out.println(parcoursPostfixe(racine));
		System.out.println(max(racine));
		LinkedList l = new LinkedList();
		listFeuilles(racine, l);
		System.out.println(l);
	}

}
