package tp3;

import java.util.Random;

import types.Tableau;

public class NombresPremiers {

	public static boolean estPremier(int n, Tableau<Integer> nombresPremiers) {
		boolean result = true;
		if (n < 0) {
			result = false;
		} else if (n != 0 && n != 1) {
			for (int i = 2; i <= n/2; i++) {
				if (n != i && n % i == 0) {
					result = false;
					break;
				}
			}
		}
		return result;
	}
	public static int calculerNombresPremiers(int N, Tableau<Integer> nombresPremiers) {
		int n;
		nombresPremiers.push_back(2);
		for(n=3;n<N;n++)
			if((n%2!=0) && (estPremier(n, nombresPremiers))){
				nombresPremiers.push_back(n);
			}
		return n;
	}
	public static Tableau<Integer> remplirHasard(int nb) {
		Tableau<Integer> tableau = new Block<Integer>(nb);
		Random r = new Random();
		for(int i=0;i<nb;i++){
			int alea = r.nextInt(nb);
			tableau.push_back(alea);
		}
		return tableau;
	}
	
	public static int eliminerPresents(Tableau<Integer> t,Tableau<Integer> nombresPremiers) {
		int nombreElementEnlever = 0;
		for(int i =0; i<t.size(); i++){
			if(rechercheDicho(nombresPremiers, t.get(i))){
				t.set(i, -1);
				nombreElementEnlever++;
			}
		}

		if(t.size() == nombreElementEnlever){
			int size = t.size();
			for(int i =0; i<size; i++){
				t.pop_back();
			}
			return nombreElementEnlever;
		}
		
		Tableau<Integer> temporaire = new Block<Integer>(t.size()-nombreElementEnlever);
		for(int i =0; i<nombreElementEnlever; i++){
			if(t.get(i)==-1)
				continue;
			temporaire.push_back(t.get(i));
		}
		
		for(int i=0; i<nombreElementEnlever; i++)
			t.pop_back();
				
		int size = temporaire.size();

		for(int i=0; i<size; i++){
			t.set(i, temporaire.get(i));
		}
		
		return nombreElementEnlever;
	}
	
	public static boolean rechercheDicho(Tableau<Integer> tnb,int val){
		try{
			int debut = 0;
			int fin = tnb.size()-1;
			while (debut <= fin) {
				int milieu =( debut + fin)/2;
				if (tnb.get(milieu)< val)   debut = milieu + 1;
				else if (tnb.get(milieu)> val) fin = milieu - 1;
				else return true; // trouv�
			}
			return false; // pas trouv�.

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return false;
	}

}
