package tp3;

import junit.framework.Assert;
import types.Array;
import types.Tableau;

public abstract class TableauAbstrait<T> implements Tableau<T> {

	private int taille;
	private Array<T> tableau;

	public TableauAbstrait(int capacite) {
		Assert.assertTrue("Erreur : capacit� nulle " ,capacite!=0);
		this.taille=0;
		tableau = new Array<T>(capacite);
	}

	@Override
	public int size() {
		return taille;
	}

	@Override
	public boolean empty() {
		return taille==0;
	}

	@Override
	public boolean full() {
		return taille==tableau.length();
	}

	@Override
	public T get(int i) {
		Assert.assertFalse("Erreur : get, index trop grand", i>taille);
		Assert.assertFalse("Erreur : get, index trop petit", i<0);
		return tableau.get(i);
	}

	@Override
	public void set(int i, T v) {
		Assert.assertFalse("Erreur : set, index trop grand", i>taille);
		Assert.assertFalse("Erreur : set, index trop petit", i<0);
		tableau.set(i, v);
	}

	@Override
	public void push_back(T x) {
		Assert.assertFalse("Erreur : push_back, block plein", full());
		tableau.set(taille, x);
		taille++;
		
	}

	@Override
	public void pop_back() {
		Assert.assertFalse("Erreur : pop_back, block vide", empty());
		tableau.get(--taille);
	}
	
	//Getter Setter

	protected int getTaille() {
		return taille;
	}

	protected void setTaille(int taille) {
		this.taille = taille;
	}

	protected Array<T> getTableau() {
		return tableau;
	}

	protected void setTableau(Array<T> tableau) {
		this.tableau = tableau;
	}

}
