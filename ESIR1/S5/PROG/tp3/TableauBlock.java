package tp3;

import junit.framework.Assert;
import types.Tableau;

public class TableauBlock<T> implements Tableau<T> {

	private int capaciteBlock;
	private int taille;
	private Tableau2x<Block<T>> tableau;

	public TableauBlock(int capacite,int capaciteBlock) {
		Assert.assertTrue("Erreur : capacit� nulle " ,capacite!=0);
		this.capaciteBlock=capaciteBlock;
		this.taille=0;
		tableau = new Tableau2x<Block<T>>(capacite);
		for(int i=0;i<capacite;i++){
			Block<T> block = new Block<T>(capaciteBlock);
			tableau.push_back(block);
		}
	}

	public TableauBlock(int capacite) {
		this(capacite,128);
	}

	@Override
	public int size() {
		return taille;
	}

	@Override
	public boolean empty() {
		return taille==0;
	}

	@Override
	public boolean full() {
		return false;
		//		return taille==tableau.size()*capaciteBlock;
	}

	@Override
	public T get(int i) {
		Assert.assertFalse("Erreur : get, index trop grand", i>taille);
		Assert.assertFalse("Erreur : get, index trop petit", i<0);
		return tableau.get(i/capaciteBlock).get(i%capaciteBlock);
	}

	@Override
	public void set(int i, T v) {
		Assert.assertFalse("Erreur : set, index trop grand", i>taille);
		Assert.assertFalse("Erreur : set, index trop petit", i<0);
		tableau.get(i/capaciteBlock).set(i%capaciteBlock, v);
	}

	@Override
	public void push_back(T x) {
		if(taille+1==tableau.size()*capaciteBlock){
			tableau.push_back(new Block<T>(capaciteBlock));
		}
		tableau.get(taille/capaciteBlock).push_back(x);
		taille++;
	}

	@Override
	public void pop_back() {
		Assert.assertFalse("Erreur : pop_back, block vide", empty());
		tableau.get(--taille/capaciteBlock).pop_back();
	}

}
