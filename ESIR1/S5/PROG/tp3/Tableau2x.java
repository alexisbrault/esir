package tp3;

import types.Array;

public class Tableau2x<T> extends TableauAbstrait<T> {

	public Tableau2x(int capacite) {
		super(capacite);
	}
	
	@Override
	public boolean full() {
		return false;
	}
	
	@Override
	public void push_back(T x) {
		if(super.getTaille()+1==super.getTableau().length()){
			Array<T> tableau = super.getTableau();
			Array<T> nouveauTableau = new Array<T>(tableau.length()*2);
			//On duplique
			for(int i=0;i<tableau.length();i++)
				nouveauTableau.set(i, tableau.get(i));
			super.setTableau(nouveauTableau);
		}
		super.push_back(x);
	}

}
