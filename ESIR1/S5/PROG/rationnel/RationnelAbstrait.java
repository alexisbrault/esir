package rationnel;

import types.Rationnel;

public abstract class RationnelAbstrait implements Rationnel{

	@Override
	public boolean equals(Rationnel r) {
		return getNumerateur()==r.getNumerateur() && getDenominateur()==r.getDenominateur();
	}

	@Override
	public Rationnel somme(Rationnel r) {
		return new RationnelSimple(r.getNumerateur()*getDenominateur()+getNumerateur()*r.getDenominateur(),r.getDenominateur()* getDenominateur());
	}

	@Override
	public Rationnel inverse() {
		assert getNumerateur()!=0 : "Erreur numerateur";
		return new RationnelSimple(getDenominateur(),getNumerateur());
	}

	@Override
	public double valeur() {
		return ((double)getNumerateur())/((double)getDenominateur());
	}

	@Override
	public abstract int getNumerateur();

	@Override
	public abstract int getDenominateur();

	@Override
	public int compareTo(Rationnel autre) {
		return ((Double) valeur()).compareTo((Double) autre.valeur());
	}

	public String toString()
	{
		return getNumerateur()+"/"+getDenominateur();
	}
	
	protected static long pgcd(long a, long b) {
	    return b == 0 ? a : pgcd(b, a % b); 
	}
	
}
