package rationnel;

import types.Rationnel;

public class RationnelSimple extends RationnelAbstrait{

	private int denominateur;
	private int numerateur;

	public RationnelSimple(int numerateur, int denominateur) {
		assert denominateur!=0 : "Erreur denominateur";

		this.denominateur = denominateur;
		this.numerateur = numerateur;
		long pgcd = pgcd(getNumerateur(), getDenominateur());
		
		this.denominateur = (int) (getDenominateur()/pgcd);
		this.numerateur = (int) (getNumerateur()/pgcd);
	}
	
	public RationnelSimple(Rationnel r) {
		this(r.getNumerateur(),r.getDenominateur());
	}

	public RationnelSimple(int a) {
		this(a,1);
	}

	@Override
	public int getNumerateur() {
		return numerateur;
	}

	@Override
	public int getDenominateur() {
		return denominateur;
	}

}
