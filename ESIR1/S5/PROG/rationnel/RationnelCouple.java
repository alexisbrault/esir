package rationnel;

import types.Rationnel;

public class RationnelCouple extends RationnelAbstrait{

	private Couple<Integer, Integer> rationnel;

	public RationnelCouple(int numerateur, int denominateur) {
		assert (denominateur!=0) : "Erreur denominateur";
		rationnel = new Couple<Integer, Integer>(numerateur, denominateur);

		long pgcd = pgcd(getNumerateur(), getDenominateur());
		rationnel.setFirst((int) (getNumerateur()/pgcd));
		rationnel.setSecond((int) (getDenominateur()/pgcd));
	}
	
	public RationnelCouple(int x) {
		this(x, 1);
	}
	
	public RationnelCouple(Rationnel rat) {
		this(rat.getNumerateur(), rat.getDenominateur());
	}

	@Override
	public int getNumerateur() {
		return rationnel.getFirst();
	}

	@Override
	public int getDenominateur() {
		return rationnel.getSecond();
	}

}

