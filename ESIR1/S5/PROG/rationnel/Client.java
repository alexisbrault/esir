package rationnel;

import java.util.Scanner;

import types.Rationnel;

public class Client {

	private static void affichage(Rationnel lastRationnel, Rationnel theRationnel) {
		String rationnel = theRationnel.toString();
		String somme = (theRationnel.somme(lastRationnel)).toString();
		String inverse = theRationnel.inverse().toString();
		double valeur = theRationnel.valeur();
		int compare = theRationnel.compareTo(lastRationnel);
		boolean equal = theRationnel.equals(lastRationnel);

		String compareTo = null;

		if(compare==1)compareTo=">";
		else if(compare==-1)compareTo="<";
		else if(compare==0)compareTo="=";

		String egale = null;

		if(equal)egale = "=";
		else if(!equal)egale = "!=";

		System.out.println(
						"courant = "+rationnel+"; "+rationnel+" + "+lastRationnel+" = "
						+somme+"; inverse = "+inverse+"; valeur : "+valeur+"; "+rationnel+ " "
						+compareTo+" "+lastRationnel+"; "+rationnel+" "+egale+" "+lastRationnel
		);

	}

	private Rationnel lireRationnel(Scanner input, Rationnel[] tab){
		System.out.println("Entrez a puis b tel que (a/b):");
		int n = input.nextInt();
		int d = input.nextInt();
		if(n==0 || d==0)
			return null;
		return makeRationnel(n,d, tab);
	}

	private Rationnel makeRationnel(int n, int d, Rationnel[] tab){
		if(n%2==0)
		{
			Rationnel ret = new RationnelCouple(n, d);
			insererRationnel(ret, tab, tab.length);
			return ret;
		}
		else
		{
			RationnelSimple ret = new RationnelSimple(n, d);
			insererRationnel(ret, tab, tab.length);
			return ret;
		}
	} 

	public static void affichage(int nb,Rationnel[] tabRationnelSimples,int taille)
	{
		assert nb>0 && nb<taille : "Le nombre est soit égale à 0 soit plus grand que la taille de l'arrayList";

		for(int i=0; i<nb; i++)
		{
			System.out.println(tabRationnelSimples[i]);
		}
	}

	public static void insererRationnel(Rationnel nouveau,Rationnel[] tabRationnelSimples,int taille)
	{
		int i =0;
		Rationnel[] result = new Rationnel[taille+1];
		while(i<taille && tabRationnelSimples[i].compareTo(nouveau)==-1)
		{
			result[i]=tabRationnelSimples[i];
			i++;
		}
		
		result[i]=nouveau;
		
		while(i<taille)
		{
			result[i+1]=tabRationnelSimples[i];
			i++;
		}
		//Clone
		for(int j=0;j<result.length;j++)
			tabRationnelSimples[j]=result[j];
	}

	public Rationnel sommeRationnel(int nb, Rationnel[] tabRationnelSimples, int taille)
	{
		assert nb>0 && nb<=taille : "Le nombre est soit égale à 0 soit plus grand que la taille du tableau";
		Rationnel r = tabRationnelSimples[0];
		for(int i=1; i<nb; i++)
		{
			r=r.somme(tabRationnelSimples[i]);
		}
		
		return r;
	}



}
