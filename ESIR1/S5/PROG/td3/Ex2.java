package td3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Ex2 {

	public Ex2() {
		List<Integer> list1 = new LinkedList<Integer>();
		
		list1.add(3);
		list1.add(0);
		
		List<Integer> list2 = new LinkedList<Integer>();
		list2.add(9);
		list2.add(2);
		list2.add(9);
		list2.add(0);
		
		System.out.println(list1);
		System.out.println(list2);
		System.out.println(sum(list1,list2));
	}
	
	private boolean isDivisibleBy3(List<Integer> list){
		return isDivisibleBy(sumList(list),3);
	}
	
	private boolean isDivisibleBy(int a,int b){
		return a%b==0;
	}
	
	private int sumList(List<Integer> list){
		Iterator<Integer> it = list.iterator();
		int sum=0;
		while(it.hasNext())
			sum+=it.next();
		return sum;
	}
	
	private boolean areEquals(List<Integer> list1,List<Integer> list2){
		Iterator<Integer> it1 = list1.iterator();
		Iterator<Integer> it2 = list2.iterator();
		
		while(it1.hasNext() && it2.hasNext()){
			if(it1.next()!=it2.next())
				return false;
		}
		return !it1.hasNext() && !it2.hasNext();
	}
	
	private LinkedList<Integer> sum(List<Integer> list1,List<Integer> list2){
		ListIterator<Integer> it1 = list1.listIterator();
		ListIterator<Integer> it2 = list2.listIterator();
		
		LinkedList<Integer> result = new LinkedList<Integer>();
		
		int r=0,c=0;
		
		//on se place � la fin
		while(it1.hasNext() || it2.hasNext()){
			if(it1.hasNext())
				it1.next();
			if(it2.hasNext())
				it2.next();
		}	
		
		while(it1.hasPrevious() || it2.hasPrevious()){
			int x=0,y=0;
			if(it1.hasPrevious())
				x=it1.previous();
			if(it2.hasPrevious())
				y=it2.previous();
			
			c=r+x+y;
			r = (int) (x+y)/10;
			result.addFirst(c-r*10);
			
		}	
		if(r>0)
			result.addFirst(r);
		return result;
	}
	
	public static void main(String[] args) {
		new Ex2();
	}

}
