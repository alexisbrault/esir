package td3;

import java.util.List;

public class Maladie {
	private String nom;
	private List<Integer> symptomes;

	public Maladie(String nom, List<Integer> symptomes) {
		this.nom = nom;
		this.symptomes = symptomes;
	}

	public String getNom() {
		return nom;
	}

	public List<Integer> getSymptomes() {
		return symptomes;
	}
}
