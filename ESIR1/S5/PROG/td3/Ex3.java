package td3;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Ex3 {
	
	public Ex3() {
		List<Integer> symptomes1 = new LinkedList<Integer>();
		Maladie m1 = new Maladie("Maladie 1", symptomes1);
	}
	
	static List<Maladie> maximiser(List<Maladie> ensemble, List<Integer> symptomes){
		List<Maladie> result = new LinkedList<Maladie>();
		
		int max=0;
		Iterator<Maladie> it = ensemble.iterator();
		while(it.hasNext()){
			Maladie m = it.next();
			int maxTemp = ressemblance(m.getSymptomes(), symptomes);
			if(maxTemp>max){
				max=maxTemp;
				result.clear();
			}	
			if(maxTemp==max){				
				result.add(m);
			}
				
		}
		return result;
	}
	
	static int ressemblance(List<Integer> s1, List<Integer> s2){
		ListIterator<Integer> it1 = s1.listIterator(), it2 = s2.listIterator();
		int r = 0;
		while(it1.hasNext() && it2.hasNext()){
			int u1 = it1.next(), u2 = it2.next();
			if(u1 < u2){
				it2.previous();
			}
			else if (u1 > u2){
				it1.previous();
			}
			else r++;
		}
		
		return r;
	}
	
	
	
	public static void main(String[] args) {
		new Ex3();
	}
}
