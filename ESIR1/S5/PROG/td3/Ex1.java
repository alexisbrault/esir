package td3;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Ex1 {

	public Ex1() {
		List<Integer> list = new LinkedList<Integer>();
		list.add(1);
		list.add(2);
		list.add(4);
		list.add(5);
		list.add(5);
		list.add(5);
		
		System.out.println(list);
		//deleteMultiple(list,2);
		list = deleteDuplicates(list);
		System.out.println(list);
	}

	private void deleteMultiple(List<Integer> list,int number){
		Iterator<Integer> it = list.iterator();
		while (it.hasNext()) {
			if(it.next()%2==0)
				it.remove();
		}
	}
	
	private <T> List<T> deleteDuplicates(List<T> list){
		
		List<T> result = new LinkedList<T>();
		Iterator<T> it = list.iterator();
		Set<T> set = new HashSet<T>();
		
		while (it.hasNext()) {
			T i = it.next();
			if(set.add(i))
				result.add(i);
		}
		
		return result;
	}
	
	private <T extends Comparable<T>> void insertionTrie(List<T> list, T elem){
		
	}

	public static void main(String[] args) {
		new Ex1();
	}

}
