package tp5;

import outilsHuffman.OutilsHuffman;
import types.ABinHuffman;
import types.Couple;

/**
 * Réalisation du décodage d'un texte par la méthode de Huffman
 */

public class DecodageHuffman
{
	private final static int NB_CHAR = 255;//On prend les char de 0 � 254
	
	public static void main (String[] args)
	{
		//------------------------------------------------------------------------
		// 0. Saisir le nom du fichier à décoder (À FAIRE)
		//------------------------------------------------------------------------
		String nomFichier = "D:/Mes Documents/Cours/ESIR/progJAVA/exemple.txt.code";

		//------------------------------------------------------------------------
		// 1. Lire et construire la table de fréquences (DONNÉ)
		//------------------------------------------------------------------------
		int [] tableFrequences = OutilsHuffman.lireTableFrequences(nomFichier);

		//------------------------------------------------------------------------
		// 2. Construire l'arbre de Huffman (DONNÉ)
		//------------------------------------------------------------------------
		ABinHuffman arbreHuffman =
				OutilsHuffman.construireArbreHuffman(tableFrequences);

		//------------------------------------------------------------------------
		// 2.1 afficher l'arbre de Huffman (À FAIRE)
		//------------------------------------------------------------------------
		System.out.println("Arbre de Huffman associé au texte " + nomFichier);
		afficherHuffman(arbreHuffman);

		//------------------------------------------------------------------------
		// 3. Lire le texte codé (DONNÉ)
		//------------------------------------------------------------------------
		String texteCode = OutilsHuffman.lireTexteCode(nomFichier);

		//------------------------------------------------------------------------
		// 4. Décoder le texte (À FAIRE)
		//------------------------------------------------------------------------
		StringBuilder texteDecode = decoderTexte(texteCode, arbreHuffman);

		//------------------------------------------------------------------------
		// 5. Enregistrer le texte décode (DONNÉ)
		//------------------------------------------------------------------------
		System.out.println("texte décodé:\n\n" + texteDecode);
		OutilsHuffman.enregistrerTexte(texteDecode, nomFichier + ".decode");
	}

	/**
	 * 4. décoder une chaîne (non vide) encodée par le codage de Huffman
	 * @param texteCode    : chaîne de "0/1" à décoder
	 * @param arbreHuffman : arbre de (dé)codage des caractères
	 */
	public static StringBuilder decoderTexte(String texteCode, ABinHuffman arbreHuffman)
	{
		int i =0;
		StringBuilder ret = new StringBuilder();
		while(i<texteCode.length()){
			Couple<Character, Integer> theCouple = getCharacterWithTree(arbreHuffman, texteCode, i);
			if(theCouple != null){
				i = theCouple.deuxieme();
				ret.append(theCouple.premier());
			}

		}
		return ret;
	}
	
	/**
	 * Parcours l'arbre de Huffman de fa�on recursif
	 * @param arbreHuffman
	 * @param texteCode 
	 * @param i = index du string
	 * @return le couple <char,int>
	 */
	public static Couple<Character, Integer> getCharacterWithTree(ABinHuffman arbreHuffman, String texteCode, int i){

		if(arbreHuffman.estFeuille()){
			return new Couple<Character, Integer>(arbreHuffman.getValeur().premier(), i);
		}
		else if(texteCode.charAt(i) == '0'){
			return getCharacterWithTree(arbreHuffman.filsGauche(), texteCode, ++i);
		}
		else{
			return getCharacterWithTree(arbreHuffman.filsDroit(), texteCode, ++i);
		}
	}

	/**
	 * 2.1 afficher un arbre de Huffman
	 * @param a : arbre binaire de Huffman
	 */
	public static void afficherHuffman(ABinHuffman a)
	{
		String [] tabMotAssocie = new String[NB_CHAR];
		int [] tabInt = new int[NB_CHAR];
		recursifAffichage(a,tabMotAssocie,tabInt,"");
		for(int i = 0;i<tabMotAssocie.length;i++)
			if(tabMotAssocie[i] != null)
				System.out.println("<"+(char)i+","+tabInt[i]+">\t:"+tabMotAssocie[i]);
	}
 /**
  * Permet d'afficher <f,3> = 1001
  * @param a : Arbre d'Huffman final
  * @param tabMotAssocie : Tableau vide et contient � la fin par exemple pour f 1001
  * @param tabCountOccurence : Tableau vide et contient � la fin par exemple pour f 3
  * @param string : Initialiser � ""
  */
	private static void recursifAffichage(ABinHuffman a, String[] tabMotAssocie, int[] tabCountOccurence, String string) {
		Couple<Character,Integer> c = a.getValeur();
		tabMotAssocie[c.premier()]=string;
		tabCountOccurence[c.premier()]=c.deuxieme();
		if(a.existeGauche()){
			String s = string + "0";
			recursifAffichage(a.filsGauche(), tabMotAssocie, tabCountOccurence, s);
		}
		if(a.existeDroit()){
			String s = string + "1";
			recursifAffichage(a.filsDroit(), tabMotAssocie, tabCountOccurence, s);
		}
	}

} // DecodageHuffman
