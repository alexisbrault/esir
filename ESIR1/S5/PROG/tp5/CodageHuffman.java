package tp5;

import java.util.ListIterator;

import outilsHuffman.OutilsHuffman;
import types.ABinHuffman;
import types.Couple;
import types.ListeABH;

/**
 * R�alisation du codage d'un texte par la m�thode de Huffman
 */

public class CodageHuffman
{

	private final static char SPECIAL_CHAR = 254;
	private final static int NB_CHAR = 255;//On prend les char de 0 � 254
	
	public static void main (String[] args)
	{
		//------------------------------------------------------------------------
		// 0. Saisir le nom du fichier � coder (� FAIRE)
		//------------------------------------------------------------------------
		String nomFichier = "D:/Mes Documents/Cours/ESIR/progJAVA/exemple.txt";

		//------------------------------------------------------------------------
		// 1. Lire le texte (DONN�)
		//------------------------------------------------------------------------
		char [] texte = OutilsHuffman.lireFichier(nomFichier);

		//------------------------------------------------------------------------
		// 2. Calculer la table des fr�quences des caract�res (� FAIRE)
		//------------------------------------------------------------------------
		int [] tableFrequences = calculerFrequences(texte);
		System.out.println(faireListeAbinHuffman(tableFrequences));

		//------------------------------------------------------------------------
		// 3. Enregistrer la table de fr�quences dans le fichier de sortie (DONN�)
		//------------------------------------------------------------------------
		OutilsHuffman.enregistrerTableFrequences(tableFrequences, nomFichier + ".code");

		//------------------------------------------------------------------------
		// 4. Construire l'arbre de codage de Huffman (DONN� - � FAIRE)
		//------------------------------------------------------------------------
		ABinHuffman arbreCodageHuffman = construireArbreHuffman(tableFrequences);

		//------------------------------------------------------------------------
		// Afficher l'arbre de codage de Huffman (D�J� FAIT)
		//------------------------------------------------------------------------
		System.out.println("Arbre de Huffman associ� au texte " + nomFichier);
		DecodageHuffman.afficherHuffman(arbreCodageHuffman);

		//------------------------------------------------------------------------
		// 5. Construire la table de codage associ�e (� FAIRE)
		//------------------------------------------------------------------------
		String [] tablecodage = construireTableCodage(arbreCodageHuffman);

		//------------------------------------------------------------------------
		// 5.1. afficher la table de codage (� FAIRE)
		//------------------------------------------------------------------------
		System.out.println("Table de codage associ�e au texte " + nomFichier);
		afficherTableCodage(tablecodage);

		//------------------------------------------------------------------------
		// 6. coder le texte avec l'arbre de Huffman (� FAIRE)
		//------------------------------------------------------------------------
		StringBuilder texteCode = coderTexte(texte, tablecodage);

		//------------------------------------------------------------------------
		// 7. enregistrer le texte cod� (DONN�)
		//------------------------------------------------------------------------

		OutilsHuffman.enregistrerTexteCode(texteCode, nomFichier + ".code");

		//------------------------------------------------------------------------
		// xx. calculer et afficher les stats (� FAIRE)
		//------------------------------------------------------------------------
		calculRatio(texte,texteCode);

	}

	/**
	 * 2. calculer la fr�quence d'apparition de chaque caract�re
	 * @param  tcar tableau des caract�res du texte
	 * @return tableau de fr�quence des caract�res, indic� par les caract�res
	 */
	public static int [] calculerFrequences(char [] tcar){
		int[] result = new int[NB_CHAR];
		for(int i=0;i<tcar.length;i++){
			result[tcar[i]]++;
		}
		return result;
	}

	/**
	 * 4. construire un arbre de codage de Huffman par s�lection et combinaison
	 * des �l�ments minimaux
	 * @param tableFrequences table des fr�quences des caract�res
	 * @return arbre de codage de Huffman
	 */
	public static ABinHuffman construireArbreHuffman(int [] tableFrequences)
	{
		ListeABH list = faireListeAbinHuffman(tableFrequences);
		while(list.size()>1){
			ABinHuffman abin = new ABinHuffman();

			ABinHuffman a0 = list.removeFirst();
			ABinHuffman a1 = list.removeFirst();
			abin.setValeur(new Couple<Character, Integer>(SPECIAL_CHAR,a0.getValeur().deuxieme()+a1.getValeur().deuxieme()));
			abin.setGauche(a0);
			abin.setDroit(a1);

			ajoutTrieAsc(abin,list);
		}
		return list.getFirst();
	}

	/**
	 * 4.1 Faire une liste tri�e dont chaque �l�ment est un arbreBinaire<br>
	 * comprenant un unique sommet dont l'�tiquette est un couple
	 * <caract�re, fr�quence>, tri� par fr�quence croissante
	 * @param tableFrequences : table des fr�quences des caract�res
	 * @return		      la liste tri�e
	 */
	private static ListeABH faireListeAbinHuffman(int [] tableFrequences){
		ListeABH list = new ListeABH();
		for(int i=0;i<tableFrequences.length;i++)
			if(tableFrequences[i]!=0){
				ABinHuffman abin = new ABinHuffman();
				abin.setValeur(new Couple<Character, Integer>((char)i, tableFrequences[i]));
				ajoutTrieAsc(abin,list);
			}
		return list;
	}

	/**
	 * Insertion tri�e dans une liste
	 * @param abin Arbre Binaire � ajouter
	 * @param list liste tri�e
	 */
	private static void ajoutTrieAsc(ABinHuffman abin, ListeABH list) {

		ListIterator<ABinHuffman> it = list.listIterator();
		while(it.hasNext()){
			ABinHuffman next = it.next();
			if(next.getValeur().deuxieme()>abin.getValeur().deuxieme()){
				it.previous();
				break;
			}
		}
		it.add(abin);		
	}

	/**
	 * 5. construire la table de codage correspondant � l'arbre de Huffman
	 * @param abinHuff : arbre de Huffman
	 * @return table de (d�)codage indic� par lex caract�res
	 */
	public static String [] construireTableCodage(ABinHuffman abinHuff)
	{
		String[] result = new String[NB_CHAR];
		parcoursArbre(abinHuff,result,"");
		return result;
	}

	/**
	 * Parcours r�cursif de l'arbre et affiche 
	 * @param abinHuff :  arbre Huffman
	 * @param result :  tableau final � initialiser contenant le mot binaire correspondant au char
	 * @param string :  "" pour commencer le parcours
	 */
	private static void parcoursArbre(ABinHuffman abinHuff, String[] result,String string) {
		Couple<Character,Integer> c = abinHuff.getValeur();
		result[c.premier()]=string;
		if(abinHuff.existeGauche()){
			String s = string+"0";
			parcoursArbre(abinHuff.filsGauche(), result, s);
		}
		if(abinHuff.existeDroit()){
			String s = string+"1";
			parcoursArbre(abinHuff.filsDroit(), result, s);
		}
	}

	/**
	 * 5.1. Afficher la table de codage associ�e au texte
	 * @param tablecodage : table de codage associ�e au texte
	 */
	public static void afficherTableCodage(String [] tablecodage)
	{
		for(int i=0;i<tablecodage.length;i++)
			if(tablecodage[i]!=null)
				System.out.println((char)i+" = "+tablecodage[i]);
	}

	/**
	 * 6. Coder un texte � l'aide de la table de codage
	 * @param texte � coder
	 * @param tablecodage : table de codage associ�e au texte
	 * @return texte cod�
	 */
	public static StringBuilder coderTexte(char [] texte, String [] tablecodage)
	{
		StringBuilder stb = new StringBuilder();
		for(char c:texte)
			stb.append(tablecodage[c]);
		return stb;
	}
	/**
	 * Calcul le ratio de compression 
	 * @param texte : le texte dans un tableau de char
	 * @param stb : le texte cod� par Huffman
	 */
	private static void calculRatio(char [] texte,StringBuilder stb){
		//char: The char data type is a single 16-bit Unicode character.
		System.out.println(texte.length*16+" -> "+stb.length());
		System.out.println("Soit un facteur de compression de "+ (double)(texte.length*16)/stb.length());
	}
}// CodageHuffman