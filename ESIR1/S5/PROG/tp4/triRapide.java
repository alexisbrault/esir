package tp4;

public class triRapide {
	
	public static void main(String[] args){

		int[] tab1 = OutilFichier.lireTableauEntier("/private/student/5/55/16009455/Documents/ESIR01/Prog/donnees_50000", 50000);

		trier(tab1, tab1.length-1);
		OutilFichier.ecrireTableauEntier("/private/student/5/55/16009455/Documents/ESIR01/Prog/test_50000", tab1);

	}

	//Partage un tableau entre 2 pivote binf et bsup (binf<=bsup)
	static int partager(int[] t, int binf, int bsup){		
		int indiceachanger = binf;
		int pivot = t[binf];
		while(binf != bsup){

			if(pivot<t[binf] && pivot>=t[bsup]){
				int temp = t[binf];
				t[binf] = t[bsup];
				t[bsup] = temp;
			}
			else{
				if(pivot>=t[binf]){
					binf++;
					if(binf == bsup){
						if(pivot< t[binf]){
							binf--;
						}
						break;
					}
				}
				if(pivot<t[bsup]){
					bsup--;
					if(binf == bsup){
						if(pivot < t[bsup]){
							binf--;
						}
						break;
					}
				}
			}

		}

		int temp = t[binf];
		t[binf] = pivot;
		t[indiceachanger] = temp;

		return binf;
	}

	//Tri un tableau entre 2 pivots
	private static void triRapide(int[] t, int binf, int bsup){

		int indicePivot = partager(t, binf, bsup);
		if(indicePivot-1>0 && binf<indicePivot-1){
			triRapide(t, binf, indicePivot-1);
		}
		if(indicePivot+1<t.length && bsup>indicePivot+1){
			triRapide(t, indicePivot+1, bsup);
		}
	}

	//Tri un tableau de int
	public static void trier(int[] t, int nb){
		triRapide(t, 0, nb);
	}

}
