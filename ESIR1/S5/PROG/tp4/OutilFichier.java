package tp4;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class OutilFichier {

	public static int[] lireTableauEntier(String chemin,int nbEntree){
		Scanner scanner = null;
		int[] result = new int[nbEntree];
		try {
			scanner = new Scanner(new File(chemin));
			int i=0;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result[i]=Integer.parseInt(line);
				i++;
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			scanner.close();
		}
		return result;
	}

	public static void ecrireTableauEntier(String chemin,int[] tnb){
		File f = new File (chemin);

		try{
			FileWriter fw = new FileWriter (f);
			for (int i : tnb){
				fw.write (String.valueOf (i));
				fw.write ("\r\n");
			}

			fw.close();
		}
		catch (IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
