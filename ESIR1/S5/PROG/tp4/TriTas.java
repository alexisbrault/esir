package tp4;


import java.util.Arrays;

import junit.framework.Assert;

public class TriTas {

	public static void main(String[] args) {
		//int tnb[] = {12,6,20,4,7,9};
		int tnb[] = OutilFichier.lireTableauEntier("/private/student/5/55/16009455/Documents/ESIR01/Prog/donnees_50000",50000);
		int tnb2[] =Arrays.copyOf(tnb, tnb.length);
		long d = System.currentTimeMillis();
		trier(tnb);
		System.out.println(System.currentTimeMillis()-d+" ms");
		Arrays.sort(tnb2);
		System.out.println("Tableau bien tri�" + isWellSorted(tnb, tnb2));
		OutilFichier.ecrireTableauEntier("/private/student/5/55/16009455/Documents/ESIR01/Prog/tri-res",tnb);
	}

	/**
	 * Trie un tableau d'entier
	 * @param tnb : le tableau � trier
	 */
	public static void trier(int [] tnb){
		int taille=0;
		for(int i=0;i<tnb.length;i++){
			ajouterTas(tnb, tnb[i],taille++);
		}

		for(int i=0;i<tnb.length;i++){
			supprimerElementTas(tnb,0,--taille);
		}
	}

	/**
	 * Ajoute dans le tas la valeur correctement dans l'arbre
	 * @param tnb : tableau � trier
	 * @param p : valeur � ajouter au tas
	 * @param taille : taille du tableau non ajout� au tas (!=tnb.length())
	 */
	private static void ajouterTas(int [] tnb,int p,int taille){
		tnb[taille]=p;
		int i = taille;
		while(tnb[i]>getParent(tnb, i)){
			echanger(tnb,i,getParentIndex(tnb, i));
			i=getParentIndex(tnb, i);
		}
	}

	/**
	 * Supprimer un �l�ment du tas et r�ordonne le tas
	 * @param tnb : tableau � trier
	 * @param p : valeur � ajouter au tas
	 * @param taille : taille du tableau non ajout� au tas (!=tnb.length())
	 */
	private static void supprimerElementTas(int [] tnb,int i,int taille){

		echanger(tnb, i, taille);

		while(gaucheExist(tnb, i,taille)){
			int indexMax=getGaucheIndex(tnb, i, taille);
			if(droitExist(tnb, i,taille) && getGauche(tnb, i,taille)<getDroit(tnb, i,taille))
				indexMax=getDroitIndex(tnb, i, taille);
			if(tnb[i]<tnb[indexMax]){
				echanger(tnb, i, indexMax);
				i=indexMax;
			}
			else
				break;
		}
	}



	//AFFICHAGE

	/**
	 * Permet l'affichage d'un tableau de int
	 * @param tnb
	 * @return Un string ou les �l�ments du tableau sont s�par�s pas des espaces
	 */
	private static String affichage(int[] tnb){
		StringBuilder stb = new StringBuilder();
		for(int i=0;i<tnb.length;i++)
			stb.append(tnb[i]+" ");
		return stb.toString();
	}

	/**
	 * Permet l'affichage d'un tableau de int
	 * @param tnb : Tableau possiblement tri�
	 * @param tnb2 : Tableau tri� et de fa�on sur
	 * @return Un string ou les �l�ments des 2 tableaux sont compar� et les affiches
	 */
	private static String affichage(int[] tnb,int[] tnb2){
		StringBuilder stb = new StringBuilder();
		for(int i=0;i<tnb.length;i++){
			stb.append(tnb[i]+"\t"+tnb2[i]);
			stb.append("\t"+Boolean.toString(tnb[i]==tnb2[i]));
			stb.append("\n");
		}
		return stb.toString();
	}

	/**
	 * Test si un tableau est �gal � un autre
	 * @param tnb : Tableau possiblement tri�
	 * @param tnb2 : Tableau tri� de fa�on sur
	 * @return tnb == tnb2
	 */
	private static boolean isWellSorted(int[] tnb,int[] tnb2){
		for(int i=0;i<tnb.length;i++){
			if(tnb[i]!=tnb2[i])
				return false;
		}
		return true;
	}

	//UTIL

	/**
	 * Permet d'�changer dans un tableau 2 index
	 * @param tnb : le tableau ou les index doivent �tre changer
	 * @param i1 : premier index
	 * @param i2 : deuxi�me index
	 */
	private static void echanger(int [] tnb,int i1,int i2){
		int temp=tnb[i1];
		tnb[i1]=tnb[i2];
		tnb[i2]=temp;
	}

	//Test si un fils gauche � l'index existe
	private static boolean gaucheExist(int [] tnb,int i,int taille){
		return taille>2*i+1;
	}
	//Test si un fils gauche � l'index existe
	private static boolean droitExist(int [] tnb,int i,int taille){
		return taille>2*i+2;
	}
	//Retourne la valeur du parent d'un index
	private static int getParent(int [] tnb,int i){
		return tnb[getParentIndex(tnb, i)];
	}
	//Retourne la valeur du fils gauche d'un index
	private static int getGauche(int [] tnb,int i,int taille){
		return tnb[getGaucheIndex(tnb, i,taille)];
	}
	//Retourne la valeur du fils droit d'un index
	private static int getDroit(int [] tnb,int i,int taille){
		return tnb[getDroitIndex(tnb, i, taille)];
	}
	//Retourne l'index du parent d'un index i
	private static int getParentIndex(int [] tnb,int i){
		Assert.assertTrue("Erreur getParentIndex: index trop grand", 0<=(i-1)/2);
		return (i-1)/2;
	}
	//Retourne l'index du fils gauche d'un index i
	private static int getGaucheIndex(int [] tnb,int i,int taille){
		Assert.assertTrue("Erreur getGaucheIndex: index trop grand", taille>2*i+1);
		return 2*i+1;
	}
	//Retourne l'index du fls droit d'un index i
	private static int getDroitIndex(int [] tnb,int i,int taille){
		Assert.assertTrue("Erreur getGaucheIndex: index trop grand", taille>2*i+2);
		return 2*i+2;
	}
}
