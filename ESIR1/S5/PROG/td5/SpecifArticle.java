package td5;

public interface SpecifArticle {
	public String designation();
	public int quantite();
	public double prixHT();
	public double prixTTC();
	public void ajouter(int q);
	public void retirer(int q);
}
