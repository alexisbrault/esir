package td5;

public class Cercle implements Figure {
	
	private double x;
	private double y;
	private double r;

	public Cercle(double x, double y, double r) {
		this.x=x;
		this.y=y;
		this.r=r;
	}

	@Override
	public String genre() {
		return "Cercle";
	}

	@Override
	public boolean inside(double x, double y) {
		double dx=x-this.x;
		double dy=y-this.y;
		return dx*dx+dy*dy<=this.r;
	}

}
