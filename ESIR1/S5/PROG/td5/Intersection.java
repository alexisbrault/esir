package td5;

public class Intersection implements Figure {
	
	private Figure f[];

	public Intersection(Figure... figures) {
		this.f=figures;
	}

	@Override
	public String genre() {
		return "Intersection";
	}

	@Override
	public boolean inside(double x, double y) {
		for(Figure figure:f)
			if(!figure.inside(x, y))
				return false;
		return true;
	}

}
