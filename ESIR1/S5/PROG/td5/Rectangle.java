package td5;

public class Rectangle implements Figure{
	
	private double x;
	private double y;
	private double h;
	private double l;
	
	public Rectangle(double x, double y, double h,double l) {
		this.x=x;
		this.y=y;
		this.h=h;
		this.l=l;
	}

	@Override
	public String genre() {
		return "Rectangle";
	}

	@Override
	public boolean inside(double x, double y) {
		return(Math.abs(this.x-x)<l/2)&&(Math.abs(this.y-y)<h/2);
	}

}
