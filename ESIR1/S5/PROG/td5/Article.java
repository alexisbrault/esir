package td5;

public class Article implements SpecifArticle,Comparable<Article>{

	private String designation;
	private int quantite;
	private double prixHT;
	private final static double tauxTVA=1.196;
	
	public Article(String designation, int quantite, double prixHT) {
		this.designation=designation;
		this.quantite=quantite;
		this.prixHT=prixHT;
	}

	@Override
	public String designation() {
		return this.designation;
	}

	@Override
	public int quantite() {
		return this.quantite;
	}

	@Override
	public double prixHT() {
		return this.prixHT;
	}

	@Override
	public double prixTTC() {
		return prixHT()*tauxTVA;
	}

	@Override
	public void ajouter(int q) {
		this.quantite+=q;
	}

	@Override
	public void retirer(int q) {
		this.quantite-=q;
	}
	
	@Override
	public String toString() {
		StringBuilder stb = new StringBuilder();
		stb.append(this.designation);
		stb.append(", ");
		stb.append(this.prixHT);
		stb.append(" euros (");
		stb.append(this.quantite);
		stb.append(" en stock)");
		return stb.toString();
	}

	@Override
	public int compareTo(Article o) {
		return this.designation.compareTo(o.designation);
	}

}
