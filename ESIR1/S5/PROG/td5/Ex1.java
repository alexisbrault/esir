package td5;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;



public class Ex1 {
	
	public static void main(String[] args) {
		List<Figure> liste = new LinkedList<Figure>();
		liste.add(new Cercle(0,0,0));
		liste.add(new Cercle(0,0,0));
		liste.add(new Rectangle(0,0,0,0));
		System.out.println(compteFigureListe(liste));
	}
	
static Map<String,Integer>compteFigureListe(List<Figure> liste){
	Map<String,Integer> map = new HashMap<String,Integer>();
	Iterator<Figure> it = liste.iterator();
	while(it.hasNext()){
		String genre = it.next().genre();
		int value = map.containsKey(genre) ? map.get(genre) : 0;
		map.put(genre, value + 1);
	}
	
	return map;
}

}
