package td5;

public class Livre extends Article implements SpecifArticle {

	private int nbPage;
	private String numeroISBN;

	public Livre(String designation, int quantite, double prixHT, int nbPage, String numeroISBN) {
		super(designation, quantite, prixHT);
		this.nbPage=nbPage;
		this.numeroISBN=numeroISBN;
	}
	
	@Override
	public String toString() {
		StringBuilder stb = new StringBuilder();
		stb.append(super.toString());
		stb.append("ISBN : ");
		stb.append(this.numeroISBN);
		stb.append(" (");
		stb.append(this.nbPage);
		stb.append(" )");
		return stb.toString();
	}

}
