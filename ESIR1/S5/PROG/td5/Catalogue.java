package td5;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Catalogue {

	private List<Article> listeArticles;
	private int nbArticles;
	
	public Catalogue() {
		nbArticles=0;
		listeArticles = new LinkedList<Article>();
	}
	
	public void afficher(){
		for(Article a: listeArticles)
			System.out.println(a);
	}
	
	public void trier(){
		Collections.sort(listeArticles);
	}
	
	public void ajouter(Article a){
		listeArticles.add(a);
		nbArticles++;
	}
	
	private class ComparPrix implements Comparator<Article>{

		@Override
		public int compare(Article o1, Article o2) {
			return Double.compare(o1.prixHT(), o2.prixHT());
		}
		
	}
	
	private class ComparQuantite implements Comparator<Article>{

		@Override
		public int compare(Article o1, Article o2) {
			return Integer.compare(o1.quantite(), o2.quantite());
		}
		
	}
}

