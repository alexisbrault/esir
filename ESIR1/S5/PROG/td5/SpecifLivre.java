package td5;

public interface SpecifLivre extends SpecifArticle {
	public int nombrePages();
	public String numeroISBN();
}
