package td5;

public class Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Catalogue c = new Catalogue();
		c.ajouter(new Livre("Le saigneur des agneaux", 20, 30, 640, "111-11-22-36"));
		c.ajouter(new Article("Arbre", 10, 50));
		c.ajouter(new Article("Tongues", 10, 50));
		c.trier();
		c.afficher();
	}

}
