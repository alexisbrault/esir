package tp1;

import java.util.Scanner;

public class Main {

	/**
	 *initialiser un tableau avec les valeurs d�une suite de nombres entiers lus au clavier
	 *la suite est termin�e par valfin
	 *
	 *@param tnb:tableau de nombres(d�j� cr��) � initialiser
	 *@param valfin:valeur qui met fin � la saisie ;ne doit PAS �tre plac�e dans le tableau
	 *@param entree:scanner d�entr�e o� se fait la lecture
	 *@post :le tableau contient N nombres entiers(0<=N<=tnb.length)
	 *@return :nombre entiers plac�s dans le tableau
	 */

	public static int lireTableau(int[] tnb, int valfin, Scanner scan){
		int i=0,n;
		for(n=0;n<tnb.length;n++){
			System.out.println("tnb["+n+"]=");
			i = scan.nextInt();
			if(i == valfin) break;
			tnb[n] = i;
		}

		if(n==tnb.length)
			System.out.println("Tableau rempli");
		if(i==-1)
			System.out.println("Arret remplissage");

		return n;
	}

	/**
	 * ajoute une valeur de fa�on tri�e
	 *@param tnb:le tableau d'entr�e
	 *@pre 0<=nb<=tnb.length
	 *@post le tableau modifi�
	 */

	public static void triParInsertion(int[] tnb){
		for(int i=0;i<tnb.length;i++){
			int k = tnb[i];
			int j = i;
			while(j>0 && tnb[j-1] > k){
				tnb[j] = tnb[j-1];
				j--;
			}
			tnb[j] = k;
		}
	}

	/**
	 * afficher les nb premiers �l�ments d�un tableau.
	 *@param tnb:tableau initialis�
	 *@param nb:nombre d��l�ments en t�te du tableau
	 *@pre 0<=nb<=tnb.length
	 *@post le tableau n�est pas modifi�
	 */
	static void afficherTableau(int[] tnb, int nb){
		assert (tnb==null || tnb.length<=0):"Erreur afficherTableau";
		try{
			if(nb>tnb.length)
				nb=tnb.length;
			for(int i=0;i<nb;i++)
				System.out.println("["+i+"]="+tnb[i]);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	/**
	 * fusionne 2 tableaux pr�alablement tri�s
	 *@param tnb1:tableau tri�
	 *@param tnb2:tableau tri�
	 *@post tableau fusionn� et tri� 
	 */	
	public static int[] fusionTableau(int[] tnb1,int tnb1Length, int[] tnb2,int tnb2Length){
		int[] tnb = new int[tnb1Length+tnb2Length];
		int i=0,j=0,k=0;

		while (k < tnb.length){ 
			if (tnb2Length == 0 || (i<tnb1Length && tnb1[i] < tnb2[j])){	
				tnb[k] = tnb1[i];
				i++;
			}
			else if(j<tnb2Length){
				tnb[k] = tnb2[j];
				j++;
			}
			k++;
		}
		return tnb;
	}

	/**
	 * trouve i tq tnb[i]=val
	 *@param tnb:tableau tri�
	 *@param val:valeur cherch�e
	 *@param nbVal:nombre de valeur dans le tableau
	 *@post indice i 
	 */
	public static int rechercheDicho(int[] tnb,int val,int len){
		try{
			int debut = 0;
			int fin = tnb.length-1;
			while (debut <= fin) {
				int milieu =( debut + fin)/2;
				if (tnb[milieu]< val)   debut = milieu + 1;
				else if (tnb[milieu]> val) fin = milieu - 1;
				else return milieu; // trouv�
			}
			return -1; // pas trouv�.

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return -1;
	}

}
