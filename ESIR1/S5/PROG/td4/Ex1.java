package td4;

public class Ex1 {


	static double puissanceX2(double x, int n){
		System.out.println(x+"^"+n);

		if(n==0)
			return 1;
		double t = puissanceX2(x,n/2);
		if(n%2==0)
			return t*t;
		else
			return x * t * t;
		
	}


	public static void main(String[] args) {
		System.out.println(puissanceX2(3, 6));
	}

}
