#!/usr/bin/env python

from Allocateur import Allocateur

#Configurations
Allocateur.a_terminal= True
Allocateur.a_fichier = True
Allocateur.fichier   = "D:/Logiciel/Python/Scripts/logs.txt"

Allocateur.ReadUserInstructions()


"""
#Scenario 1
allocateur = Allocateur(2)
allocateur.nouveauProcessus(1)
allocateur.demandeRessource(1,1)
allocateur.nouveauProcessus(2)
allocateur.demandeRessource(2,1)
Allocateur.cl_afficherProcessusActif(allocateur)
"""

"""
#Scenario 2
allocateur = Allocateur(2)
allocateur.nouveauProcessus(2)
allocateur.demandeRessource(2,1)
allocateur.demandeRessource(1,2)
allocateur.demandeRessource(1,1)
allocateur.demandeRessource(2,2)
allocateur.afficher()
"""

"""
#Scenario 3
allocateur = Allocateur(2)
allocateur.nouveauProcessus(1)
allocateur.nouveauProcessus(2)
allocateur.nouveauProcessus(3)
allocateur.demandeRessource(1,2)
allocateur.demandeRessource(2,2)
allocateur.demandeRessource(2,1)
allocateur.demandeRessource(3,2)
Allocateur.cl_attenteParRessource(allocateur)
Allocateur.cl_attenteParProcessus(allocateur)
allocateur.supprimerProcessus(1)
allocateur.libereRessource(2,2)
Allocateur.cl_afficherProcessusActif(allocateur)
"""
