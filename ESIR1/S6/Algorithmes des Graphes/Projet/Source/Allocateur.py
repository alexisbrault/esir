#!/usr/bin/env python

#import matplotlib.pyplot as plt

import networkx as nx

class Allocateur:

        a_terminal= True
        a_fichier = False
        fichier   = "D:/Logiciel/Python/Scripts/logs.txt"

        def __init__(self, nombreRessources):
                self.nombreRessources = nombreRessources
                self.g=nx.DiGraph()
                while(nombreRessources>0):
                        self.g.add_node('R'+str(nombreRessources))
                        nombreRessources=nombreRessources-1
                
        def nouveauProcessus(self, idProcessus):
                if not self.g.has_node('P'+str(idProcessus)):
                        self.g.add_node('P'+str(idProcessus))
                        return True
                return False

        def supprimerProcessus(self, idProcessus):
                if self.g.has_node('P'+str(idProcessus)):
                        for node in self.g.predecessors('P'+str(idProcessus)):
                                self.libereRessource(idProcessus,node[1:])
                        self.g.remove_node('P'+str(idProcessus))
                        return True
                return False
                
        def estBloque(self, idProcessus):
                return len(self.g.successors(idProcessus))>0
                
        def demandeRessource(self, idProcessus, idRessources):
                if self.g.has_node('R'+str(idRessources)):
                        if not self.g.has_node('P'+str(idProcessus)):
                                self.nouveauProcessus(idProcessus)
                        if not self.estBloque('P'+str(idProcessus)):
                                affectation = False
                                if(not self.g.successors('R'+str(idRessources))):
                                        self.g.add_edge('R'+str(idRessources), 'P'+str(idProcessus),weight = 0)
                                        affectation = True
                                else:
                                        self.g.add_edge('P'+str(idProcessus), 'R'+str(idRessources),
                                                        weight = len(self.g.predecessors('R'+str(idRessources))))
                                        
                                if self.estInterbloquer():
                                        Allocateur.prints("Interblocage évité")
                                        if not affectation:
                                                self.g.remove_edge('P'+str(idProcessus), 'R'+str(idRessources))
                                        else:
                                                self.g.remove_edge('R'+str(idRessources), 'P'+str(idProcessus))
                                        return False
                                return True
                return False
                                
        def libereRessource(self, idProcessus, idRessources):
                if self.g.has_node('P'+str(idProcessus))and self.g.has_node('R'+str(idRessources)):
                        if not self.estBloque('P'+str(idProcessus)):
                                if(self.g.has_edge('R'+str(idRessources), 'P'+str(idProcessus))):
                                        self.g.remove_edge('R'+str(idRessources), 'P'+str(idProcessus))
                                        if( self.g.predecessors('R'+str(idRessources))):
                                                min_node = 0
                                                min_weight = 100000
                                                for node in self.g.predecessors('R'+str(idRessources)):
                                                        for (u,v,d) in self.g.edges(data=True):
                                                                if node == u :
                                                                        if(d['weight']<min_weight):
                                                                                min_node = node
                                                                                min_weight = d['weight']
                                                                        d['weight'] = d['weight'] - 1
                                                self.g.remove_edge(min_node, 'R'+str(idRessources))
                                                self.g.add_edge('R'+str(idRessources), min_node,weight = 0)
                                                return True
                return False

        def estInterbloquer(self):
                #Algorithme de Marimont
                h = self.g.copy()
                h_vide = h.size()==0
                if not h_vide:
                        E = self.pointsEntree(h)
                        S = self.pointsSortie(h)
                while( not(h_vide)):
                        for node in E:
                                h.remove_node(node)
                        for node in S:
                                if h.has_node(node):
                                        h.remove_node(node)
                        h_vide = h.size()==0
                        if not h_vide:
                                E = self.pointsEntree(h)
                                S = self.pointsSortie(h)
                        if(len(E)==0 and len(S)==0 and not h_vide):
                                return True;
                return False

        def pointsEntree(self, h):
                pointsEntree = []
                for node in h.nodes():
                        if not h.predecessors(node):
                                pointsEntree.append(node)
                return pointsEntree
        
        def pointsSortie(self, h):
                pointsSortie = []
                for node in h.nodes():
                        if not h.successors(node):
                                pointsSortie.append(node)
                return pointsSortie

        def processusActif(self):
                processusactif = []
                for node in self.g.nodes():
                        if node.split()[0][0] == 'P':
                                if not self.estBloque(node) :
                                        processusactif.append(node)
                return processusactif

        def attenteParRessource(self):
                dd = {}
                for n in self.g.nodes():
                        if str(n).split()[0][0]== 'R':
                                liste = {}
                                i=0
                                for node in self.g.predecessors(n):
                                        for (u,v,we) in self.g.edges(data=True):
                                                if node == u:
                                                        w = int(we['weight'])
                                                        liste[str(u)]= w;
                                                        i = i +1
                                if i!=0:
                                        dd[str(n)]=liste
                                                        
                return dd
        
        def attenteParProcessus(self):
                d = {}
                for n in self.g.nodes():
                        if str(n).split()[0][0]== 'R':
                                liste = {}
                                i=0
                                succ = self.g.successors(n)
                                if len(succ)==1:
                                        p = succ[0]
                                else:
                                        continue
                                for node in self.g.predecessors(n):
                                        for (u,v,we) in self.g.edges(data=True):
                                                if node == u:
                                                        w = int(we['weight'])
                                                        liste[str(u)]= w;
                                                        i = i +1
                                if i!=0:
                                        d[str(p)]=liste
                                                        
                return d
        
        def afficher(self):
                processus =[]
                ressource =[]
                for node in self.g.nodes():
                        if node.split()[0][0] == 'P':
                                if self.estBloque(node) :
                                        processus.append(str(node)+' bloque')
                                else:
                                        processus.append(str(node)+' actif')
                        else:
                                if self.g.successors(node):
                                        ressource.append(str(node)+ " allouée")
                                else:
                                        ressource.append(str(node)+ " libre")
                Allocateur.prints("Ressources :")
                Allocateur.prints(ressource)
                Allocateur.prints("Processus :")
                Allocateur.prints(processus)
                                
# ======================================= LECTURE CLAVIER =======================================

        @staticmethod                               
        def ReadUserInstructions():
                nb = input('Entrer le nombre de ressources :');
                print('\nTapez "aide" pour avoir la liste des instructions disponibles \n\n')
                allocateur = Allocateur(int(nb))
                Allocateur.prints('------- Allocateur créé -------')
                instruction = 'debut'
                while instruction != 'fin':
                        instruction = input("Instruction:")
                        Allocateur.prints("Instruction: "+instruction)
                        Allocateur.switch(instruction,allocateur)
                        Allocateur.prints('\n')
                Allocateur.prints('Fin de l\'allocateur')
                
        @staticmethod
        def cl_demandeRessource(a):
            Allocateur.prints('------- Demande de ressource(s) par un processus -------')
            id_P = input('Id processus :')
            id_R = input('Id ressource :')
            Allocateur.prints("demandeRessource("+str(id_P)+","+str(id_R)+")")
            if a.demandeRessource(int(id_P),int(id_R)):
                    Allocateur.prints("Ressource allouée ")
                    

        @staticmethod
        def cl_libereRessource(a):
            Allocateur.prints('------- Libération d’une ressource par un processus -------')
            id_P = input('Id processus :')
            id_R = input('Id ressource :')
            Allocateur.prints("libereRessource("+str(id_P)+","+str(id_R)+")")
            if a.libereRessource(int(id_P),int(id_R)):
                  Allocateur.prints("Ressource libérée ")  

        @staticmethod
        def cl_nouveauProcessus(a):
            Allocateur.prints('------- Création d\'un processus -------')
            id_P = input('Id processus :')
            Allocateur.prints("nouveauProcessus("+str(id_P)+")")
            if a.nouveauProcessus(int(id_P)):
                    Allocateur.prints("Processus créé ")

        @staticmethod
        def cl_supprimerProcessus(a):
            Allocateur.prints('------- Destruction d\'un processus -------')
            id_P = input('Id processus :')
            Allocateur.prints("supprimerProcessus("+str(id_P)+")")
            if a.supprimerProcessus(int(id_P)):
                   Allocateur.prints("Processus détruit ")

        @staticmethod
        def cl_afficherProcessusActif(a):
            Allocateur.prints('------- Affichage des processus actifs -------')
            liste = a.processusActif()
            Allocateur.prints("processusActif()")
            if liste:
                   for node in liste:
                           Allocateur.prints(node)
            else:
                    Allocateur.prints('Aucun processus est actif')

        @staticmethod
        def cl_attenteParRessource(a):
            Allocateur.prints('------- Affichage des files d’attente par ressource -------')
            d1 = a.attenteParRessource()
            Allocateur.prints("attenteParRessource()")
            if d1:
                for key, value in d1.items():
                        Allocateur.prints ('Ressource :'+key)
                        Allocateur.prints(value)
                        
            else:
                    print('Aucun processus en attente')

        @staticmethod
        def cl_attenteParProcessus(a):
            Allocateur.prints('------- Affichage des attentes entre processus -------')
            d2 = a.attenteParProcessus()
            Allocateur.prints("attenteParProcessus()")
            if d2:
                for key, value in d2.items():
                        Allocateur.prints ('Processus :'+key)
                        Allocateur.prints(value)
                        
            else:
                    Allocateur.prints('Aucun processus en attente')

        @staticmethod
        def cl_afficher(a):
            Allocateur.prints('------- Affichage du graphe -------')
            a.afficher()
            
        @staticmethod
        def cl_aide():
                Allocateur.prints('------- Aide -------')
                Allocateur.prints(' O1 : Création d\'un processus ')
                Allocateur.prints(' O2 : Destruction d\'un processus ')
                Allocateur.prints(' O3 : Demande de ressource(s) par un processus ')
                Allocateur.prints(' O4 : Libération d’une ressource par un processus ')
                Allocateur.prints(' O5 : Affichage des files d’attente par ressource ')
                Allocateur.prints(' O6 : Affichage des processus actifs ')
                Allocateur.prints(' O7 : Affichage des attentes entre processus ')
                Allocateur.prints(' a  : Affichage du graphe ')
                Allocateur.prints(' fin : Terminer le programme')

            
        @staticmethod
        def switch(argument,a):
                if argument   == 'O1':
                        Allocateur.cl_nouveauProcessus(a)
                elif argument == 'O2':
                        Allocateur.cl_supprimerProcessus(a)
                elif argument == 'O3':
                        Allocateur.cl_demandeRessource(a)
                elif argument == 'O4':
                        Allocateur.cl_libereRessource(a)
                elif argument == 'O5':
                        Allocateur.cl_attenteParRessource(a)
                elif argument == 'O6':
                        Allocateur.cl_afficherProcessusActif(a)
                elif argument == 'O7':
                        Allocateur.cl_attenteParProcessus(a)
                elif argument == 'a':
                        Allocateur.cl_afficher(a)
                elif argument == 'aide':
                        Allocateur.cl_aide()
                elif argument == 'fin':
                        return 
                else:
                        Allocateur.prints('Instruction non reconnue')

# ======================================= ENREGISTREMENT + AFFICHAGE =======================================
        @staticmethod
        def prints(s):
                if Allocateur.a_terminal:
                        print(s)
                if Allocateur.a_fichier:
                        with open(Allocateur.fichier, "a+") as myfile:
                                myfile.write(str(s)+"\n")
