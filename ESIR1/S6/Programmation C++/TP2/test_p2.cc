#include <deque>
#include <gtest/gtest.h>
#include <cstdlib>

// fichier qui contient l'implémentation de la classe Liste
#include "liste.hh"


//------------------------------------------------------------------------
// tests iterateur
//------------------------------------------------------------------------

// initialiser const_iterator
// - begin
// - operateur ++
// - operateur --
TEST(TestList, q20)
{
  Liste<int> liste;
  liste.push_back(5);
  liste.push_back(8);

  EXPECT_EQ(
	    (unsigned int) 1,
	    (unsigned int) liste.size()) << "Erreur taille liste";

  Liste<int>::const_iterator it = liste.begin();
  EXPECT_EQ(
  	    (unsigned int) 5,
  	    (unsigned int) *it) << "Erreur valeur";
		
	++it;
	
  EXPECT_EQ(
	    (unsigned int) 8,
	    (unsigned int) *it) << "Erreur valeur";
		
	--it;
	EXPECT_EQ(
	    (unsigned int) 5,
	    (unsigned int) *it) << "Erreur valeur";

}

#ifndef VALGRIND
// operateur ->
TEST(TestList, q23)
{
	Liste<std::string> liste;
	std::string a = "a";
	liste.push_back(a);
	Liste<std::string>::iterator it = liste.begin();
	it->append("bc");
	EXPECT_EQ(
	    (std::string) "abc",
	    (std::string) *it) << "Erreur valeur";
}
#endif

#ifndef VALGRIND
// operateur +
// operateur =
// constructeur de copie
TEST(TestList, q44)
{
	Liste<std::string> l1;
	std::string a="a",b="b",c="c",d="d";
	l1.push_back(a);
	l1.push_back(b);
	Liste<std::string> l2 ;
	l2.push_back(c);
	l2.push_back(d);
	l2 = l1+l2;
	EXPECT_EQ(
	    (std::string) "d",
	    (std::string) l2.back()) << "Erreur valeur";
	l2.pop_back();
	l2.pop_back();
	EXPECT_EQ(
	    (std::string) "b",
	    (std::string) l2.back()) << "Erreur valeur";
	
}
#endif
