/*
 * Liste.hh
 *
 *  Created on: 24 févr. 2016
 *      Author: 16009455
 */

#ifndef LISTE_H_
#define LISTE_H_


#include "cyclicNode.h"
#include <cassert>
#include <assert.h>
#include <ostream>


template <class T>
class Liste{


protected:
	typedef DataStructure::cyclicNode<T> Chainon;

private:
	Chainon* sentinelle;
	int taille;

	//Permet de copier une liste
	void Liste<T>::copierListe(const Liste<T> &liste) {
		sentinelle = new Chainon();
		for (Liste<T>::const_iterator it = liste.begin(); it != liste.end(); ++it) {
			T temp = *it;
			this->push_back(temp);
		}
	}

public:
	//Constructeur
	Liste():sentinelle(new Chainon()), taille (0){};
	//Destructeur
	~Liste(){
		int n =taille;
		for(int i=0; i<n; i++){
			pop_back();
		}
		delete sentinelle;
	}
	//Constructeur de copie
	Liste(const Liste<T> &liste) {
		copierListe(liste);
	}
	//Permet de savoir si la liste est vide
	bool empty()const{
		return taille==0;
	}
	//Renvoie la taille de la liste
	int size()const{
		return taille;
	}
	//Renvoie la valeur du premier element de la liste
	T& front(){
		assert(!empty());
		return sentinelle->next()->data();
	}
	//Renvoie la valeur du dernier element de la liste
	T& back(){
		assert(!empty());
		return sentinelle->previous()->data();
	}
	//Ajoute l'élément T au debut de la liste
	void push_front(T& element){
		sentinelle->insertAfter(new Chainon(element));
		taille++;
	}
	//Ajoute l'élément T à la fin de la liste
	void push_back(T& element){
		sentinelle->insertBefore(new Chainon(element));
		taille++;
	}
	//Retire le premier element de la liste
	void pop_front(){
		assert(!empty());
		delete sentinelle->next();
		taille--;
	}
	//Retire le dernier element de la liste
	void pop_back(){
		assert(!empty());
		delete sentinelle->previous();
		taille--;
	}
	//Redefinition de l'operateur d'affectation
	Liste<T>& Liste<T>::operator =(const Liste<T> & liste) {
		if (this != &liste) {
			copierListe(liste);
		}
		return (*this);
	}
	/*
	
	====================================================================
				Const_iterator
	====================================================================
	
	*/

	class const_iterator{

	private:
		Chainon* elementCourant;
		const Liste<T>* list;
		friend class Liste;
		const_iterator(Chainon *element, const Liste<T> *theList){
			list = theList;
			elementCourant=element;
		};


	public:
		~const_iterator(){
		}

		const_iterator & operator ++(void){
			//assert(elementCourant->next()!=list->sentinelle);
			//if (elementCourant == list->sentinelle)
			//	elementCourant = elementCourant->next();
			elementCourant = elementCourant->next();
			return *this;
		}
		const_iterator & operator --(void){
			assert(elementCourant->previous()!=list->sentinelle);
			elementCourant = elementCourant->previous();
			return *this;
		}

		const T & operator *(void) const{
			return elementCourant->data();
		}
		const T * operator ->(void) const{
			return elementCourant->data();
		}

		const bool operator !=(const const_iterator &ite) const{
			return this->elementCourant != ite.elementCourant;
		}

		const bool operator ==(const const_iterator &ite) const{
			return this->elementCourant == ite.elementCourant;
		}

	};

	const_iterator begin(void) const{
		return const_iterator(sentinelle->next(), this);
	}

	const_iterator end(void) const{
		return const_iterator(sentinelle, this);
	}
	/*

	====================================================================
				Iterator
	====================================================================

	*/
	class iterator{

	private:

		Chainon* elementCourant;
		const Liste<T>* list;
		friend class Liste;
		iterator(Chainon *element, const Liste<T> *theList){
			list = theList;
			elementCourant=element;
		};


	public:
		~iterator(){
		}

		iterator & operator ++(void){
			//assert(elementCourant->next()!=list->sentinelle);
			//if(elementCourant==list->sentinelle)
			//	elementCourant = elementCourant->next();
			elementCourant = elementCourant->next();
			return *this;
		}
		iterator & operator --(void){
			assert(elementCourant->previous()!=list->sentinelle);
			elementCourant = elementCourant->previous();
			return *this;
		}

		T & operator *(void) const{
			return elementCourant->data();
		}
		T * operator ->(void) const{
			return elementCourant->data();
		}

		const bool operator !=(const iterator &ite) const{
			return this->elementCourant != ite.elementCourant;
		}

		const bool operator ==(const iterator &ite) const{
			return this->elementCourant == ite.elementCourant;
		}

	};

	iterator begin(void){
		return iterator(sentinelle->next(), this);
	}

	iterator end(void){
		return iterator(sentinelle, this);
	}

	//Methode renvoyant un iterateur sur une valeur cherchée
	iterator find(typename Liste<T>::iterator premier,typename Liste<T>::iterator dernier, const T & x){
		T valeur = x;
		for(Liste<T>::iterator it = premier; it!=dernier; ++it){
			if(valeur==*it)
				return it;
		}
		return dernier;
	}
	//Ajoute une valeur à l'endroit ou l'iterateur pointe
	//Methode renvoyant un iterateur sur la valeur inserée
	iterator insert (iterator position, const T & x){
		position.elementCourant->insertBefore(new Chainon(x));
		taille++;
		return --position;
	}
	//Supprime la valeur à l'endroit ou l'iterateur pointe
	iterator erase(iterator position){
		Chainon* aSuppr = position.elementCourant;
		--position;
		delete aSuppr;
		taille--;
		return position;
	}
	//Prototype de la redefinition de l'operateur <<
	template<typename T> 
	friend std::ostream & operator <<(std::ostream& out, const Liste<T> & list);

	//Redefinition de l'operateur + permettant de concatener 2 listes
	Liste<T> Liste<T>::operator +(const Liste<T> & list) {
		Liste<T> l;
		for (Liste<T>::iterator it = this->begin(); it != this->end(); ++it) {
			T temp = *it;
			l.push_back(temp);
		}
		for (Liste<T>::const_iterator it = list.begin(); it != list.end(); ++it) {
			T temp = *it;
			l.push_back(temp);
		}
		return l;
	}
};
//Redefnition de l'operateur << permettant d'afficher une liste
template <class T>
std::ostream & operator <<(std::ostream& out, const Liste<T> & list) {
	out << "[";
	for (Liste<T>::const_iterator it = list.begin(); it != list.end(); ++it) {
		out << *it << ",";
	}
	out << "]";
	return out;
}
#endif /* LISTE_H_ */
