/*
 * copier.h
 *
 *  Created on: 3 mars 2016
 *      Author: 16009455
 */

#ifndef COPIER_H_
#define COPIER_H_

//pre : la liste doit �tre tri�e dans l'ordre croissant
//return : un iterateur sur le premier element sup�rieur � la valeur pass�e en parametre
template <class T>
typename Liste<T>::iterator fonctionQuiChercheValeur(typename Liste<T>::iterator premier, typename Liste<T>::iterator dernier, const T & x) {
	for (Liste<T>::iterator it = premier; it != dernier; ++it) {
		if (x >= *it)
			return it;
	}
	return dernier;
}


#endif /* COPIER_H_ */
