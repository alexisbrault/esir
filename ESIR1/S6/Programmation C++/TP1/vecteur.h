/** \brief Ce fichier doit contenir la déclaration de la classe vecteur
    Attention, elle ne doit contenir aucune implémentation de méthode / fonction
 */

#ifndef _VECTEUR_H
#define _VECTEUR_H

#include <cassert>
#include <assert.h>     /* assert */


using namespace std;

//Type générique T
template <class  T>


// Déclaration de la classe vecteur
class Vecteur {
private :
	unsigned int m_nbDimension;
	T *ptr_tableau;
public :
	Vecteur(int nbDimension=3, T valeurInitiale = T());

	//Constructeur de copie
	Vecteur(const Vecteur<T> &vecteur);
	~Vecteur();
	T get(unsigned int index)const;
	void set(unsigned int index,T valeur);
	unsigned int dimensions()const;
	Vecteur<T>& operator =(const Vecteur<T>& vecteur);
	Vecteur<T>& operator =(const T& truc);
	Vecteur<T> operator +(const Vecteur<T>& vecteur);
	T & operator[] (unsigned int index);
	void afficherVecteur(const Vecteur<T> *v, std::ostream & out);

private :
	void copierVecteur(const Vecteur &vecteur);
};

//Constructeur
template <class T>
Vecteur<T>::Vecteur(int nbDimension, T valeurInitiale){
	assert(nbDimension>0);
	ptr_tableau = new T[nbDimension];
	m_nbDimension = nbDimension;
	//Initialisation
	for(int i=0;i<nbDimension;i++){
		*(ptr_tableau+i)=valeurInitiale;
	}

	cout <<"nbDimension="<<nbDimension << endl;
	cout <<"valeurInitiale="<<valeurInitiale << endl;
}

//Destructeur
template <class T>
Vecteur<T>::~Vecteur(){
	delete[] ptr_tableau;
}

template <class T>
Vecteur<T>::Vecteur(const Vecteur<T> &vecteur){
	copierVecteur(vecteur);
}

template <class T>
void Vecteur<T>::copierVecteur(const Vecteur<T> &vecteur){
	m_nbDimension = vecteur.dimensions();
	ptr_tableau = new T[m_nbDimension];

	//Initialisation
	for(unsigned int i=0;i<m_nbDimension;i++){
		*(ptr_tableau+i)=vecteur.get(i);
	}
}

template <class T>
Vecteur<T>& Vecteur<T>::operator =(const Vecteur<T> & vecteur){
	if(this != &vecteur){
		if(ptr_tableau != NULL){
			delete[] ptr_tableau;
		}
		copierVecteur(vecteur);
	}
	return (*this);
}

template <class T>
Vecteur<T>& Vecteur<T>::operator =(const T & truc){
	if(this != &truc){
		if(ptr_tableau != NULL){
			delete[] ptr_tableau;
		}
		copierVecteur(truc);
	}
	return (*this);
}

template <class T>
Vecteur<T> Vecteur<T>::operator +(const Vecteur<T> & vecteur){
	assert(this->dimensions()==vecteur.dimensions());
	int dim = this->dimensions();
	Vecteur v(dim);
	for(int i=0;i<dim;i++){
		v.set(i, this->get(i)+vecteur.get(i));
	}
	return v;
}

template <class T>
T Vecteur<T>::get(unsigned int index)const{
	assert(index<m_nbDimension);
	return ptr_tableau[index];
}

template <class T>
void Vecteur<T>::set(unsigned int index,T valeur){
	assert(index<m_nbDimension);
	ptr_tableau[index]=valeur;
}

template <class T>
unsigned int Vecteur<T>::dimensions()const{
	return m_nbDimension;
}

template <class T>
T & Vecteur<T>::operator[](unsigned int index){
	assert(index<m_nbDimension);
	return ptr_tableau[index];
}

//
//template <class T>
//Vecteur<T> * Vecteur<T>::lireVecteur(std::istream & in){
//	int valeurInit;
//	cout << "Entrez une valeur initiale " << endl;
//	in >> valeurInit;
//
//	int dimension;
//	cout << "Entrez une dimension " << endl;
//	in >> dimension;
//
//	return new Vecteur<T>(dimension, valeurInit);
//}
//

template <class T>
T operator *(const Vecteur<T> & v1, const Vecteur<T> & v2){
	assert(v1.dimensions()==v2.dimensions());
	float res = 0;
	for(unsigned int i=0;i<v1.dimensions();i++){
		res += v1.get(i)*v2.get(i);
	}

	return res;
}

template <class T>
std::istream& operator >>(std::istream & in, Vecteur<T> & vecteur){
	int valeurInit;

	for(unsigned int i=0; i<vecteur.dimensions(); i++){
		cout << "Entrez une valeur initiale " << endl;
		in >> valeurInit;
		vecteur.set(i, valeurInit);
	}

	return in;
}

template <class T>
std::ostream& operator <<(std::ostream & out, const Vecteur<T> & vecteur){
	int dimension = vecteur.dimensions();
	for(int i=0; i<dimension; i++){
		out << vecteur.get(i) << " ";
	}
	return out;
}



#endif
