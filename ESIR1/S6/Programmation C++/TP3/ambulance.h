/*
 * ambulance.h
 *
 *  Created on: 10 mars 2016
 *      Author: 16009455
 */

#ifndef AMBULANCE_H_
#define AMBULANCE_H_

class Ambulance:public Auto{
public:
	Ambulance(unsigned int nbp=1,bool t = false);
	~Ambulance(void);
	virtual double calculerTarif(void) const;
	virtual void afficher(std::ostream & s = std::cout) const;
	Ambulance * clone() const;
};


#endif /* AMBULANCE_H_ */
