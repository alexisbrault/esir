/*
 * cycle.cpp
 *
 *  Created on: 10 mars 2016
 *      Author: 16009455
 */

#include <iostream>
#include "cycle.h"
#include "vehicule.h"


Cycle::Cycle():
	Vehicule(1, 1){}

Cycle::~Cycle(void){

}

Cycle * Cycle::clone() const{
	return(new Cycle(*this));
}

double Cycle::calculerTarif(void) const{
	return 20+tarifPassager;
}

void Cycle::afficher(std::ostream & s ) const{
	s<<"La longueur du velo est : "
			<<getLongueur()
			<<". Son nombre de passagers est de "
			<<getPassagers();
}


