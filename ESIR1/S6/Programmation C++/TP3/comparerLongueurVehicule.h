#ifndef COMPARER_LONGUEUR_H_
#define COMPARER_LONGUEUR_H_

class comparerLongueurVehicule :public comparerVehicule {
public:
	comparerLongueurVehicule(bool a_croissant=true):comparerVehicule(a_croissant){};
	virtual bool operator ()(const Vehicule *a, const Vehicule *b){
		return comparerVehicule::compare(a->getLongueur(),b->getLongueur());
	}
};

#endif
