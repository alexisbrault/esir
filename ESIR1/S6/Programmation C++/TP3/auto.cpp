/*
 * Auto.cpp
 *
 *  Created on: 8 mars 2016
 *      Author: 16009455
 */
#include <iostream>
#include "auto.h"
#include "vehicule.h"


Auto::Auto(unsigned int nbp, bool t):
	Vehicule(2, nbp),type(t){}

Auto::~Auto(void){

}

//Methode de clonage des objets, le constructeur de copie de marchant pas en polymorphisme
Auto * Auto::clone() const{
	return(new Auto(*this));
}

double Auto::calculerTarif(void) const{
	return getPassagers()*tarifPassager+((type) ? 350 : 100);
}

void Auto::afficher(std::ostream & s ) const{
	s<<"La longueur de la voiture est : "
			<<getLongueur()
			<<". Son nombre de passagers est de "
			<<getPassagers()
			<<". Son type est "
			<<(type?"Tout terrain":"Autre");
}



