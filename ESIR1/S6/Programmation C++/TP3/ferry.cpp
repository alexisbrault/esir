/*
 * ferry.cpp
 *
 *  Created on: 8 mars 2016
 *      Author: 16009455
 */
#include "ferry.h"
#include <vector>
#include <algorithm>
#include "vehicule.h"

Ferry::Ferry (unsigned int longueur, unsigned int personnes):
longueur(longueur), personnes(personnes), longueurActuelle(0), nbPersonnesActuel(0){
	listeVehicule = new liste();
}

Ferry::~Ferry(){
	::std::for_each(listeVehicule->begin(),listeVehicule->end(),
		[](const Vehicule * v){delete v;});
	delete listeVehicule;
}

bool Ferry::ajouter(const Vehicule * pv){
	if(longueur>=longueurActuelle+pv->getLongueur() && personnes>=nbPersonnesActuel+pv->getPassagers()){
		Vehicule * v = pv->clone();
		listeVehicule->push_back(v);
		longueurActuelle += pv->getLongueur();
		personnes += pv->getPassagers();
		return true;
	}
	return false;
}

double Ferry::calculerTarif(void) const{
	double total = 0.0;
	::std::for_each(listeVehicule->begin(),
					listeVehicule->end(),
					[&total](const Vehicule * v)
					{total+=v->calculerTarif();});
	return total;
}

void Ferry::afficher(std::ostream & s) const{
	::std::for_each(listeVehicule->begin(),
					listeVehicule->end(),
					[&s](const Vehicule * v)
					{s << *v<< std::endl;});
}

std::ostream & operator << (std::ostream & sortie, const Ferry & ferry){
	ferry.afficher(sortie);
	return sortie;
}

// Q3
//void Ferry::trier(bool comp){
//
//	//Version Lambda
//
//	/*auto comparateur = [&comp](const Vehicule* a, const Vehicule* b) {
//		if (comp)
//			return a->getLongueur() < b->getLongueur();
//		else
//			return a->getLongueur() > b->getLongueur();
//	};
//
//	std::sort(listeVehicule->begin(), listeVehicule->end(), comparateur);*/
//
//	std::sort(listeVehicule->begin(), listeVehicule->end(),comparerLongueurVehicule(comp));
//}
