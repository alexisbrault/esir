/*
 * Cycle.h
 *
 *  Created on: 8 mars 2016
 *      Author: 16009455
 */

#ifndef CYCLE_H_
#define CYCLE_H_
#include "vehicule.h"


class Cycle:public Vehicule{
public:
	Cycle();
	~Cycle(void);
	virtual double calculerTarif(void) const;
	virtual void afficher(std::ostream & s = std::cout) const;
	Cycle * clone() const;
};


#endif /* AUTO_H_ */
