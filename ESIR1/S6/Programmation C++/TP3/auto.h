/*
 * Auto.h
 *
 *  Created on: 8 mars 2016
 *      Author: 16009455
 */

#ifndef AUTO_H_
#define AUTO_H_
#include "vehicule.h"


class Auto:public Vehicule{
public:
	Auto(unsigned int nbp=1,bool t = false);
	~Auto(void);
	virtual double calculerTarif(void) const;
	virtual void afficher(std::ostream & s = std::cout) const;
	Auto * clone() const;
protected:
	//True = toutTerrain
	//False	= autre
	bool type;
};


#endif /* AUTO_H_ */
