// -*- c++ -*-

#ifndef _FERRY_H
#define _FERRY_H
// choisir selon le conteneur utilisé
//#include <vector>
//#include <deque>
//#include <list>
#include "vehicule.h"
#include <vector>
#include <algorithm>

/**
 * Un ferry transporte des véhicules
 */
class Ferry {
	typedef std::vector<const Vehicule*> liste;
public:
  /**
     constructeur : initialiser un ferry vide
     @param longueur  : capacité du ferry en unités de longueur
     @param personnes : capacité du ferry en nombre de passagers
  */
  Ferry (unsigned int longueur, unsigned int personnes);

  /// destructeur
  virtual ~Ferry(void);

  /** ajouter un véhicule dans le ferry.
      sans effet s'il n'y a plus de place
      @param pv : désigne le véhicule à ajouter
      @return vrai si l'ajout a eu lieu, faux sinon
  */
  virtual bool   ajouter(const Vehicule * pv);

  //! déterminer le tarif de l'ensembles des véhicules présents dans le ferry
  virtual double calculerTarif(void) const;

  //! afficher les informations sur le contenu du ferry
  virtual void   afficher(std::ostream & s = std::cout) const;


  //void trier(bool croissant=true);

  template <class TComparateur>
  void trier(TComparateur c = TComparateur()){
	  std::sort(listeVehicule->begin(),listeVehicule->end(),c);
  }

private :
  //Polymorphisme donc il faut ajouter un pointeur pour le type Vehicule.
  liste *listeVehicule;

  unsigned int longueur;
  unsigned int personnes;
  unsigned int longueurActuelle;
  unsigned int nbPersonnesActuel;

};

// opérateur d'affichage
std::ostream & operator << (std::ostream & sortie, const Ferry & ferry);


#endif // _FERRY_H
