/*
 * ambulance.cpp
 *
 *  Created on: 10 mars 2016
 *      Author: 16009455
 */

#include <iostream>
#include "auto.h"
#include "ambulance.h"


Ambulance::Ambulance(unsigned int nbp, bool t):
	Auto(nbp,t){}

Ambulance::~Ambulance(void){

}

Ambulance * Ambulance::clone() const{
	return(new Ambulance(*this));
}

double Ambulance::calculerTarif(void) const{
	return 0.0;
}

void Ambulance::afficher(std::ostream & s ) const{
	s<<"La longueur de l'ambulance est : "
			<<getLongueur()
			<<". Son nombre de passagers est de "
			<<getPassagers()
			<<". Son type est "
			<<(type?"Tout terrain":"Autre");
}


