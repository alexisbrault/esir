/*
 * bus.cpp
 *
 *  Created on: 8 mars 2016
 *      Author: 16009455
 */

#include <iostream>
#include "bus.h"
#include "vehicule.h"


Bus::Bus(unsigned int lg, unsigned int nbp):
	Vehicule(lg, nbp){}

Bus::~Bus(void){

}

Bus * Bus::clone() const{
	return(new Bus(*this));
}

double Bus::calculerTarif(void) const{
	return getPassagers()*tarifPassager+200+50*getLongueur();
}

void Bus::afficher(std::ostream & s ) const{
	s<<"La longueur du bus est : "
			<<getLongueur()
			<<". Son nombre de passagers est de "
			<<getPassagers();
}





