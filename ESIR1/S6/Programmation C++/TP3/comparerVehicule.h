#ifndef COMPARER_VEHICULE_H_
#define COMPARER_VEHICULE_H_

class comparerVehicule {//:public std::binary_function<Vehicule, Vehicule, bool>{
protected:
	bool croissant;

	template <class T>
	bool compare(T t1, T t2){
		if(croissant)
			return t1<t2;
		else
			return t1>t2;
	}
public:
	comparerVehicule(bool a_croissant=true):croissant(a_croissant){};
	virtual bool operator ()(const Vehicule *a, const Vehicule *b)=0;


	//virtual ~comparerVehicule();
};
#endif
