#ifndef COMPARER_PASSAGER_H_
#define COMPARER_PASSAGER_H_

class comparerPassagerVehicule :public comparerVehicule {//:public std::binary_function<Vehicule, Vehicule, bool>{
public:
	comparerPassagerVehicule(bool a_croissant=true):comparerVehicule(a_croissant){};
	virtual bool operator ()(const Vehicule *a, const Vehicule *b){
		return comparerVehicule::compare(a->getPassagers(),b->getPassagers());
	}
};

#endif
