// -*- c++ -*-
#ifndef _VEHICULE_H
#define _VEHICULE_H

#include <iostream>
/**
 * définition abstraite d'un Vehicule
 */

class Vehicule {
public:
  /** @param lg : longueur du véhicule
      @param nbp : nombre de personnes transportées
  */
  Vehicule(unsigned int lg = 1, unsigned int nbp = 1);

  /// destructeur
  virtual ~Vehicule(void);

  //! déterminer le tarif du véhicule
  //Chaque vehicule va avoir une tarification différentes.
  virtual double	calculerTarif(void) const = 0;

  //! afficher les caractéristiques du véhicule
  //Chaque véhicule va avoir un nom ainsi que des caractéristiques différentes.
  virtual void		afficher(std::ostream & s = std::cout) const = 0;

  //! déterminer la longueur d'un véhicule
  //Chaque véhicule va avoir une longueur.
  unsigned int		getLongueur(void) const;

  /// déterminer le nombre de personnes dans le véhicule
  //Chaque véhicule va avoir un nombre de passagers.
  unsigned int		getPassagers() const;

  //Constructeur de copie
  Vehicule(const Vehicule &v);
  virtual Vehicule * clone() const=0;

private:
  unsigned int longueur;		///< longueur du véhicule
  unsigned int passagers;		///< nombre de personnes dans le véhicule
protected:
  static const int tarifPassager = 15; ///< tarif passager
};

// opérateur << pour la classe Vehicule
std::ostream & operator << (std::ostream & sortie, const Vehicule & v);

#endif // _VEHICULE_H


