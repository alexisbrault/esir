/*
 * bus.h
 *
 *  Created on: 8 mars 2016
 *      Author: 16009455
 */

#ifndef BUS_H_
#define BUS_H_
#include "vehicule.h"

class Bus:public Vehicule{
public:
	Bus(unsigned int lg=2, unsigned int nbp=1);
	~Bus(void);
	virtual double calculerTarif(void) const;
	virtual void afficher(std::ostream & s = std::cout) const;
	Bus * clone() const;
};

#endif /* BUS_H_ */
