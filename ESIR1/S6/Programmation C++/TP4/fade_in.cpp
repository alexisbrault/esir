#include "fade_in.h"

fade_in::fade_in(double debut, double duree):fade(debut,duree){
	std::function<double()> in = [this]() { return double(i) / double(i_max); };
	fct = in;
}