#include "imp_flot.h"
#include <memory>
#include <deque>
#include "assert.h"
// spécification d'un flot d'échantillons

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \class	imp_flot
///
/// \brief	Implémentation de la classe abstraite flot
///
/// \author	Alexis BRAULT
/// \date	16/03/2016
////////////////////////////////////////////////////////////////////////////////////////////////////

	//Constructeur
	imp_flot::imp_flot():flot(){	}
	//Destructeur
	imp_flot::~imp_flot(){	}
	//
	void imp_flot::inserer(double echantillon){
		deque.push_back(echantillon);
	}

	//@pre : deque non vide
	double imp_flot::extraire(){
		assert(!vide());
		double back = deque.back();
		deque.pop_back();
		return back;
	}

	bool imp_flot::vide() const{
		return deque.empty();
	}
