#ifndef __OPERATION__BINAIRE__H__
#define __OPERATION__BINAIRE__H__

template <class T>
class operation_binaire :
	 public virtual filtre_base {
private:
	T m_t;
public:	
	operation_binaire(): filtre_base::filtre_base(2, 1) {	}
	void operator()() const{
		const std::shared_ptr<flot> e1 = consommateur_base::getEntree(0);
		const std::shared_ptr<flot> e2 = consommateur_base::getEntree(1);
		double d1 = (*e1).extraire();
		double d2 = (*e2).extraire();
		T operation;
		double produit = operation(d1,d2);
		const std::shared_ptr<flot> & s = producteur_base::getSortie(0);
		(*s).inserer(produit);
	}
	void calculer() {
	/*	const std::shared_ptr<flot> e1 = val1.getEntree(0);
		const std::shared_ptr<flot> e2 = val2.getEntree(0);
		double d1 = (*e1).extraire();
		double d2 = (*e2).extraire();
		double produit = t(d1*d2);
		const std::shared_ptr<flot> & s = producteur_base::getSortie(0);
		(*s).inserer(produit);*/
	}
	
};

#endif