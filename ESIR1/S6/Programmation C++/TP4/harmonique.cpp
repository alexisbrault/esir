#include "harmonique.h"
#include "math.h"
#include "constantes.h"
#include "imp_flot.h"

harmonique::harmonique(int f, double phi):producteur_base(1),m_f(f),m_phi(phi),i(0){
	
}

harmonique::~harmonique() {

}

void harmonique::calculer() {
	double r = sin(((double)i / MixageSonore::frequency) * 2.0 * MixageSonore::pi*((double)m_f) + m_phi);
	getSortie(0)->inserer(r);
	i++;
}
