#ifndef MULTIPLICATEUR_H
#define MULTIPLICATEUR_H

#include"filtre_base.h"

class multiplicateur : public virtual filtre_base {
public:
	multiplicateur();
	virtual ~multiplicateur();
	void calculer();
};

#endif /* MULTIPLICATEUR_H */