#ifndef __CONSOMMATEUR__BASE__H__
#define __CONSOMMATEUR__BASE__H__


#include "consommateur.h"
#include <vector>

/*CLasse du conteneur consommateur*/
class consommateur_base : public virtual consommateur {
private:
	/*Les entr�es du consommateur*/
	std::vector<std::shared_ptr<flot>> m_lesEntrees;

protected:
	

public:
	/*Constrcteur de la classe*/
	consommateur_base(int nbEntrees);
	/*Destructeur*/
	virtual ~consommateur_base();
	/*Retourne le nombre d'entr�e du consommateur*/
	virtual unsigned int nbEntrees() const;
	/*Retourne un shared_ptr de flot � l'indice demand�*/
	virtual const std::shared_ptr<flot> & getEntree(unsigned int numentree) const;
	virtual void connecterEntree(const std::shared_ptr<flot> & f, unsigned int numentree);
	/*Methode permettant de savoir s'il existe ou non des entr�es*/
	virtual bool yaDesEchantillons() const;
	
};

#endif