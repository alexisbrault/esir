#ifndef MIXEUR_H
#define MIXEUR_H

#include "filtre_compose.h"
#include <vector>

/*Classe du mixeur*/
class mixeur : public virtual filtre_compose {

private:
	std::vector<std::shared_ptr<producteur>> m_producteur;
public:
	mixeur(unsigned int nbEntree, std::vector<double> & v);
	virtual void calculer();


};














#endif