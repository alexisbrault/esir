#include "multiplicateur.h"
#include "filtre_base.h"
#include "assert.h"
#include "imp_flot.h"

multiplicateur::multiplicateur()
	:filtre_base::filtre_base(2, 1){

}

multiplicateur::~multiplicateur() {

}

void multiplicateur::calculer() {
	assert(consommateur_base::yaDesEchantillons());

	const std::shared_ptr<flot> e1 = consommateur_base::getEntree(0);
	const std::shared_ptr<flot> e2 = consommateur_base::getEntree(1);
	double d1 = (*e1).extraire();
	double d2 = (*e2).extraire();
	double produit = d1 * d2;
	const std::shared_ptr<flot> & s= producteur_base::getSortie(0);
	//
	(*s).inserer(produit);
}