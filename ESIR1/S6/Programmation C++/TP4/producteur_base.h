#ifndef __PRODUCTEUR__BASE__H__
#define __PRODUCTEUR__BASE__H__

#include "producteur.h"
#include <memory>
#include <vector>

class producteur_base : public virtual producteur {
private:
	std::vector<std::shared_ptr<flot>> m_lesSorties;
protected:
	
public:
	producteur_base(int nbSorties=1);
	~producteur_base();
	virtual unsigned int nbSorties() const;
	virtual const std::shared_ptr<flot> & getSortie(unsigned int numsortie) const;
	virtual void connecterSortie(const std::shared_ptr<flot> & f, unsigned int numSortie);
};

#endif