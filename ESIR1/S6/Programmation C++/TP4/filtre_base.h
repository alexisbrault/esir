#ifndef __FILTRE__BASE__H__
#define __FILTRE__BASE__H__

#include "filtre.h"
#include "consommateur_base.h"
#include "producteur_base.h"

/*Classe �tant � la fois un consommateur et aussi un producteur*/
class filtre_base : public virtual filtre,public consommateur_base,public producteur_base {
public:
	/*Constructeur contenant le nombre d'entr�es et le nombre de sorties*/
	filtre_base(int nbEntrees, int nbSorties);
	/*Destructeur*/
	~filtre_base();
};


#endif
