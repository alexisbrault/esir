#include "producteur_base.h"
#include <memory>
#include <vector>
#include "assert.h"
#include "imp_flot.h"

producteur_base::producteur_base(int nbSorties){
	for (unsigned int i = 0; i < nbSorties; i++) {
		const std::shared_ptr<flot> ptr(new imp_flot());
		m_lesSorties.push_back(ptr);
	}
}
producteur_base::~producteur_base() {

}

unsigned int producteur_base::nbSorties() const {
	return m_lesSorties.size();
}
const std::shared_ptr<flot> & producteur_base::getSortie(unsigned int numsortie) const {
	return m_lesSorties[numsortie];
}
void producteur_base::connecterSortie(const std::shared_ptr<flot> & f, unsigned int numSortie) {
	m_lesSorties[numSortie] = f;
}
