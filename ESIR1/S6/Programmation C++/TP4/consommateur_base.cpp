#include "consommateur_base.h"
#include <memory>
#include <vector>
#include "assert.h"

consommateur_base::consommateur_base(int nbEntrees) {
	for (unsigned int i = 0; i < nbEntrees; i++) {
		m_lesEntrees.push_back(nullptr);
	}
}
consommateur_base::~consommateur_base() {

}

unsigned int consommateur_base::nbEntrees() const {
	return m_lesEntrees.size();
}
const std::shared_ptr<flot> & consommateur_base::getEntree(unsigned int numsortie) const {
	return m_lesEntrees[numsortie];
}
void consommateur_base::connecterEntree(const std::shared_ptr<flot> & f, unsigned int numSortie) {
	m_lesEntrees[numSortie] = f;
}

bool consommateur_base::yaDesEchantillons() const {
	return !m_lesEntrees.empty();
}
