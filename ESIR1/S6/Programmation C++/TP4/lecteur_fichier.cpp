#include "lecteur_fichier.h"
#include "exception_lecture.h"
#include <iostream>
#include <fstream>

lecteur_fichier::lecteur_fichier(std::string filename, unsigned int outputs):m_outputs(outputs),producteur_base(outputs) {
	m_file.open(filename, std::ios::in | std::ios::binary);
	if (!(m_file.is_open()))
		throw exception_lecture("Fichier introuvable");
}

lecteur_fichier::~lecteur_fichier() {
	if (m_file.is_open())
		m_file.close();
}

void lecteur_fichier::calculer() {

	if (m_file.is_open() == true){
		for (int i = 0; i < m_outputs; i++) {
			short int p = (lire() << 8) | lire();
			double d = (double)p/32768.0;
			getSortie(i)->inserer(d);
		}
		
		if(m_file.eof())
			m_file.close();
	}
	else {
		throw exception_lecture("Fin de fichier");
	}
}

char lecteur_fichier::lire() {
	char buff;
	m_file.read(&buff, sizeof(char));
	return buff;
}