#ifndef FADE_H
#define FADE_H

#include "filtre_base.h"
#include <functional>

/*Classe m�re de la classe fade In et fade Out*/
class fade : public filtre_base {
private :
	//Variables de placement au bon �chantillon
	unsigned int n = 0;
	unsigned int n_max;
	
protected:
	//Variables utilis�es pour la pente du fade
	unsigned int i = 0;
	unsigned int i_max;

	bool debut = false, fin = false;

	std::function<double()> fct;
public:
	/*Constrcteur de la classe comprenant le debut du filtre et sa dur�e*/
	fade(double debut, double duree);
	void virtual calculer();
};

#endif