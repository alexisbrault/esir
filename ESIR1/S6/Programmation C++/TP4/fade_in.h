#ifndef FADE_IN_H
#define FADE_IN_H

#include "fade.h"

/*Classe du filtre fadeIn*/
class fade_in :public virtual fade {
public:
	/*Constructeur de la classe contenant le debut du filtre et sa dur�e*/
	fade_in(double debut, double duree) :fade(debut, duree) {
		std::function<double()> in = [this]() { return double(i) / double(i_max); };
		fct = in;
	}
};

#endif