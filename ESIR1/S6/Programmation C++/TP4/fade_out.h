#ifndef FADE_OUT_H
#define FADE_OUT_H

#include "fade.h"

/*Classe du filtre fadeOut*/
class fade_out :public virtual fade {
public:
	/*Constructeur de la classe contenant le debut du filtre et sa dur�e*/
	fade_out(double debut, double duree) :fade(debut, duree) {
		std::function<double()> out = [this]() { return 1-double(i) / double(i_max); };
		fct = out;
	}
};

#endif // !FADE_OUT_H