#include "filtre_compose.h"
#include "filtre_base.h"
#include <vector>

filtre_compose::filtre_compose(int nbEntrees, int nbSorties):filtre_base(nbEntrees,nbSorties) {
	std::vector<filtre_base> m_entrees;
}

void filtre_compose::calculer() {
	std::vector<std::shared_ptr<composant>>::iterator it;
	for (it = m_composants.begin(); it < m_composants.end(); it++) {
		(*(*it)).calculer();
	}
}

void filtre_compose::ajouter(std::shared_ptr<composant> composant) {
	m_composants.push_back(composant);
}


void filtre_compose::connecterEntree(const std::shared_ptr<flot> & f, unsigned int numEntree) {

	std::vector<std::pair<int, std::pair<std::shared_ptr<consommateur>, int>>>::iterator it;
	for (it = m_entrees.begin(); it < m_entrees.end(); it++) {
		if ((*it).first == numEntree) {
			std::shared_ptr<consommateur> composant = ((*it).second).first;
			int numEntreeComposant = ((*it).second).second;

			(*composant).connecterEntree(f, numEntreeComposant);

			break;
		}
	}
}

void filtre_compose::connecterEntreeComposant(int numEntree, std::shared_ptr<consommateur> consommateur, int numEntreeComposant) {
	m_entrees.push_back(std::make_pair(numEntree, std::make_pair(consommateur,numEntreeComposant)));
}

//void filtre_compose::connecterSortie(const std::shared_ptr<flot> & f, unsigned int numSortie) {
//
//	std::vector<std::pair<int, std::pair<int, int>>>::iterator it;
//	for (it = m_sorties.begin(); it < m_sorties.end(); it++) {
//		if ((*it).first == numSortie) {
//			int numComposant = ((*it).second).first;
//			int numSortieComposant = ((*it).second).second;
//			if (producteur_base v = dynamic_cast<producteur_base>(m_composants[numComposant])) {
//				// old was safely casted to NewType
//				v.connecterSortie(f, numSortieComposant);
//			}
//			//((producteur_base)(*m_composants[numComposant])).connecterSortie(f, numSortieComposant);
//
//			break;
//		}
//	}
//}

//void filtre_compose::connecterSortieComposant(int numComposant, int numSortieComposant, int numSortie) {
//	m_sorties.push_back(std::make_pair(numSortie, std::make_pair(numComposant, numSortieComposant)));
//}