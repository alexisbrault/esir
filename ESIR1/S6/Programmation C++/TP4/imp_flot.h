#include "flot.h"
#include <memory>
#include <deque>


// spécification d'un flot d'échantillons

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \class	imp_flot
///
/// \brief	Implémentation de la classe abstraite flot
///
/// \author	Alexis BRAULT
/// \date	16/03/2016
////////////////////////////////////////////////////////////////////////////////////////////////////
class imp_flot:public flot {

private:
	//std::shared_ptr<std::deque<double>> deque;
	std::deque<double> deque;

public :
	//Constructeur
	imp_flot();
	//Destructeur
	virtual ~imp_flot();
	//
	virtual void inserer(double echantillon);

	virtual double extraire();

	virtual bool vide() const;
};
