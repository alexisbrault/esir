#ifndef ECHO_H
#define ECHO_H

#include "filtre_base.h"
#include "operation_binaire.h"
#include <algorithm>
#include <deque>

/*Classe du filtre d'echo*/
class echo : public filtre_base {
private:
	operation_binaire<std::multiplies<double>> additionneur;
	std::deque<double> *memoire;
	unsigned int i_max;
	unsigned int i_max2;
	unsigned int n_max;
	unsigned int n = 0;
	unsigned int i = 0;
	bool enregistrement = true;
	bool debut = false, fin = false;


public:
	/*Constrcteur de la classe avec le debut de l'echo aisni que �a dur�e*/
	echo(double debut=0.0, double duree=2.0);
	/*Destructeur de la classe*/
	~echo();
	/**/
	virtual void calculer();
};





#endif

