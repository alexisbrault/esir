#ifndef COMPRESSION_H
#define COMPRESSION_H

#include "filtre_base.h"

/*
*Classe permettant de faire le filtre de compression
*/
class compression : public filtre_base {
private:
	double m_puissance;
public:
	/*Constrcteur de la classe*/
	compression(double puissance);
	/*Calcul pour effectuer la compression gr�ce � l'attribut m_puissance*/
	void virtual calculer();
};

#endif // !COMPRESSION_H
