#include "fade.h"
#include "constantes.h"
#include "signal_constant.h"
#include "multiplicateur.h"

fade::fade(double debut, double duree):filtre_base(1,1) {
	i_max = duree * MixageSonore::frequency;
	n_max = debut * MixageSonore::frequency;
}

void fade::calculer() {
	//on se place � l'endroit ou le fade commence
	if (n < n_max) {
		n++;
		return;
	}
	else {
		debut = true;
		//On commence le fade
		if (i < i_max)
			i++;
		else {
			//on finit le fade
			fin = true;
			return;
		}
	}
	
	double a;
	if (debut && (!fin)) {
		//ici on calcule le fade ( la pente )
		//fct est d�fnie dans les classes filles de fade
		double v = fct();		
		signal_constant s(v);
		multiplicateur m;
		m.connecterEntree(s.getSortie(0), 0);
		m.connecterEntree(getEntree(0), 1);
		s.calculer();
		m.calculer();

		a = m.getSortie(0)->extraire();
		getSortie(0)->inserer(a);
	}
	else {
		//Sinon on imprime le signal tel qu'il �tait avant
		a = getEntree(0)->extraire();
	}
	getSortie(0)->inserer(a);
}