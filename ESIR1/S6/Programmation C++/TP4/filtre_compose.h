#ifndef FILTRE_COMPOSE_H
#define FILTRE_COMPOSE_H
#include "filtre_base.h"
#include <utility>


class filtre_compose : public filtre_base {
private :
	std::vector<std::pair<int, std::pair<std::shared_ptr<consommateur>, int>>> m_entrees;
protected:
	std::vector<std::shared_ptr<composant>> m_composants;
	//numEntree correspond � l'entree qu'on souhaite brancher
	void connecterEntreeComposant(int numEntree, std::shared_ptr<consommateur> consommateur, int numEntreeComposant);
	//Ajouter le composant pour le calculer, attention � l'ordre
	void ajouter(std::shared_ptr<composant> composant);
public:
	filtre_compose(int nbEntrees, int nbSorties);
	virtual void calculer();
	virtual void connecterEntree(const std::shared_ptr<flot> & f, unsigned int numEntree);
};



#endif
