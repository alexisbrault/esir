#include "volume_compose.h"
#include "multiplicateur.h"
#include "signal_constant.h"

volume_compose::volume_compose(double v):filtre_compose(1,1){
	signal_constant * signal = new signal_constant(v);

	std::shared_ptr<producteur_base> s(signal);
	std::shared_ptr<filtre_base> m(new multiplicateur);
	
	(*m).connecterEntree(signal->getSortie(0),0);
	connecterSortie((*m).getSortie(0),0);
	ajouter(s);
	ajouter(m);
	connecterEntreeComposant(0, m, 1);
}

