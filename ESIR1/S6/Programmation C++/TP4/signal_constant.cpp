/*
 * signal_constant.h
 *
 *  Created on: 16 mars 2016
 *      Author: 16003463
 */

#include "signal_constant.h"
#include <memory>
#include "imp_flot.h"

//Constructeur
signal_constant::signal_constant(double valeur):m_valeur(valeur){
}
//Destructeur
signal_constant::~signal_constant() {

}
unsigned int signal_constant::nbSorties() const{
	return 1; 
}

void signal_constant::calculer(){
	getSortie(0)->inserer(m_valeur);
}
