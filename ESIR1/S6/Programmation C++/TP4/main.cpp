/*
 * main.cpp
 *
 *  Created on: 8 mars 2013
 *      Author: engel
 */

#include "constantes.h"
#include "enregistreur_fichier.h"
#include "enregistreur_fichier_texte.h"
#include "harmonique.h"
#include "signal_constant.h"
#include "multiplicateur.h"
#include "operation_binaire.h"
#include "volume.h"
#include "volume_compose.h"
#include "mixeur.h"
#include "lecteur_fichier.h"

/*   FILTRES     */
#include "fade_in.h"
#include "fade_out.h"
#include "panning.h"
#include "compression.h"
#include "echo.h"

void q2_signal_constant(){
  signal_constant constant(0.5);
  enregistreur_fichier_texte enregistreur("02_constant.txt", 1);
  enregistreur.connecterEntree(constant.getSortie(0), 0);
  // générer des valeurs
  for (unsigned int i = 0; i < 50; ++i) {
    constant.calculer();
    enregistreur.calculer();
  }
}

void q4_harmonique(){
  harmonique la440(880); // la 440Hz (voir fr.wikipedia.org/wiki/Note_de_musique)
  enregistreur_fichier enregistreur("04_harmonique.raw", 1);	// fichier mono
  enregistreur.connecterEntree(la440.getSortie(0), 0);
  // produire 2 secondes de son
  for (unsigned long int i = 0; i < 2 * MixageSonore::frequency; ++i) {
    la440.calculer();
    enregistreur.calculer();
  }
}

void note() {
	int v_note = 880;
	harmonique note(v_note);
	enregistreur_fichier enregistreur("0_note.raw", 1);	// fichier mono
	enregistreur.connecterEntree(note.getSortie(0), 0);
	// produire 2 secondes de son
	for (unsigned long int i = 0; i < 2 * MixageSonore::frequency; ++i) {
		note.calculer();
		enregistreur.calculer();
	}
}

void q9_multiplicateur() {
	harmonique la440(440);
	harmonique la880(880);
	multiplicateur m;
	enregistreur_fichier enregistreur("09_multiplicateur.raw", 1);
	
	m.connecterEntree(la440.getSortie(0),0);
	m.connecterEntree(la880.getSortie(0),1);
	
	enregistreur.connecterEntree(m.getSortie(0), 0);

	for (unsigned long int i = 0; i < 2 * MixageSonore::frequency; ++i) {
		la440.calculer();
		la880.calculer();
		m.calculer();
		enregistreur.calculer();
	}
}

void q10() {
	
	harmonique la440(440);
	harmonique la880(880);
	operation_binaire<std::multiplies<double>> op;
	op.connecterEntree(la440.getSortie(0), 0);
	op.connecterEntree(la880.getSortie(0), 1);
	enregistreur_fichier enregistreur("10_multiplicateur.raw", 1);
	enregistreur.connecterEntree(op.getSortie(0), 0);

	for (unsigned long int i = 0; i < 2 * MixageSonore::frequency; ++i) {
		la440.calculer();
		la880.calculer();
		op();
		enregistreur.calculer();
	}

}

void q12() {
	harmonique la440(440);
	volume v(0.5);
	enregistreur_fichier enregistreur("12_volume.raw", 1);

	v.connecterEntree(la440.getSortie(0), 0);

	enregistreur.connecterEntree(v.getSortie(0), 0);

	for (unsigned long int i = 0; i < 2 * MixageSonore::frequency; ++i) {
		la440.calculer();
		v.calculer();
		enregistreur.calculer();
	}
}

void q14() {
	harmonique la440(440);
	volume_compose v(0.1);
	enregistreur_fichier enregistreur("13_volume_compose.raw", 1);

	v.connecterEntree(la440.getSortie(0), 0);

	enregistreur.connecterEntree(v.getSortie(0), 0);

	for (unsigned long int i = 0; i < 2 * MixageSonore::frequency; ++i) {
		la440.calculer();
		v.calculer();
		enregistreur.calculer();
	}
}

void q15() {
	harmonique la440(440);
	harmonique la880(880);

	std::vector<double> v;
	v.push_back(0.1);
	v.push_back(0.5);

	mixeur m(2, v);
	enregistreur_fichier enregistreur("15_volume_mixeur.raw", 1);

	m.connecterEntree(la440.getSortie(0), 0);
	m.connecterEntree(la880.getSortie(0), 1);

	enregistreur.connecterEntree(m.getSortie(0), 0);

	for (unsigned long int i = 0; i < 2 * MixageSonore::frequency; ++i) {
		la440.calculer();
		la880.calculer();
		m.calculer();
		enregistreur.calculer();
	}
}



void q17() {
	try {
		std::string s1 = "D:/Mes Documents/Cours/ESIR/PROG_CPP/tp13/raw/mono2.raw";
		lecteur_fichier f1(s1, 1);

		std::string s2 = "D:/Mes Documents/Cours/ESIR/PROG_CPP/tp13/raw/stereo.raw";
		lecteur_fichier f2(s2, 2);

		enregistreur_fichier enregistreur("17_lecteur_fichier.raw", 2);

		//enregistreur.connecterEntree(f1.getSortie(0), 0);

		enregistreur.connecterEntree(f2.getSortie(0), 0);
		enregistreur.connecterEntree(f2.getSortie(1), 1);

		for (unsigned long int i = 0; i < 10 * MixageSonore::frequency; ++i) {
			//f1.calculer();
			f2.calculer();

			enregistreur.calculer();
		}
	}
	catch (std::exception const& e){
		std::cerr << "ERREUR : " << e.what() << std::endl;
	}
	
}

void q_fade_in() {
	fade_in f(1, 1);
	harmonique la440(440);
	f.connecterEntree(la440.getSortie(0), 0);
	enregistreur_fichier enregistreur("fade_in.raw", 1);
	enregistreur.connecterEntree(f.getSortie(0), 0);
	for (unsigned long int i = 0; i < 3 * MixageSonore::frequency; ++i) {
		la440.calculer();
		f.calculer();
		enregistreur.calculer();
	}
}

void q_fade_out() {
	fade_out f(1, 1);
	harmonique la440(440);
	f.connecterEntree(la440.getSortie(0), 0);
	enregistreur_fichier enregistreur("fade_out.raw", 1);
	enregistreur.connecterEntree(f.getSortie(0), 0);
	for (unsigned long int i = 0; i < 3 * MixageSonore::frequency; ++i) {
		la440.calculer();
		f.calculer();
		enregistreur.calculer();
	}
}

void q_panning() {
	panning f(0, 3);
	harmonique la440(440);
	harmonique la880(880);
	f.connecterEntree(la440.getSortie(0), 0);
	f.connecterEntree(la880.getSortie(0), 1);
	enregistreur_fichier enregistreur("panning.raw", 2);
	enregistreur.connecterEntree(f.getSortie(0), 0);
	enregistreur.connecterEntree(f.getSortie(1), 1);
	for (unsigned long int i = 0; i < 3 * MixageSonore::frequency; ++i) {
		la440.calculer();
		la880.calculer();
		f.calculer();
		enregistreur.calculer();
	}
}

void q_compression() {
	compression f(0.1);
	harmonique la440(440);
	f.connecterEntree(la440.getSortie(0), 0);
	enregistreur_fichier enregistreur("compression.raw", 1);
	enregistreur.connecterEntree(f.getSortie(0), 0);
	for (unsigned long int i = 0; i < 3 * MixageSonore::frequency; ++i) {
		la440.calculer();
		f.calculer();
		enregistreur.calculer();
	}
}

void q_echo() {

	std::string s2 = "D:/Mes Documents/Cours/ESIR/PROG_CPP/tp13/raw/stereo.raw";
	harmonique la(440);
	lecteur_fichier f2(s2, 2);

	echo e(1, 1);

	enregistreur_fichier enregistreur("echo.raw", 1);

	e.connecterEntree(f2.getSortie(0), 0);

	enregistreur.connecterEntree(e.getSortie(0), 0);

	for (unsigned long int i = 0; i < 4 * MixageSonore::frequency; ++i) {
		f2.calculer();
		e.calculer();
		enregistreur.calculer();
	}
}


int main(){
	/*q2_signal_constant();
	q4_harmonique();
	note();
	q9_multiplicateur();
	q10();
	q12();
	q14();
	q15();
	q17();
	q_fade_in();
	q_fade_out();
	q_panning();
	q_compression();/**/
	q_echo();
	return 0;
}
