/*
 * signal_constant.h
 *
 *  Created on: 16 mars 2016
 *      Author: 16003463
 */

#include "producteur.h"
#include "flot.h"
#include "composant.h"
#include <memory>
#include "producteur_base.h"

#ifndef SIGNAL_CONSTANT_H_
#define SIGNAL_CONSTANT_H_

class signal_constant : public producteur_base{
private:
	double m_valeur;
public:
	//Constructeur
	signal_constant(double valeur);
	//Destructeur
	~signal_constant();
	//
	virtual unsigned int nbSorties() const;

	virtual void calculer();
};


#endif /* SIGNAL_CONSTANT_H_ */
