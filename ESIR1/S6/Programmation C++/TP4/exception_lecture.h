#ifndef EXCEPTION_LECTURE_H
#define EXCEPTION_LECTURE_H

#include "composant_exception.h"

class exception_lecture : public virtual composant_exception {
public:
	exception_lecture(const std::string & msg) : composant_exception(msg) {};
};

#endif // !EXCEPTION_LECTURE_H
