#ifndef __HARMONIQUE__H__
#define __HARMONIQUE__H__

#include "producteur_base.h"
class harmonique : public producteur_base {
private:
	int m_f;
	double m_phi;
	int i;
public:
	harmonique(int f, double phi=0);
	~harmonique();
	virtual void calculer();

};
#endif