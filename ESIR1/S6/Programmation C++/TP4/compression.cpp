#include "compression.h"
#include <cmath>
#include "assert.h"


compression::compression(double puissance):filtre_base(1,1),m_puissance(puissance){
	assert(puissance > 0 && puissance <= 1);
}

void compression::calculer() {
	double v = getEntree(0)->extraire();
	double a = ((v < 0) ? -1 : 1) * pow(std::abs(v), m_puissance);
	getSortie(0)->inserer(a);
}