#include "mixeur.h"
#include "volume_compose.h"
#include <vector>

mixeur::mixeur(unsigned int nbEntree, std::vector<double> & v) : filtre_compose(nbEntree, 1){
	for (int i = 0; i < nbEntree; i++) {
		std::shared_ptr<volume_compose> v_compose(new volume_compose(v[i]));
		ajouter(v_compose);
		connecterEntreeComposant(i, v_compose, 0);
		m_producteur.push_back(v_compose);
	}
}

void mixeur::calculer() {
	filtre_compose::calculer();
	
	std::vector<std::shared_ptr<producteur>>::iterator it;
	double sum=0;
	for (it = m_producteur.begin(); it < m_producteur.end(); it++) {
		sum+=(*(*it)).getSortie(0)->extraire();
	}
	getSortie(0)->inserer(sum);
}
