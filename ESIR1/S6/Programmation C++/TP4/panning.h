#ifndef PANNING_H
#define PANNING_H


#include "filtre_compose.h"
#include "fade_in.h"
#include "fade_out.h"

/*Classe du filtre de panning*/
class panning : public filtre_compose {
private:
	std::shared_ptr<fade_in> fadeIn;
	std::shared_ptr<fade_out> fadeOut;
public :
	/*Constructeur de la classe contenant le debut du filtre ainsi que sa dur�e*/
	panning(double debut, double duree);
};

#endif