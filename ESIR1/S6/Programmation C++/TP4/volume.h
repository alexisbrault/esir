#ifndef VOLUME_H
#define VOLUME_H

#include "filtre_base.h"
#include "signal_constant.h"
#include "multiplicateur.h"


class volume : public virtual filtre_base {
private:
	signal_constant signal;
	multiplicateur multi;
public:
	volume(double v);
	virtual void calculer();
	virtual void connecterEntree(const std::shared_ptr<flot> & f, unsigned int numSortie);
};

#endif;