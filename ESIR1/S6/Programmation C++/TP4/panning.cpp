#include "panning.h"

panning::panning(double debut, double duree):filtre_compose(2,2),fadeIn(new fade_in(debut,duree)), fadeOut(new fade_out(debut,duree)){
	connecterSortie((*fadeIn).getSortie(0), 0);
	connecterSortie((*fadeOut).getSortie(0), 1);
	connecterEntreeComposant(0, fadeIn, 0);
	connecterEntreeComposant(1, fadeOut, 0);

	ajouter(fadeIn);
	ajouter(fadeOut);
}