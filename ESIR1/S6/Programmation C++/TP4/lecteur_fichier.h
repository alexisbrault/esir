#include "producteur_base.h"
#include <iostream>
#include <fstream>

class lecteur_fichier : public virtual producteur_base {
private:
	unsigned int m_outputs;
	std::ifstream m_file;
public:
	lecteur_fichier(std::string filename, unsigned int outputs);
	virtual ~lecteur_fichier();
	virtual void calculer();
	char lire();
};