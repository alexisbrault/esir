#include "volume.h"

volume::volume(double v):filtre_base(1, 1), signal(signal_constant(v)), multi(multiplicateur()){
	multi.connecterEntree(signal.getSortie(0), 1);
	connecterSortie(multi.getSortie(0), 0);
}

void volume::calculer() {
	signal.calculer();
	multi.calculer();
}

void volume::connecterEntree(const std::shared_ptr<flot> & f, unsigned int numSortie) {
	multi.connecterEntree(f, numSortie);
}