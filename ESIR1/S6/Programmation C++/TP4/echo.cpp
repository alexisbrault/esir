#include "echo.h"
#include "constantes.h"
#include "signal_constant.h"

echo::echo(double debut, double duree) : filtre_base(1, 1) {
	i_max = duree * MixageSonore::frequency;
	i_max2 = i_max * 2;
	n_max = debut * MixageSonore::frequency;
	n = 0;
	i = 0;
	memoire = new std::deque<double>();
}

echo::~echo() {
	delete memoire;
}

void echo::calculer() {
	/*if (n < n_max) {
		n++;
		return;
	}
	else {
		debut = true;
		if (i < i_max) {
			i++;
		}
		else {
			if (i < i_max2) {
				enregistrement = false;
				i++;
			}
			else {
				fin = true;
				return;
			}
		}
			
	}*/
	if (n < n_max) {
		n++;
	}
	else {
		debut = true;
		if (i < i_max2)
			i++;
		if (i > i_max  && i < i_max2)
			enregistrement = false;
		if (i >= i_max2)
			fin = true;
	}

	double a;
	if (debut && (!fin)) {
		if (enregistrement) {//Enregistrement des valeurs
			a = getEntree(0)->extraire();
			memoire->push_back(a);
		}
		else {
			signal_constant s(memoire->front());
			
			additionneur.connecterEntree(s.getSortie(0), 0);
			additionneur.connecterEntree(getEntree(0), 1);

			additionneur.connecterSortie(getSortie(0), 0);

			s.calculer();
			additionneur.calculer();


			/*a = additionneur.getSortie(0)->extraire();
			getSortie(0)->inserer(a);*/

			memoire->pop_front();

		}
	}
	else {
		a = getEntree(0)->extraire();
		getSortie(0)->inserer(a);
	}
	
}