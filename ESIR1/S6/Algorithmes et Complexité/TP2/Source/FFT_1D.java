package TP2;

/**** on va ici implémenter la transformée de Fourier rapide 1D ****/

public class FFT_1D {
	
	//"combine" c1 et c2 selon la formule vue en TD
	// c1 et c2 sont de même taille
	// la taille du résultat est le double de la taille de c1
	public static CpxTab combine(CpxTab c1, CpxTab c2) {
		assert (c1.taille()==c2.taille()) : "combine: c1 et c2 ne sont pas de même taille, taille c1="+c1.taille()+" taille c2="+c2.taille();
		int n = 2*c1.taille();
		
		CpxTab res = new CpxTab(n);
		for (int k = 0; k < n/2; k++) {
			double calcPi = (2*Math.PI*k)/n;
			double calcSin = Math.sin(calcPi);
			double calcCos = Math.cos(calcPi);
			double a = c1.get_p_reel(k);
			double b = c1.get_p_imag(k);
			double c = c2.get_p_reel(k);
			double d = c2.get_p_imag(k);
			
			res.set_p_reel(k, a + calcCos*c - calcSin*d);
			res.set_p_imag(k, b + d*calcCos + calcSin * c);
			
			res.set_p_reel(k+n/2, a - calcCos*c + calcSin*d );
			res.set_p_imag(k+n/2, b - d*calcCos - calcSin * c);
		}
		
		return res;
	}

	//renvoie la TFD d'un tableau de complexes
	//la taille de x doit être une puissance de 2
	public static CpxTab FFT(CpxTab x) {
		//A FAIRE : Test d'arrêt
		if(x.taille()==1){
			return x;
		}
		assert (x.taille()%2==0) : "FFT: la taille de x doit être une puissance de 2";
		int n = x.taille();
		CpxTab pair = new CpxTab(n/2);
		CpxTab impair = new CpxTab(n/2);
		int paircount = 0;
		int impaircount = 0;
		
		for(int k =0 ; k<n ; k++){
			if(k%2==0){
				pair.set_p_reel(paircount, x.get_p_reel(k));
				pair.set_p_imag(paircount, x.get_p_imag(k));
				paircount++;
			}
			else{
				impair.set_p_reel(impaircount, x.get_p_reel(k));
				impair.set_p_imag(impaircount, x.get_p_imag(k));
				impaircount++;
			}
		}
		//A FAIRE : Décomposition en "pair" et "impair" et appel récursif
		CpxTab res = combine(FFT(pair), FFT(impair));

		return res;
	}

	//renvoie la TFD d'un tableau de réels
	//la taille de x doit être une puissance de 2
	public static CpxTab FFT(double[] x) {
		return FFT(new CpxTab(x));
	}
			
	//renvoie la transformée de Fourier inverse de y
	public static CpxTab FFT_inverse(CpxTab y) {
		CpxTab res;
		res = FFT(y.conjuge());
		for (int k = 0; k < y.taille(); k++) {
			res.set_p_reel(k, res.get_p_reel(k)/y.taille());
			res.set_p_imag(k, res.get_p_imag(k)/y.taille());
		}
		res = res.conjuge();
		return res;
	}
	
	//calcule le produit de deux polynômes en utilisant la FFT
	//tab1 et tab2, sont les coefficients de ces polynômes
	// CpxTab sera le tableau des coefficients du polynôme produit (purement réel)
	public static CpxTab multiplication_polynome_viaFFT(double[] tab1, double[] tab2) {
		
		//on commence par doubler la taille des vecteurs en rajoutant des zéros à la fin (cf TD)
		double[] t1 = new double[2*tab1.length], t2 = new double[2*tab1.length];
		for (int i = 0; i < tab1.length; i++) {
			t1[i] = tab1[i];
			t2[i] = tab2[i];
		}
		
		CpxTab cp1 = new CpxTab(t1);
		CpxTab cp2 = new CpxTab(t2);
		cp1 = FFT(cp1);
		cp2 = FFT(cp2);
		
		CpxTab ret = CpxTab.multiplie(cp1, cp2);
		
		return FFT_inverse(ret);
	}

	
	//renvoie un tableau de réels aléatoires
	//utile pour tester la multiplication de polynômes
	public static double[] random(int n) {
		double[] t = new double[n];

		for (int i = 0; i < n; i++)
			t[i] = Math.random();
		return t;
	}

	//effectue la multiplication de polynômes représentés par coefficients
	// p1, p2 les coefficients des deux polynômes P1 et P2
	// renvoie les coefficients du polynôme P1*P2
	private static double [] multiplication_polynome_viaCoeff(double[] p1, double[] p2){
		
		int n = p1.length + p2.length - 1;
		double a,b;
		double [] out = new double[n];
		for (int k = 0; k < n; k++) {
			for (int i = 0; i <= k; i++) {
				a = (i<p1.length) ? p1[i]:0;
				b = (k-i<p2.length) ? p2[k-i] : 0;
				out[k] += a*b;
			}
		}
		return out;
	}
	

	//affiche un tableau de réels
	private static void afficher(double [] t){
		System.out.print("[");
		for(int k=0;k<t.length;k++){
			System.out.print(t[k]);
			if (k<(t.length-1))
				System.out.print(" ");
		}
		System.out.println("]");
	}
	
	public static void main(String[] args) {
		/* Pour tester exo 2: calculez et affichez TFD(1,2,3,4) */
			//A FAIRE
//		double[] t = {1,2,3,4};
//		CpxTab tab = new CpxTab(t);
//		tab = FFT(tab);
//		tab = FFT_inverse(tab);
		//System.out.println(tab);
		/* Pour tester exo 3: calculez et affichez TFD_inverse(TFD(1,2,3,4)) */
			//A FAIRE		
		/* Pour tester Partie 3 : multiplication polynomiale */
		
	
//		System.out.println("-----------------------------------------------------");
//		System.out.println("   Comparaison des 2 méthodes de multiplications polynomiales");
//		double[] t5 = {1,2,3,4};
//		double[] t6 = {-3,2,0,-5};
//		System.out.println("mult via FFT  --> "+multiplication_polynome_viaFFT(t5, t6));
//		System.out.print(  "mult via coeff -> ");afficher(multiplication_polynome_viaCoeff(t5, t6));
//	
//	
//		// Pour étude du temps de calcul 
//		int n = 1024*16;  // taille des polynômes à multiplier (testez différentes valeurs en gardant des multiples de 2)
//			
//		System.out.println("Temps de calcul pour n="+n);
//		double[] tab1 =random(n),tab2 = random(n);
//		long date1, date2;
//		date1 = System.currentTimeMillis();
//		multiplication_polynome_viaCoeff(tab1, tab2);
//		date2 = System.currentTimeMillis();
//		System.out.println("   via Coeff: " + (date2 - date1));
//
//		date1 = System.currentTimeMillis();
//		multiplication_polynome_viaFFT(tab1, tab2);
//		date2 = System.currentTimeMillis();
//		System.out.println("   via FFT  : " + (date2 - date1));
		
		
		//Partie 2
		//Exercice 1
		CpxTab tab;
		double t[];
		int n = 16;
		
		t = new double[n];
		for(int i=0;i<n;i++){
			t[i]=2;//Constante
		}
		tab = new CpxTab(t);
		tab = FFT(tab);
		System.out.println(tab);
		
		t = new double[n];
		int k=3;
		for(int i=0;i<16;i++){
			t[i]=Math.cos(((2*Math.PI*k)/n)*i);//sinusoïde pure
		}
		tab = new CpxTab(t);
		tab = FFT(tab);
		System.out.println(tab);
		
		t = new double[n];
		for(int i=0;i<16;i++){
			t[i]=Math.cos(((2*Math.PI)/n)*i)+0.5*Math.cos(((2*Math.PI*3)/n)*i);//somme de 2 sinusoïde
		}
		tab = new CpxTab(t);
		tab = FFT(tab);
		System.out.println(tab);
		
		t = new double[n];
		for(int i=0;i<16;i++){
			t[i]=4+2*Math.sin(((2*Math.PI*2)/n)*i)+Math.cos(((2*Math.PI*7)/n)*i);//fonction
		}
		tab = new CpxTab(t);
		tab = FFT(tab);
		System.out.println(tab);

	}

}

