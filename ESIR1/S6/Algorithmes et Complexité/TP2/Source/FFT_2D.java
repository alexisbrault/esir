package TP2;

import java.io.IOException;

public class FFT_2D {

	//renvoie la TFD d'une image de complexes
	public static CpxImg FFT(CpxImg I) {

		CpxImg out = new CpxImg(I.taille());

		// FFT 1D sur les lignes
		for (int k = 0; k < I.taille(); k++)
			out.set_line(k,FFT_1D.FFT(I.get_line(k)));
		  
		// transposition
		out.transpose();

		// FFT 1D sur les "nouvelles" lignes de out (anciennes colonnes)
		for (int k = 0; k < I.taille(); k++)
			out.set_line(k,FFT_1D.FFT(out.get_line(k)));

		//on re transpose pour revenir dans le sens de d�part
		out.transpose();
		
		//on divise par la taille de I
		out.multiply(1./I.taille());
		return out.recentrage();
	}
	
	//renvoie la TFD inverse d'une images de complexes
	public static CpxImg FFT_inverse(CpxImg I) {
		I = I.recentrage();
		CpxImg out = new CpxImg(I.taille());
		for (int k = 0; k < I.taille(); k++)
			out.set_line(k, I.get_line(k).conjuge());

		out = FFT(out).recentrage();
		for (int k = 0; k < I.taille(); k++)
			out.set_line(k, out.get_line(k).conjuge());
		return out;
	}

	// compression par mise � z�ro des coefficients de fr�quence 
	// FI contient la TDF de I 
	// Dans FI on met � z�ros tous les coefficients correspondant � des fr�quences inf�rieures � k
	public static void compression(CpxImg FI, int k) {
		for (int i = 0; i < FI.taille(); i++) {
			for (int j = 0; j < FI.taille(); j++) {
				if(i>=(FI.taille()/2)+k || i<=(FI.taille()/2)-k || j>=(FI.taille()/2)+k || j<=(FI.taille()/2)-k){
					FI.set_p_imag(i, j, 0);
					FI.set_p_reel(i, j, 0);
				}
				
			}
		}
	}

	// compression par seuillage des coefficients faibles
	// FI contient la TDF de I 
	// Dans FI on met � z�ros tous les coefficients dont le module est inf�rieur � seuil 
	// on renvoie le nombre de coefficients conserv�s 
	public static int compression_seuil(CpxImg FI, double seuil){
		int cpt=0;
		for (int i = 0; i < FI.taille(); i++) {
			for (int j = 0; j < FI.taille(); j++) {
				double reel = FI.get_p_reel(i, j);
				double img  = FI.get_p_imag(i, j);
				double module = Math.sqrt(reel*reel + img*img);	
				if(module<seuil){
					FI.set_p_imag(i, j, 0);
					FI.set_p_reel(i, j, 0);
				}
				else{
					cpt++;
				}
			}
		}
		return cpt;
	}

	
	public static void main(String[] args) {
		
//		try {			
//			//PLACEZ ICI VOS TESTS en 2D
//			//Exemple, lecture
//			BytePixmap BP = new BytePixmap("src/TP2/tigre_512.pgm");
//			CpxImg I = new CpxImg(BP);
//			I = FFT(I);
//			I = FFT_inverse(I);
//			//Exemple, �criture
//			BP = I.convert_to_BytePixmap();
//			BP.write("test_tigre_512.pgm");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		try {			
//			//PLACEZ ICI VOS TESTS en 2D
//			//Exemple, lecture
//			BytePixmap BP = new BytePixmap("src/TP2/barbara_512.pgm");
//			CpxImg I = new CpxImg(BP);
//			I = FFT(I);
//			//Exemple, �criture
//			BP = I.convert_to_BytePixmap();
//			BP.write("test_barbara.pgm");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		try {			
//			//PLACEZ ICI VOS TESTS en 2D
//			//Exemple, lecture
//			BytePixmap BP = new BytePixmap("src/TP2/tigre_512.pgm");
//			CpxImg I = new CpxImg(BP);
//			I = FFT(I);
//			//On annule en n/2
//			I.set_p_imag(I.taille()/2-2, I.taille()/2, 0);
//			I.set_p_reel(I.taille()/2-2, I.taille()/2, 0);
//			I=FFT_inverse(I);
//			//Exemple, �criture
//			BP = I.convert_to_BytePixmap();
//			BP.write("test_tigre_ex4.pgm");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		try {			
//			//PLACEZ ICI VOS TESTS en 2D
//			//Exemple, lecture
//			BytePixmap BP = new BytePixmap("src/TP2/barbara_512.pgm");
//			CpxImg I = new CpxImg(BP);
//			I = FFT(I);
//			//On annule en n/2
//			compression(I, 20);
//			I=FFT_inverse(I);
//			//Exemple, �criture
//			BP = I.convert_to_BytePixmap();
//			BP.write("test_barbara_ex5.pgm");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		try {			
//			//PLACEZ ICI VOS TESTS en 2D
//			//Exemple, lecture
//			BytePixmap BP = new BytePixmap("src/TP2/barbara_512.pgm");
//			CpxImg I = new CpxImg(BP);
//			I = FFT(I);
//			//On annule en n/2
//			compression_seuil(I, 20);
//			I=FFT_inverse(I);
//			//Exemple, �criture
//			BP = I.convert_to_BytePixmap();
//			BP.write("test_barbara_ex6.pgm");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		try {			
			//PLACEZ ICI VOS TESTS en 2D
			//Exemple, lecture
			BytePixmap BP = new BytePixmap("src/TP2/barbara_512.pgm");
			CpxImg I = new CpxImg(BP);
			I = FFT(I);
			//On annule en n/2
			compression(I, (int)Math.sqrt(186515));
			I=FFT_inverse(I);
			//Exemple, �criture
			BP = I.convert_to_BytePixmap();
			BP.write("test_barbara_ex7.pgm");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {			
			//PLACEZ ICI VOS TESTS en 2D
			//Exemple, lecture
			BytePixmap BP = new BytePixmap("src/TP2/barbara_512.pgm");
			CpxImg I = new CpxImg(BP);
			I = FFT(I);
			//On annule en n/2
			System.out.println(compression_seuil(I,2.85));
			I=FFT_inverse(I);
			//Exemple, �criture
			BP = I.convert_to_BytePixmap();
			BP.write("test_barbara_ex7_seuil.pgm");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
