import java.io.InputStream;


public class Client_Sac_A_Dos {


	/**
	 * lit une liste de poids dans un fichier
	 * @param nomFichier  nom du fichier texte contenant les poids
	 * @param nbr_objets nombre d'objets possibles
	 * @return tableau de poids
	 */
	public static double[] charge_poids(String nomFichier, int nbr_objets){
		double[] poids = new double[nbr_objets];
    	InputStream IS = Lecture.ouvrir(nomFichier);
    	if (IS==null){
    		System.err.println("Impossible d'ouvrir le fichier \""+nomFichier+"\" (n'existe pas ? pas au bon endroit ?)");
    	}
    	int i=0;
    	int somme=0;
    	while(!Lecture.finFichier(IS) && i<nbr_objets){
    		poids[i] = Lecture.lireDouble(IS);
    		somme += poids[i];
    		i++;
    	}
    	System.out.println("charge_poids ("+nomFichier+") : poids total des objets = "+somme);

    	Lecture.fermer(IS);
    	return poids;
	}

	
	public static void main(String[] args){
		
		/* paramètres */ 
		int nbr_indiv=100;
		double prob_mut=0.1;
		
		/* On initialise les poids en lisant un fichier 
		 */
		
//		int nbr_objets=28;
//		int capacite=1581;
		
		int nbr_objets=70;
		int capacite=350;		
		
		double[] poids = charge_poids("./tp4_donnees/data_sad/nbrobj"+nbr_objets+"_capacite"+capacite+".txt",nbr_objets);
		Individu_SAD[] t_test2 = new Individu_SAD[nbr_indiv];
		for(int i=0; i<nbr_indiv; i++){
			t_test2[i] = new Individu_SAD(poids,capacite);
		}
		Population<Individu_SAD> popu_test2 = new Population<Individu_SAD>(t_test2);


		
		/* on g�n�re les g�n�rations successives
		 * en faisant se reproduire la population
		 * et on affiche l'adaptation moyenne et maximale de chaque g�n�ration
		 * on s'arr�te si on a atteint la capacité ou si on fait un nombre donn� (param�tre) d'it�rations
		 * le r�sultat est alors donn� par l'individu maximal de la derni�re g�n�ration
		 */
		int nb_iteration = 10;
		int i=0;
		do{
			popu_test2.reproduction(prob_mut);	
			System.out.println("G�n�ration "+i);
			System.out.println("Adaptation maximale="+popu_test2.adaptation_maximale()+
					" Adaptation moyenne ="+popu_test2.adaptation_moyenne());
			i++;
		}while(i<nb_iteration && popu_test2.adaptation_maximale()<capacite);
		
		//Pour tester la méthode selection
//		Individu_SAD[] t_test = new Individu_SAD[3];
//		for(int i=0; i<3; i++){
//			t_test[i] = new Individu_SAD(poids,capacite);
//		}
//		Population<Individu_SAD> popu_test = new Population<Individu_SAD>(t_test);
//		double adapt_totale = 0;
//		for(int i=0; i<3; i++)
//			adapt_totale += t_test[i].adaptation();
//		int[] cpt = {0,0,0};
//		int n=1000000;
//		for(int k=0;k<n;k++){
//			cpt[popu_test.selection(adapt_totale)]++;
//		}
//		
//		for(int i=0; i<3; i++){
//			System.out.println(cpt[i]/(double) n+" -----------> "+t_test[i].adaptation()/adapt_totale);
//		}
//		System.out.println("Qu'en pensez-vous ?");
		
		/* on crée une population (aléatoire)
		 * de nbr_indiv individus de taille nbr_objets
		 */
		
		
		

	}
}

