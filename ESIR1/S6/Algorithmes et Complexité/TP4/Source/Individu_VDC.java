import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Individu_VDC implements Individu {

	double[] coord_x, coord_y;
	int[] parcours;

	//Constructeur
	public Individu_VDC(double[] coord_x, double[] coord_y) {
		this.coord_x = coord_x;
		this.coord_y = coord_y;
		parcours = new int[coord_x.length];
		for(int i =0; i<parcours.length; i++){
			parcours[i]=i;
		}
		shuffleArray(parcours);
		optim_2opt();
	}

	// Implementing Fisher–Yates shuffle
	static void shuffleArray(int[] ar)
	{
		// If running on Java 6 or older, use `new Random()` on RHS here
		Random rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--)
		{
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

	/* Classes de l'interface Individu
	 */
	@Override
	public double adaptation() {
		double somme = 0.0;
		for (int i = 0; i < parcours.length-1; i++) {
			somme+=distance(parcours[i], parcours[i+1]);
		}

		return 1/somme;
	}
	@Override
	public Individu[] croisement(Individu conjoint) {
		Random r = new Random();
		int random = r.nextInt(parcours.length);
		Individu_VDC[] ret = new Individu_VDC[2];
		Individu_VDC fils1 = new Individu_VDC(coord_x, coord_y);
		
//		System.out.println("---------- PARENT 1");
//		for(int i=0; i<this.parcours.length; i++){
//			System.out.print(i+"="+this.parcours[i]+" ");
//		}System.out.println("--");
//		
//		System.out.println("---------- PARENT 2");
//		for(int i=0; i<((Individu_VDC) conjoint).parcours.length; i++){
//			System.out.print(i+"="+((Individu_VDC) conjoint).parcours[i]+" ");
//		}System.out.println("--");
		
		
		
		Individu_VDC fils2 = new Individu_VDC(coord_x, coord_y);
		boolean[] bool1_val = new boolean[parcours.length];
		boolean[] bool2_val = new boolean[parcours.length];

		for (int i = 0; i < parcours.length; i++) {
			if(i<random){
				bool1_val[this.parcours[i]]=true;
				fils1.parcours[i]=this.parcours[i];
				bool2_val[((Individu_VDC) conjoint).parcours[i]]=true;
				fils2.parcours[i]=((Individu_VDC) conjoint).parcours[i];
			}
			else{
				if(!bool1_val[((Individu_VDC) conjoint).parcours[i]]){
					fils1.parcours[i]=((Individu_VDC) conjoint).parcours[i];
					bool1_val[((Individu_VDC) conjoint).parcours[i]]=true;
				}
				
				if(!bool2_val[this.parcours[i]]){
					fils2.parcours[i]=this.parcours[i];
					bool2_val[fils2.parcours[i]]=true;
				}
			}
		}
		
//		System.out.println("---------- BOOL_val 1");
//		for(int i=0; i<this.parcours.length; i++){
//			System.out.print(i+"="+bool1_val[i]+" ");
//		}System.out.println("--");
		
		int c1 = 0;
		for (int i = 0; i < bool1_val.length; i++) {
			if(!bool1_val[i])
				c1++;
		}
		
		int c2 = 0;
		for (int i = 0; i < bool2_val.length; i++) {
			if(!bool2_val[i])
				c2++;
		}
		
		for (int i = 0; i < bool1_val.length; i++) {
			if(!bool1_val[i]){
				int indice = bool1_val.length -c1;
				fils1.parcours[indice]=i;
				c1--;
			}
				
		}
		
		for (int i = 0; i < bool2_val.length; i++) {
			if(!bool2_val[i]){
				int indice = bool2_val.length -c2;
				fils2.parcours[indice]=i;
				c2--;
			}
				
		}
		
		
//		System.out.println("---------- FILS 1");
//		for(int i=0; i<fils1.parcours.length; i++){
//			System.out.print(i+"="+fils1.parcours[i]+" ");
//		}System.out.println("--");
//		
//		System.out.println("---------- FILS 2");
//		for(int i=0; i<fils1.parcours.length; i++){
//			System.out.print(i+"="+fils2.parcours[i]+" ");
//		}System.out.println("--");
		
		
//		System.out.println();
		ret[0] = fils1;
		ret[1] = fils2;
		return ret;
	}
	
	@Override
	public void mutation(double prob) {
		for (int i = 0; i < parcours.length; i++) {
			if(Math.random()<prob){
				Random r = new Random();
				int random = r.nextInt(parcours.length);
				int a = parcours[random];
				parcours[random]= parcours[i];
				parcours[i] = a;
			}	
		}
	}

	/* Accesseurs (pour Display_VDC)
	 */
	public int[] get_parcours(){
		return parcours;
	}
	public double[] get_coord_x(){
		return coord_x;
	}

	public double[] get_coord_y(){
		return coord_y;
	}
	
	public void optim_2opt(){
		for(int i=0;i<parcours.length;i++){
			for(int j=i+1;j<parcours.length;j++){
				if (gain(i,j)<0){ 
					for(int k=0;k<(j-i+1)/2;k++){
						int tmp = parcours[i+k];
						parcours[i+k] = parcours[j-k];
						parcours[j-k] = tmp;
						
					}
				}
			}
		}		    
	}
	
	private double gain(int i, int j){
		int nb_villes = parcours.length;
		 double gain = distance(parcours[i], parcours[(j+1)%nb_villes])
				 	 + distance(parcours[(i+nb_villes-1)%nb_villes], parcours[j])
				 	 - distance(parcours[(i+nb_villes-1)%nb_villes], parcours[i])
				 	 - distance(parcours[j], parcours[(j+1)%nb_villes]);
         return gain;
	}

	private double distance(int i, int j) {
		double xa = coord_x[i];
		double ya = coord_y[i];
		double xb = coord_x[j];
		double yb = coord_y[j];
		return Math.sqrt((xb-xa)*(xb-xa)+(yb-ya)*(yb-ya));
	}
}
