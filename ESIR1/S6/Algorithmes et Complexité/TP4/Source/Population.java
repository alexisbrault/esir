import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Population<Indiv extends Individu> {

	// Liste contenant les différents individus d'une génération
	private List<Indiv> population;


	/**
	 * construit une population à partir d'un tableau d'individu
	 */
	public  Population(Indiv[] popu){
		population = new ArrayList<Indiv>();
		for (int i = 0; i < popu.length; i++) {
			population.add(popu[i]);
		}
	}

	/**
	 * sélectionne un individu (sélection par roulette par exemple, cf TD)
	 * @param adapt_totale somme des adaptations de tous les individus (pour ne pas avoir à la recalculer)
	 * @return indice de l'individu sélectionné
	 */
	public int selection(double adapt_totale){
		Iterator<Indiv> ite = population.iterator();
		double r = Math.random();
		int i =0;
		double p = 0;
		
		do{
			if(!ite.hasNext())
				break;
			Indiv indiv = ite.next();
//			System.out.println("Adaptation individu ="+indiv.adaptation());
//			System.out.println("Adaptation totale ="+adapt_totale);
//			System.out.println("Quotient ="+indiv.adaptation()/adapt_totale);
//			System.out.println("P = "+p);
//			System.out.println("R = "+r);
//			System.out.println("--------");
			p = p+indiv.adaptation()/adapt_totale;
			i++;
			
		}while(p<r);
//		System.out.println("i-1="+(i-1));
		return i-1;
	}

	/**
	 * remplace la génération par la suivante
	 * (croisement + mutation)
	 * @param prob_mut probabilité de mutation
	 */
	@SuppressWarnings("unchecked")
	public void reproduction(double prob_mut) {

		Iterator<Indiv> ite = population.iterator();

		double adapt_totale =0;
		while(ite.hasNext()){
			Indiv next = ite.next();
			adapt_totale+=next.adaptation();
		}

		/***** on construit la nouvelle génération ****/
		List<Indiv> new_generation = new LinkedList<Indiv>();


		/* élitisme */
		new_generation.add(individu_maximal());

		// tant qu'on n'a pas le bon nombre 
		while (new_generation.size()<population.size()){
			// on sélectionne les parents
			Indiv ind1 = population.get(selection(adapt_totale));
			Indiv ind2 = population.get(selection(adapt_totale));
			//System.out.println("Parent identique="+ind1.equals(ind2));

			// ils se reproduisent
			Indiv[] enfants = (Indiv[]) ind1.croisement(ind2);


			// on les ajoute à la nouvelle génération
			new_generation.add(enfants[0]);
			new_generation.add(enfants[1]);
		}

		// on applique une éventuelle mutation à toute la nouvelle génération
		ite = new_generation.iterator();
		ite.next();
		while(ite.hasNext()){
			ite.next().mutation(prob_mut);
		}

		//on remplace l'ancienne par la nouvelle
		population = new_generation;
	}

	/**
	 * renvoie l'individu de la population ayant l'adaptation maximale
	 */	
	public Indiv individu_maximal(){
		Iterator<Indiv> ite = population.iterator();
		Indiv xmen = population.get(0);

		while(ite.hasNext()){
			Indiv next = ite.next();
			if(next.adaptation()>xmen.adaptation())
				xmen = next;
			
		}
		return xmen;
	}

	/**
	 * renvoie l'adaptation moyenne de la population
	 */
	public double adaptation_moyenne(){
		Iterator<Indiv> ite = population.iterator();
		double somme = 0;

		while(ite.hasNext()){
			Indiv next = ite.next();
			somme += next.adaptation();
			
		}
		return somme/population.size();
	}

	/**
	 * renvoie l'adaptation maximale de la population
	 */	
	public double adaptation_maximale(){
		return individu_maximal().adaptation();
	}
}
