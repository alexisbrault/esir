import java.util.Random;


public class Individu_SAD implements Individu{

	private double[] poids;
	private int capacite;
	private boolean[] tab;

	public Individu_SAD(double[] poids, int capacite) {
		this.poids = poids;
		this.capacite = capacite;
		tab = new boolean[poids.length];

		for(int i =0; i<tab.length; i++){
			tab[i]=(Math.random()<0.5);
		}
	}

	@Override
	public double adaptation() {
		double somme = 0.0;
		for (int i = 0; i < poids.length; i++) {
			if(tab[i])
				somme+=poids[i];
		}

		return somme<=capacite ? somme : 0;
	}

	@Override
	public Individu[] croisement(Individu conjoint) {
		Random r = new Random();
		int random = r.nextInt(tab.length);
		Individu[] ret = new Individu_SAD[2];
		Individu_SAD fils1 = new Individu_SAD(poids, capacite);
		Individu_SAD fils2 = new Individu_SAD(poids, capacite);

		for (int i = 0; i < tab.length; i++) {
			if(i<random){
				fils1.tab[i]=this.tab[i];
				fils2.tab[i]=((Individu_SAD) conjoint).tab[i];
			}
			else{
				fils2.tab[i]=this.tab[i];
				fils1.tab[i]=((Individu_SAD) conjoint).tab[i];
			}
		}
		ret[0] = fils1;
		ret[1] = fils2;
		return ret;
	}

	@Override
	public void mutation(double prob) {
		for (int i = 0; i < tab.length; i++) {
			if(Math.random()<prob){
				tab[i] = !tab[i];
			}	
		}
	}

}
