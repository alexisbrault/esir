import java.io.*;

public class Client_Voyageur_De_Commerce {


	/**
	 * lit une liste de poids dans un fichier
	 * @param nomFichier  nom du fichier texte contenant les coordonnées des villes
	 * @param nbr_villes nombre de villes
	 * @param coord_x et coord_y : les 2 tableaux que la fonction remplit et qui vont contenir les coordonnées des villes
	 */
	public static void charge_coords(String nomFichier, int nbr_villes, double[] coord_x, double[] coord_y){
		assert(coord_x.length==coord_y.length) : "charge_coords : coord_x et coord_y n'ont pas la même taille ?";
		InputStream IS = Lecture.ouvrir(nomFichier);
		if (IS==null){
			System.err.println("pb d'ouverture du fichier "+nomFichier);
		}
		int i=0;
		while(!Lecture.finFichier(IS) && i<coord_x.length){
			coord_x[i] = Lecture.lireDouble(IS);
			coord_y[i] = Lecture.lireDouble(IS);
			i++;
		}
		Lecture.fermer(IS);
	}

	public static void main(String[] args) throws InterruptedException{
		
		/* paramètres */ 
		double prob_mut=0.1;

		/* on initialise les coordonnées des villes en les lisant ds un fichier 
		 */
		int nbr_villes = 250;
		double[] coord_x = new double[nbr_villes];
		double[] coord_y = new double[nbr_villes];
		charge_coords("./tp4_donnees/data_vdc/"+"spirale_256.txt",nbr_villes, coord_x, coord_y);

		/* Exemple d'utilisation de Display_VDCC (il faut d'abord faire le constructeur pour ce test fonctionne, ainsi que compléter les accesseurs)
		 */
		
		Individu_VDC[] t_test = new Individu_VDC[16];
		for(int i=0; i<16; i++){
			t_test[i] = new Individu_VDC(coord_x,coord_y);
		}
		Population<Individu_VDC> popu_test = new Population<Individu_VDC>(t_test);
		int nb_iteration = 10;
		int i=0;
		Display_VDC disp = new Display_VDC(popu_test.individu_maximal());
		do{
			double t = System.currentTimeMillis();
			popu_test.reproduction(prob_mut);	
			System.out.println("G�n�ration "+i);
			System.out.println("Adaptation maximale="+popu_test.adaptation_maximale()+" Adaptation moyenne ="+popu_test.adaptation_moyenne());
			System.out.println("Temps = "+(System.currentTimeMillis()-t)+" ms");
			//Thread.sleep(3000);
			i++;
			disp.refresh(popu_test.individu_maximal());
		}while(i<nb_iteration );
	}
}