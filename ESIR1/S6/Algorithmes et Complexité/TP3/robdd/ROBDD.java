package robdd;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

//ReprÃ©sente un ROBDD sous forme de liste de Noeud_ROBDD
public class ROBDD {

	//Constantes pour les numÃ©ros des "feuilles" VRAI et FAUX
	public static final int idFalse = 0;
	public static final int idTrue = 1;

	//liste reprÃ©sentant le ROBDD
	private List<Noeud_ROBDD> R;

	//construit un ROBDD vide
	public ROBDD(){
		R = new LinkedList<Noeud_ROBDD>();
	}
	//ajoute le Noeud_ROBDD n au ROBDD courant
	public void ajouter(Noeud_ROBDD n) {
		R.add(n);
	}


	//renvoie le nombre de noeuds du ROBDD
	public int nb_noeuds() {
		return R.size()+2; // longueur de la liste R + les 2 noeuds correspondants Ã  VRAI et FAUX
	}

	@Override
	public String toString() {
		return R.toString();
	}

	// renvoie l'index, dans la liste R,  du noeud BDD associÃ© Ã  la variable nom et dont les fils droit et gauche sont d'indices respectifs fd et fg.
	// Si ce noeud n'existe pas dans le diagramme, la fonction renvoie -1.
	public int obtenirROBDDIndex(String nom, int fg, int fd) {
		Iterator<Noeud_ROBDD> ite = R.iterator();
		while(ite.hasNext()){
			Noeud_ROBDD noeud = ite.next();
			if(noeud.getNom().equals(nom) && noeud.getIdFilsGauche() == fg && noeud.getIdFilsDroit() == fd){
				return noeud.getId();
			}
		}
		return -1;
	}

	public String trouve_sat(){
		if(this.R.isEmpty())
			return "Non satifaisable";
		StringBuilder stb = new StringBuilder();
		int index=1;
		Iterator<Noeud_ROBDD> it = R.iterator();
		while (it.hasNext()) {
			Noeud_ROBDD noeud = it.next();
			if(noeud.getIdFilsDroit()==index){
				stb.append(noeud.getNom()+"= 1 \n");
				index=noeud.getId();
			}
			else if(noeud.getIdFilsGauche()==index){
				stb.append(noeud.getNom()+"= 0 \n");
				index=noeud.getId();
			}

		}
		return stb.toString();
	}

	public void reines_affiche_sat(int n,String piece){
		int taille = 50;
		JFrame frame = new JFrame();
		frame.setTitle("N reines, n="+n);
		frame.setSize(taille*n+150, taille*n+150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);

		//Le conteneur principal
		JPanel content = new JPanel();
		content.setPreferredSize(new Dimension(taille*n, taille*n));
		content.setBackground(Color.WHITE);
		//On définit le layout manager
		content.setLayout(new GridBagLayout());

		//L'objet servant à positionner les composants
		GridBagConstraints gbc = new GridBagConstraints();

		//La taille en hauteur et en largeur
		gbc.gridheight = 1;
		gbc.gridwidth = 1;

		JLabel jlabels[] = new JLabel[n*n];

		//On crée nos différents conteneurs de couleur différente
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				//On positionne la case de départ du composant
				gbc.gridx = i;
				gbc.gridy = j;
				JPanel cell = new JPanel();
				jlabels[j+i*n] = new JLabel(piece);
				jlabels[j+i*n].setFont(new Font(jlabels[j+i*n].getFont().getName(), Font.PLAIN, 25));
				cell.add(jlabels[j+i*n]);
				if((j+i*n+(n%2==0?i:0))%2==0)
					cell.setBackground(new Color(209,139,71));
				else
					cell.setBackground(new Color(255,206,158));

				cell.setPreferredSize(new Dimension(taille,taille));

				content.add(cell, gbc);
			}
		}
		String s = trouve_sat();
		String s2[] = s.split("\n");
		for (int i = 0; i < s2.length; i++) {
			if(s2[i].contains("= 0")){
				String s3[] = s2[i].substring(0,s2[i].length()-4).split(";");
				jlabels[Integer.parseInt(s3[0])+n*Integer.parseInt(s3[1])].setText("");
			}
		}

		//---------------------------------------------
		//On ajoute le conteneur
		frame.setContentPane(content);
		frame.setVisible(true);
	}
}
