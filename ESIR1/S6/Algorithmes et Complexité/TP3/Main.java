import java.util.LinkedList;
import java.util.List;

import expression.Atome;
import expression.Constante;
import expression.Equiv;
import expression.Et;
import expression.Expression;
import expression.Implique;
import expression.Non;
import expression.Ou;
import robdd.ROBDD;

public class Main {

	public static void main(String[] args) {
		//Exercice a afficher
		boolean exercice1 = true,
				exercice2 = true,
				exercice3 = true,
				exercice5 = true,
				exercice6 = true,
				exercice7 = true,
				exercice8 = true,
				calcul1   = false,
				calcul2   = false;
		
		int n = 8;
		////////////////////////////////////////////////////////////////////////Exercice 1//////////////////////////////////////////////////////////////

		//EXEMPLE

		if(exercice1){
			System.out.println("///////////////// Exercice 1");
			System.out.println("Test de l'impl�mentation de Atome et Equiv :");
			Expression exp = new Et(new Atome("x"),new Atome("y")); // représente (x ^ y)
			System.out.println(exp.atomes()); // affiche la liste des atomes (=variables booléennes) présents dans exp
			exp = exp.remplace("x",true); // exp vaut maintenant (true ^ y)
			try {
				System.out.println(exp.evalue()); // <- erreur car (true ^ y) ne peut pas être évalué
			} catch (Exception e) {}
			exp = exp.remplace("y",false); // exp vaut maintenant (true ^ false)
			System.out.println(exp.evalue());

			//Affichage de l'arbre associé à l'expression exp pour l'ordre x > y 
			List<String> ordre_atomes = new LinkedList<String>();
			ordre_atomes.add("x");
			ordre_atomes.add("y");
			System.out.println("\n Arbre de exp : \n" + exp.arbre(ordre_atomes)); // <- que se passe-t-il ? 
			Expression exp2 = new Et(new Atome("x"),new Atome("y")); // représente (x ^ y)
			System.out.println("\n Arbre de exp2 : \n" + exp2.arbre(ordre_atomes));	

			System.out.println();
			System.out.println();
		}

		////////////////////////////////////////////////////////////////////////Exercice 2//////////////////////////////////////////////////////////////
		if(exercice2){
			System.out.println("///////////////// Exercice 2");
			System.out.println("Soit la fonction f(x1,x2,y1,y2)=(x1<=>y1)^(x2<=>y2), on calcule puis affiche l'arbre:");
			Atome x1 = new Atome("x1");
			Atome x2 = new Atome("x2");
			Atome y1 = new Atome("y1");
			Atome y2 = new Atome("y2");

			Expression ex1 = new Equiv(x1,y1);
			Expression ex2 = new Equiv(x2,y2);
			Expression ex3 = new Et(ex1,ex2);
			System.out.println(ex3.atomes());
			List<String> ordre_atomes = new LinkedList<String>();
			ordre_atomes.add("x1");
			ordre_atomes.add("y1");
			ordre_atomes.add("x2");
			ordre_atomes.add("y2");
			System.out.println(ex3.arbre(ordre_atomes));
			List<String> ordre_atomes2 = new LinkedList<String>();
			ordre_atomes2.add("x1");
			ordre_atomes2.add("x2");
			ordre_atomes2.add("y1");
			ordre_atomes2.add("y2");
			System.out.println(ex3.arbre(ordre_atomes2));


			ex3 = ex3.remplace("x1", true);
			ex3 = ex3.remplace("y1", true);
			ex3 = ex3.remplace("x2", false);
			ex3 = ex3.remplace("y2", false);
			System.out.println(ex3.evalue());

			System.out.println();
			System.out.println();
		}
		////////////////////////////////////////////////////////////////////////Exercice 3//////////////////////////////////////////////////////////////

		if(exercice3){
			System.out.println("///////////////// Exercice 3");
			System.out.println("Test des fonctions estVrai() et estFaux():");

			Expression exp = new Constante(true);
			System.out.println(exp.estVrai());
			exp = new Non(new Constante(false));
			System.out.println(exp.estVrai());

			System.out.println();
			System.out.println();
		}

		////////////////////////////////////////////////////////////////////////Exercice 5//////////////////////////////////////////////////////////////
		if(exercice5){
			System.out.println("///////////////// Exercice 5");
			System.out.println("Test avec l'exercice 2 de ce TP:");
			Atome x = new Atome("x");
			Atome y = new Atome("y");
			Atome z = new Atome("z");

			Expression ex1 = new Equiv(x,y);
			Expression ex2 = new Et(z,y);
			Expression ex3 = new Ou(ex1,ex2);
			System.out.println(ex3.atomes());

			List<String> ordre_atomes2 = new LinkedList<String>();
			ordre_atomes2.add("x");
			ordre_atomes2.add("y");
			ordre_atomes2.add("z");


			ROBDD ro = ex3.robdd(ordre_atomes2);
			System.out.println(ro);

			System.out.println();
			System.out.println();
		}

		////////////////////////////////////////////////////////////////////////Exercice 6//////////////////////////////////////////////////////////////

		if(exercice6){
			System.out.println("///////////////// Exercice 6");
			System.out.println("Test avec l'exercice 2 de ce TP:");
			List<String> ordre_atomes2 = new LinkedList<String>();
			ordre_atomes2.add("x");
			ordre_atomes2.add("y");
			ordre_atomes2.add("z");

			Atome x = new Atome("x");
			Atome y = new Atome("y");
			Atome z = new Atome("z");

			Expression ex1 = new Equiv(x,y);
			Expression ex2 = new Et(z,y);
			Expression ex3 = new Ou(ex1,ex2);

			ROBDD ro = ex3.robdd(ordre_atomes2);
			System.out.println(ro.trouve_sat());

			System.out.println();
			System.out.println();
		}

		////////////////////////////////////////////////////////////////////////Exercice 7//////////////////////////////////////////////////////////////

		if(exercice7){
			System.out.println("///////////////// Exercice 7");

			System.out.println("Nous pouvons mod�liser le probl�me en mettant chaque case comme un noeud donc N�.");

			System.out.println();
			System.out.println();
		}

		////////////////////////////////////////////////////////////////////////Exercice 8//////////////////////////////////////////////////////////////

		if(exercice8){
			System.out.println("///////////////// Exercice 8");
			System.out.println("Cr�ation de l'expression du probleme des N reines.");

			LinkedList<String> ordre = new LinkedList<String>();

			//Initialisation
			for(int i=0; i<n; i++){
				for(int j=0; j<n; j++){
					ordre.add(""+i+";"+j+"");
				}
			}
			//Expressions
			Expression e = new Constante(true);
			Expression ligne,col,diag1,diag2;

			for(int i=0 ; i<n; i++){
				for(int j=0; j<n; j++){

					ligne = new Constante(true);
					col = new Constante(true);
					diag1 = new Constante(true);
					diag2 = new Constante(true);

					for (int k=0; k<n; k++){

						if(k!=j){
							ligne = new Et(ligne, new Non(new Atome(i+";"+k)));
						}

						if(k!=i){
							col = new Et(col, new Non(new Atome(k+";"+j)));
							int idx = j+i-k;
							if(idx>=0 && idx<n){
								diag1 = new Et(diag1, new Non(new Atome(k+";"+idx)));
							}
							
							idx = j-i+k;
							if(idx>=0 && idx<n){
								diag2 = new Et(diag2, new Non(new Atome(k+";" + idx)));
							}
						}
					}
					col = new Implique(new Atome(i+";"+j), col);
					ligne = new Implique(new Atome(i+";"+j), ligne);
					diag1 = new Implique(new Atome(i+";"+j), diag1);
					diag2 = new Implique(new Atome(i+";"+j), diag2);

					e = new Et(e, col);
					e = new Et(e, ligne);
					e = new Et(e, diag1);
					e = new Et(e, diag2);


				}
			}

			col = new Constante(true);

			for(int i=0; i<n; i++){

				ligne = new Constante(false);

				for(int j=0; j<n; j++){
					ligne = new Ou(ligne, new Atome(i+";"+j));
				}
				col = new Et(col, ligne);
			}

			e = new Et(e, col);

			ROBDD ro = null;
			if(calcul1){
				System.out.println("Calcul en cours ... (Peu �tre long)");
				System.out.flush();
				long t = System.currentTimeMillis();
				e.arbre(ordre);
				t=System.currentTimeMillis()-t;
				System.out.println("Cr�ation de l'arbre de Shannon en "+t+" ms pour n="+n);
			}

			if(calcul2){
				long t2 = System.currentTimeMillis();
				ro = e.robdd();
				t2=System.currentTimeMillis()-t2;
				System.out.println("Cr�ation du ROBDD en "+t2+" ms pour n="+n);
			}
			System.out.println();
			System.out.println("Nous avons donc mesur� un temps exponentiel pour la cr�ation de l'arbre de Shannon (n;t(ms): 3;16 4;610 5;513139)");
			System.out.println("Nous avons donc mesur� un temps exponentiel pour la cr�ation du ROBDD (n;t(ms): 5;31 6;59 7;244 8;1161 9;9153");
			System.out.println();
			System.out.println();
			System.out.println("///////////////// Exercice 9");
			ro = e.robdd();
			ro.reines_affiche_sat(n,"\u265B");
		}


	}
}
