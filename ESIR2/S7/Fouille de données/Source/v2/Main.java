package v2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;


public class Main {
	public static void main(String args[]) throws Exception{
		convertInTP3Format("Archive/TR-Mails/TR/TRAIN_", 2500, "Archive/spam-mail.tr.label", "TR");
		convertInTP3Format("Archive/TT-Mails/TT/TEST_", 1827, null, "TT");

	}

	private static void convertInTP3Format(String path, int nbMail, String pathAnswer, String nameFileOutput) throws IOException {
		final int NBMESSAGES = nbMail;
		ArrayList<String[]> spamOrNot = null;
		
		if (pathAnswer != null){
			spamOrNot = CSV2Array(pathAnswer);
		}
		
		StringBuilder stb = new StringBuilder();
		for (int i = 1; i <= NBMESSAGES; i++) {
			try{
				HashMap<String, Integer> map  = new HashMap<String, Integer>();
				test(new File(path+ i+".eml"),map);
				stb.append("ID"+i+" ");
				if (spamOrNot != null){
					String s = spamOrNot.get(i)[1];
					if(s.equals("1"))
						stb.append("ham ");
					else
						stb.append("spam ");
				}else{
					stb.append("N/C");
				}
				
				for (Map.Entry<String, Integer> entry : map.entrySet()) {
					stb.append(entry.getKey()+" "+entry.getValue()+" ");
				}
				stb.delete(stb.length()-1, stb.length());
				stb.append(System.getProperty("line.separator"));
			}catch (Exception e) {
				System.err.println("Erreur avec "+i);
			}
			
		}
		BufferedWriter bwr = new BufferedWriter(new FileWriter(new File(nameFileOutput+".csv")));
        bwr.write(stb.toString());
        bwr.flush();
        bwr.close();
	}
	
	private static void constructCSVFileTP3_Like(String content,HashMap<String, Integer> map){
		
		Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
                System.out.println();
                int k = map.get(matcher.group())==null?0:map.get(matcher.group());
				map.put(matcher.group(),++k);
        }
	}
	
	public static void test(File emlFile,HashMap<String, Integer> map) throws Exception{
		Properties props = System.getProperties();
		props.put("mail.host", "smtp.gmail.com");
		props.put("mail.transport.protocol", "smtp");
		Session mailSession = Session.getDefaultInstance(props, null);
		InputStream source = new FileInputStream(emlFile);
		MimeMessage message = new MimeMessage(mailSession, source);
//		System.out.println("Subject : " + message.getSubject());
//		System.out.println("From : " + message.getFrom()[0]);
//		System.out.println("--------------");
//		System.out.println("Body : " +  message.getContent());
//		System.out.println(message.getContent());
		constructCSVFileTP3_Like(message.getContent().toString(),map);
	}
	
	/**
	 * @author https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
	 * @param nameFile
	 * @return ArrayList of line split by " "
	 */
	private static ArrayList<String[]> CSV2Array(String nameFile){
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		ArrayList<String []> list = new ArrayList<>();
		try {
			br = new BufferedReader(new FileReader(nameFile));

			while ((line = br.readLine()) != null) {
				list.add(line.split(cvsSplitBy));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
}
