package v1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

public class Main {

	public static void main(String[] args) {

		String csvFile = "TR.csv";

		HashMap<String,Integer> mapSpam = new HashMap<String,Integer>(); 
		HashMap<String,Integer> mapHam = new HashMap<String,Integer>(); 

		constuctMaps(csvFile, mapSpam, mapHam);
		
		computeProbabilities(csvFile, mapSpam, mapHam);

		frequentItemSet1(sortByValue(mapSpam,true), 5, countWords(mapSpam), "SPAM");

		System.out.println();
		frequentItemSet1(sortByValue(mapHam,true), 5, countWords(mapHam), "HAM");


		Map<String,Integer> mapProbSpam = compareMap(mapSpam, mapHam);
		Map<String,Integer> mapProbHam = compareMap(mapHam, mapSpam);

		System.out.println();
		filterMail("TT.CSV", mapProbSpam, mapProbHam);

	}
	
	private static void computeProbabilities(String csvFile, HashMap<String, Integer> mapSpam,
			HashMap<String, Integer> mapHam) {
		Iterator<String[]> it = CSV2Array(csvFile).iterator();

		int spam=0,ham=0;
		
		while (it.hasNext()) {

			String[] str = it.next();

			if(str[1].equals("ham"))
				ham++;
			else
				spam++;

		}
		System.out.println("Probabilities:");
		System.out.println("P(spam)="+spam+" / "+(spam+ham));
		System.out.println("P(spam)="+ham+" / "+(spam+ham));
		System.out.println();
	}

	
	private static void constuctMaps(String csvFile, HashMap<String, Integer> mapSpam,
			HashMap<String, Integer> mapHam) {
		Iterator<String[]> it = CSV2Array(csvFile).iterator();

		while (it.hasNext()) {

			String[] str = it.next();

			HashMap<String,Integer> map= null;
			if(str[1].equals("ham"))
				map = mapHam;
			else
				map = mapSpam;

			for (int i = 2; i < str.length; i=i+2) {

				int k = map.get(str[i])==null?0:map.get(str[i]);

				map.put(str[i],Integer.parseInt(str[i+1])+k);

			}
		}
	}

	private static void filterMail(String nameFile, Map<String, Integer> spamProbabilityMap, Map<String, Integer> hamProbabilityMap) {

		int numLine = 0;
		int tauxReussite = 0;
		int hamConsideredAsSpam = 0;
		int spamConsideredAsHam = 0;

		Iterator<String[]> it = CSV2Array(nameFile).iterator();

		while (it.hasNext()) {
			numLine++;
			double probSpam = 1;
			double probHam = 1;

			String[] str = it.next();

			for (int i = 2; i < str.length; i=i+2) {

				int probMotSpam = (spamProbabilityMap.get(str[i]) == null) ? 100 : spamProbabilityMap.get(str[i]) ;
				probSpam *= ((double)probMotSpam/(double)100);

				int probMotHam = (hamProbabilityMap.get(str[i]) == null) ? 100 : hamProbabilityMap.get(str[i]) ;
				probHam *= ((double)probMotHam/(double)100);
			}
			boolean spam = probSpam>=probHam;

//			if(spam==str[1].equals("spam")){
//				tauxReussite++;
//			}else{
				if(spam)
					hamConsideredAsSpam++;
				else
					spamConsideredAsHam++;
			//}
		}
		System.out.println("Sur "+ numLine+ " lignes, le taux de reussite est de "+ percent(tauxReussite, numLine) + "%");
		//System.out.println(percent(hamConsideredAsSpam,numLine)+"% ham ont �t� consid�r� comme spam");
		//System.out.println(percent(spamConsideredAsHam,numLine)+"% spam ont �t� consid�r� comme ham");
		
		System.out.println(percent(hamConsideredAsSpam,numLine)+"% spams");
		System.out.println(percent(spamConsideredAsHam,numLine)+"% hams");
	}

	private static double percent(int a, int b){
		return round(((double)a/(double)b)*100,2);
	}

	private static void frequentItemSet1(Map<String, Integer> map,int bestOf, int total, String name){
		System.out.println(name);
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			System.out.println(entry.getKey()+" : "+entry.getValue()+"/"+total);
			if(bestOf==1)
				break;
			bestOf--;
		}
	}

	private static Map<String, Integer> compareMap(Map<String, Integer> map1, Map<String, Integer> map2){
		HashMap<String,Integer> map = new HashMap<String,Integer>();

		for (Map.Entry<String, Integer> entry : map1.entrySet()) {
			String key = entry.getKey();
			double k = map2.get(key)==null?0:map2.get(key);
			double l = entry.getValue();
			double d = (l/(l+k))*100;
			map.put(key, (int)d);
		}

		return map;
	}

	/**
	 * @author https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
	 * @param nameFile
	 * @return ArrayList of line split by " "
	 */
	private static ArrayList<String[]> CSV2Array(String nameFile){
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = " ";
		ArrayList<String []> list = new ArrayList<>();
		try {
			br = new BufferedReader(new FileReader(nameFile));

			while ((line = br.readLine()) != null) {
				list.add(line.split(cvsSplitBy));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}

	/**
	 * @author https://www.mkyong.com/java/how-to-sort-a-map-in-java/
	 * @param unsortMap key:word value:nbOccurence
	 * @param desc: trier de fa�on decroissante
	 * @return map tri�e
	 */
	private static Map<String, Integer> sortByValue(Map<String, Integer> unsortMap,boolean desc) {

		// 1. Convert Map to List of Map
		LinkedList<Entry<String, Integer>> list =
				new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

		// 2. Sort list with Collections.sort(), provide a custom Comparator
		//    Try switch the o1 o2 position for a different order
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> o1,
					Map.Entry<String, Integer> o2) {
				if(desc)
					return (o2.getValue()).compareTo(o1.getValue());
				else
					return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		// 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Map.Entry<String, Integer> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}


	/**
	 * @param Map key:mot value:nbOccurence
	 * @return Le nombre d'occurence totale 
	 */
	private static int countWords(Map<String,Integer> map){
		int i = 0;
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			i += entry.getValue();
		}
		return i;
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}
	
	
	/*
	public static void combinations(int n, List<String> arr, List<String[]> list) {
	    // Calculate the number of arrays we should create
	    int numArrays = (int)Math.pow(arr.size(), n);
	    // Create each array
	    for(int i = 0; i < numArrays; i++) {
	        list.add(new String[n]);
	    }
	    // Fill up the arrays
	    for(int j = 0; j < n; j++) {
	        // This is the period with which this position changes, i.e.
	        // a period of 5 means the value changes every 5th array
	        int period = (int) Math.pow(arr.size(), n - j - 1);
	        for(int i = 0; i < numArrays; i++) {
	            String[] current = list.get(i);
	            // Get the correct item and set it
	            int index = i / period % arr.size();
	            current[j] = arr.get(index);
	        }
	    }
	}
	*/
}
