theory tp89
imports Main  "~~/src/HOL/Library/Code_Target_Nat" 
begin

 
quickcheck_params [size=6,tester=narrowing,timeout=120]
nitpick_params [timeout=120]


(*Structures*)
type_synonym transid= "nat*nat*nat" (* (c,m,id) *)

type_synonym transaction= "transid * nat"(* (c,m,id) * am) *)

datatype message= 
  Pay transid nat 
| Ack transid nat 
| Cancel transid

type_synonym transBdd= "(message list)*(transaction list)*(transid list)" (*waitList, validateList, canceledList*)

(*Fonctions*)
fun matchWithAck :: "transaction \<Rightarrow> message list \<Rightarrow> (bool*nat)"
where
"matchWithAck _ [] = (False,0)" |
"matchWithAck ((c,m,i),am) ((Pay (c2,m2,i2) am2)#msgList) = (if ((c=c2)\<and>(m=m2)\<and>(i=i2)\<and>(am2\<ge>am))then (True,am2) else matchWithAck ((c,m,i),am)  msgList )"|
"matchWithAck a ((Ack (c2,m2,i2) am2)#msgList) = (matchWithAck a  msgList )"|
"matchWithAck a ((Cancel (c2,m2,i2))#msgList) = (matchWithAck a  msgList )"

fun matchWithPay :: "transaction \<Rightarrow> message list \<Rightarrow> (bool*nat)"
where
"matchWithPay _ [] = (False,0)" |      (*Attention au signe entre am2 et am  *)
"matchWithPay ((c,m,i),am) ((Ack (c2,m2,i2) am2)#msgList) = (if ((c=c2)\<and>(m=m2)\<and>(i=i2)\<and>(am2\<le>am))then (True,am2) else matchWithPay ((c,m,i),am)  msgList )"|
"matchWithPay a ((Pay (c2,m2,i2) am2)#msgList) = (matchWithPay a  msgList )"|
"matchWithPay a ((Cancel (c2,m2,i2))#msgList) = (matchWithPay a  msgList )"

fun isAmountCorrect :: "nat \<Rightarrow> bool"
where
"isAmountCorrect x = (x>0)"

fun deleteTrans::"transaction list \<Rightarrow>transaction \<Rightarrow>transaction list"
where
"deleteTrans [] _ = []"|
"deleteTrans (((c,m,i),am)#xs) ((c2,m2,i2),am2) = (if((c=c2)\<and>(m=m2)\<and>(i=i2))then deleteTrans xs ((c2,m2,i2),am2) else [((c,m,i),am)]@( deleteTrans xs ((c2,m2,i2),am2))) "

fun deleteMsg::"message list \<Rightarrow>message \<Rightarrow>message list"
where
"deleteMsg [] _ = []"|
"deleteMsg (a#xs) b = (if(a=b)then deleteMsg xs b else [a]@( deleteMsg xs b))"

fun isTransCancelled::"transid list \<Rightarrow> transid \<Rightarrow> bool"
where
"isTransCancelled [] _ = False"|
"isTransCancelled ((c,m,i)#xs) (c2,m2,i2) = (if((c=c2)\<and>(m=m2)\<and>(i=i2))then True else (isTransCancelled xs (c2,m2,i2)) )"

fun isTransAlreadyValidate::"transaction list \<Rightarrow> transaction \<Rightarrow> bool"
where
"isTransAlreadyValidate [] _ = False"|
"isTransAlreadyValidate (((c,m,i),am)#xs) ((c2,m2,i2),am2) = (if((c=c2)\<and>(m=m2)\<and>(i=i2))then True else (isTransAlreadyValidate xs ((c2,m2,i2),am)) )"

fun updateMessage::"message \<Rightarrow> message list \<Rightarrow> message list"
where
"updateMessage _ [] = []"|
"updateMessage (Pay (e1) am) ((Pay (e2) am2)#xs) = 
  (if(e1=e2 \<and> am>am2) then Pay e1 am#(updateMessage (Pay (e1) am)  xs) else Pay e1 am2#(updateMessage (Pay (e1) am)  xs)) "|
"updateMessage (Ack (e1) am) ((Ack (e2) am2)#xs) = 
  (if(e1=e2 \<and> am<am2) then Ack e1 am#(updateMessage (Ack (e1) am)  xs) else Ack e1 am2#(updateMessage (Ack (e1) am)  xs)) "|
"updateMessage msg (a#xs) = 
  (a#(updateMessage msg xs)) "

fun messageInList::"message \<Rightarrow> message list \<Rightarrow> bool"
where
"messageInList _ [] = False"|
"messageInList (Pay (e1) am) ((Pay (e2) am2)#xs) = 
  (if(e1=e2) then True else messageInList (Pay (e1) am) xs) "|
"messageInList (Ack (e1) am) ((Ack (e2) am2)#xs) = 
  (if(e1=e2) then True else messageInList (Ack (e1) am) xs) "|
"messageInList msg (a#xs) = (messageInList msg xs)"

fun traiterMessage:: "message \<Rightarrow> transBdd \<Rightarrow> transBdd"
where 
"traiterMessage (Cancel (c,m,i)) (waitList, validateList, canceledList) =  
  (if(isTransCancelled canceledList (c,m,i)) then 
    (waitList, validateList,canceledList)
   else
    (waitList, (deleteTrans validateList ((c, m, i),0)), [(c, m, i)]@canceledList)
  )
" |
"traiterMessage (Pay (c,m,i) am) (waitList, validateList, canceledList) = 
  (let (b1) = (isTransCancelled canceledList (c,m,i)) in
    (let (b4) = (isTransAlreadyValidate validateList ((c,m,i),am)) in
     (let (b3) = (isAmountCorrect am) in
      (if(b1 \<or> \<not>b3  \<or> b4) then
        (waitList, validateList, canceledList)
      else
        (let (b2,amM) = (matchWithPay((c,m,i),am) waitList) in
          (if(b2)then
            (deleteMsg waitList (Ack (c,m,i) amM),[((c, m, i),am)]@validateList, canceledList)
          else
            (
              (if(messageInList (Pay (c, m, i) am) waitList ) then 
                  ((updateMessage (Pay (c, m, i) am) waitList ), validateList, canceledList)
               else
                  ([(Pay (c, m, i) am)]@waitList, validateList, canceledList)
              )
            )
          )
        )
      )
     )
   )
  )
"|
"traiterMessage (Ack (c,m,i) am) (waitList, validateList, canceledList) =
(let (b1) = (isTransCancelled canceledList (c,m,i)) in
  (let (b4) = (isTransAlreadyValidate validateList ((c,m,i),am)) in
    (let (b3) = (isAmountCorrect am) in
      (if(b1 \<or> \<not>b3 \<or> b4) then
        (waitList, validateList, canceledList)
      else
        (let (b2,amClient) = (matchWithAck((c,m,i),am) waitList) in
          (if(b2)then
            (deleteMsg waitList (Pay (c,m,i) amClient),[((c, m, i),amClient)]@validateList, canceledList)
          else
             (
              (if(messageInList (Ack (c, m, i) am) waitList ) then 
                  ((updateMessage (Ack (c, m, i) am) waitList ), validateList, canceledList)
               else
                  ([(Ack (c, m, i) am)]@waitList, validateList, canceledList)
              )
            )
          )
        )
      )
     )
   )
  )
"

fun export :: "transBdd \<Rightarrow> transaction list"
where 
"export (waitList, validateList, canceledList) = validateList"

fun traiterMessageList::"message list \<Rightarrow> transBdd"
where
"traiterMessageList []  = ([],[],[])" |
"traiterMessageList (x#xs) = (traiterMessage x (traiterMessageList xs))"

(* pn = test,pnE = expected *)
definition "p1 = [Pay (1, 1, 2) 1]"
definition "p1E = ([Pay (1, 1, 2) 1],[],[])"

definition "p2 = [(Pay (1, 1, 2) 1),(Pay (2, 1, 2) 2)]"
definition "p2E = ([(Pay (1, 1, 2) 1),(Pay (2, 1, 2) 2)],[],[])"

definition "p3 = [(Pay (1, 1, 2) 1),(Pay (1, 1, 2) 2)]"
definition "p3E = ([(Pay (1, 1, 2) 2)],[],[])"

(* Pay am \<ge> Ack am *)
definition "p4 = [(Pay (1, 1, 2) 1),(Ack (1, 1, 2) 2)]"
definition "p4E = ([(Pay (1, 1, 2) 1),(Ack (1, 1, 2) 2)],[],[])"

(* clear de la wait list si trans acceptée *)
definition "p5 = [(Pay (1, 1, 2) 2),(Ack (1, 1, 2) 2)]"
definition "p5E = ([],[((1, 1, 2),2)],[])"

(* 4 inversé *)
definition "p6 = [(Ack (1, 1, 2) 2),(Pay (1, 1, 2) 1)]"
definition "p6E = ([(Ack (1, 1, 2) 2),(Pay (1, 1, 2) 1)],[],[])"

(* 5 inversé *)
definition "p7 = [(Ack (1, 1, 2) 2),(Pay (1, 1, 2) 2)]"
definition "p7E = ([],[((1, 1, 2),2)],[])"

(* même validées les transactions peuvent être annulées *)
definition "p8 = [(Ack (1, 1, 2) 2),(Pay (1, 1, 2) 2),(Cancel (1, 1, 2))]"
definition "p8E = ([],[],[(1, 1, 2)])"

definition "p9 = [(Cancel (1, 1, 2)),(Ack (1, 1, 2) 2),(Pay (1, 1, 2) 2)]"
definition "p9E = ([],[],[(1, 1, 2)])"

definition "p10 = [(Ack (1, 1, 2) 0)]"
definition "p10E = ([],[],[])"

definition "p11 = [(Pay (1, 1, 2) 0)]"
definition "p11E = ([],[],[])"

(* Prend le am du client *)
definition "p12 = [(Pay (1, 1, 2) 5),(Ack (1,1,2) 1)]"
definition "p12E = ([],[((1,1,2),5)],[])"
(*  *)
definition "p13 = [(Ack (1, 1, 2) 2),(Ack (1, 1, 2) 3)]"
definition "p13E = ([(Ack (1, 1, 2) 2)],[],[])"

definition "p14 = [(Ack (1, 1, 2) 3),(Ack (1, 1, 2) 2),(Pay (1, 1, 2) 2)]"
definition "p14E = ([],[((1, 1, 2),2)],[])"

definition "p15 = [(Pay (1, 1, 2) 2),(Pay (1, 1, 1) 4)]"
definition "p15E = ([(Pay (1, 1, 2) 2),(Pay (1,1,1) 4)],[],[])"

value"(traiterMessageList p1) = p1E"
value"(traiterMessageList p2) = p2E"
value"(traiterMessageList p3) = p3E"
value"(traiterMessageList p4) = p4E"
value"(traiterMessageList p5) = p5E"
value"(traiterMessageList p6) = p6E"
value"(traiterMessageList p7) = p7E"
value"(traiterMessageList p8) = p8E"
value"(traiterMessageList p9) = p9E"
value"(traiterMessageList p10) = p10E"
value"(traiterMessageList p11) = p11E"
value"(traiterMessageList p12) = p12E"
value"(traiterMessageList p13) = p13E"
value"(traiterMessageList p14) = p14E"
value"(traiterMessageList p15) = p15E"

(*Proprietes attendues*)


(* Toutes les transactions validées ont un montant strictement positif. *)
lemma prop1 : "(List.length(List.filter(\<lambda>am. am<0) (List.map (\<lambda>((c,m,id), am). am) (export (traiterMessageList l)))) = 0)" 
apply auto
done

(* Dans la liste de transactions validées, tout triplet (c,m,id) (où c est un numéro de client, m est un
numéro de marchand et id un numéro de transaction) n’apparait qu’une seule fois. *)
lemma prop2 :"(List.distinct (List.map (\<lambda>((c,m,id), am). (c,m,id)) (export (traiterMessageList l) ) ) ) = True" 
quickcheck
oops

(* Toute transaction (meme validee) peut etre annulee. *)
lemma prop3 : "\<forall>y>0. \<forall>x\<ge>y. ((List.member l (Cancel (a))) \<and> (List.member l (Pay (a) x) ) \<and> (List.member l (Ack (a) y) ) ) \<longrightarrow> (\<not> List.member(export(traiterMessageList l)) (a,x))"
quickcheck
oops

(* Toute transaction annul\<acute>ee l’est d\<acute>efinitivement : un message (Cancel (c,m,id)) rend impossible la
validation d’une transaction de num\<acute>ero id entre un marchand m et un client c. *)
lemma prop4 : "\<forall>y>0. \<forall>x\<ge>y. ((List.member l (Cancel (a))) \<and> (List.member l (Pay (a) x) ) \<and> (List.member l (Ack (a) y) ) ) \<longrightarrow> (\<not> List.member(export(traiterMessageList l)) (a,x))"
quickcheck
oops

(* Si un message Pay et un message Ack, tels que le montant propose par le Pay est superieur ou egal au
montant propose par le message Ack et non annulee, ont ete envoyes alors la transaction figure dans
la liste des transactions validees. *)
lemma prop5 : "\<forall>y>0. \<forall>x\<ge>y. (\<not>(List.member l (Cancel (a))) \<and> (List.member l (Pay (a) x) ) \<and> (List.member l (Ack (a) y) ) ) = ( List.member(export(traiterMessageList l)) (a,x))"
apply (induct l)
apply (metis export.simps member_rec(2) traiterMessageList.simps(1))
sledgehammer
oops

(* Toute transaction figurant dans la liste des transactions validees l’a ete par un message Pay et un
message Ack tels que le montant propose par le Pay est superieur ou egal au montant propose par le
message Ack *)
lemma prop6 : " \<forall>y>0. \<forall>x\<ge>y . List.member(export(traiterMessageList [(Pay (a) x),(Ack (a) y)])) (a,x)"
quickcheck
oops

(* Si un client (resp. marchand) a propos\<acute>e un montant am pour une transaction, tout montant am’
inf\<acute>erieur (resp. sup\<acute>erieur) propos\<acute>e par la suite est ignor\<acute>e par l’agent de validation. *)
lemma prop7 : "True"
apply auto
done

(* Toute transaction valid\<acute>ee ne peut ˆetre ren\<acute>egoci\<acute>ee : si une transaction a \<acute>et\<acute>e valid\<acute>ee avec un montant
am celui-ci ne peut ˆetre chang\<acute>e. *)
lemma prop8 : "True"
apply auto
done

(* Le montant associ\<acute>e `a une transaction valid\<acute>ee correspond `a un prix propos\<acute>e par le client pour cette
transaction. *)
lemma prop9 : "True"
apply auto
done

(* ----- Exportation en Scala (Isabelle 2014) -------*)

(* Suppression de l'export des abstract datatypes (Isabelle 2014) *)
code_reserved Scala
  message

code_printing
  type_constructor message \<rightharpoonup> (Scala) "message"
  | constant Ack \<rightharpoonup> (Scala) "Ack"
  | constant Pay \<rightharpoonup> (Scala) "Pay"
  | constant Cancel \<rightharpoonup> (Scala) "Cancel"
  | code_module "" \<rightharpoonup> (Scala)

(* Directive d'exportation *)
export_code export traiterMessage in Scala

end


