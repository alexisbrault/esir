theory tp1
imports Main
begin

lemma "A \<or> B"
nitpick
oops

lemma "A \<and> B \<longrightarrow> B"
apply auto
done
(* EXERCICE 1 *)
lemma "A\<and>(B\<or>C)\<longleftrightarrow>(A\<and>B)\<or>(A\<and>C)"
apply auto
done

lemma "\<not>(A\<and>B)\<longleftrightarrow>\<not>A\<or>\<not>B"
apply auto
done


(* EXERCICE 2 

E: Ecossais 
R: Chaussette rouge
K: Kilt
M: Marié
D: Dimanche
*)
lemma "((\<not>E\<longrightarrow>R)\<and>(R\<longrightarrow>K)\<and>(M\<longrightarrow>\<not>D)\<and>(D\<longleftrightarrow>E)\<and>(K\<longrightarrow>E\<and>M)\<and>(E\<longrightarrow>K))"
apply auto
oops

lemma "\<not>((\<not>E\<longrightarrow>R)\<and>(R\<longrightarrow>K)\<and>(M\<longrightarrow>\<not>D)\<and>(D\<longleftrightarrow>E)\<and>(K\<longrightarrow>E\<and>M)\<and>(E\<longrightarrow>K))"
apply auto
done 
(*Il n'y a pas de contre exemple 
à la négation donc il ne peut pas y avoir de membre*)

lemma "((R\<longrightarrow>K)\<and>(M\<longrightarrow>\<not>D)\<and>(D\<longleftrightarrow>E)\<and>(K\<longrightarrow>E\<and>M)\<and>(E\<longrightarrow>K))"
quickcheck
oops

lemma "\<not>((R\<longrightarrow>K)\<and>(M\<longrightarrow>\<not>D)\<and>(D\<longleftrightarrow>E)\<and>(K\<longrightarrow>E\<and>M)\<and>(E\<longrightarrow>K))"
quickcheck
oops

(*Il y a un contre exemple 
à la négation donc il peut y avoir un  membre*)

(* EXERCICE 3 *)
(*A*)
lemma "\<forall>(x::nat) y z .x+y >x+z \<longrightarrow> x+x>y+z"
nitpick
oops

lemma "\<not>(\<forall>(x::nat) y z .\<not>(x+y >x+z \<longrightarrow> x+x>y+z))"
apply auto
done

(* Contradictoire *)

lemma "(\<forall>(x::nat) y z .x>y\<and>x>z  \<longrightarrow> x+x>y+z)"
apply auto
done

(*B*)
lemma "\<forall>(x::nat)y .x+y\<le>x*y"
nitpick
oops

(*Satisfaisable
Hypothese de validité, tout entier différent de 1*)

lemma "\<forall>(x::nat)y .x+y\<le>x*y"
apply auto
oops

(*C*)
lemma "\<forall>(x::nat)yz. x > y\<and>z>0 \<longrightarrow> x*z > y*z"
apply auto
done
(*Valide*)

(*D*)
lemma "(\<exists>(x::nat) .P(f(x))) \<longrightarrow> (\<forall>x .P(f(x)))"
apply auto
oops

lemma "\<not>((\<exists>(x::nat) .P(f(x))) \<longrightarrow> (\<forall>x .P(f(x))))"
apply auto
oops
(*Satisfaisable
Hypothese : On inverse*) 

lemma "((\<forall>(x::nat) .P(f(x))) \<longrightarrow> (\<exists>x .P(f(x))))"
apply auto
done

(*Exercice 4*)
(*Commutativité*)
lemma "\<forall>(x::nat)y .x+y=y+x"
apply auto
done

(*Associativité*)
lemma "\<forall>(x::nat)y z .(x+y)+z=x+(y+z)"
apply auto
done

(*Elem neutre*)
lemma "\<forall>(x::nat). \<exists>y .x=x+y"
apply auto
done

(*Exercice 5*)
(*Commutatif*)
lemma "((append (append a b)  c)) = (append a (append b c))"
apply auto
done

(*Associatif*)
lemma "(append a (append b c)) = (append (append a b) c)"
apply auto
done

(*Elem neutre*)
lemma "\<forall>(x). \<exists>y .x=(append x y)"
apply auto
done

(*Exercice 6*)
lemma "(length(append a b)) = (length a) + (length b)"
apply auto
done

lemma "(map l1 l2) = (append l1 l2) "
apply auto

lemma "(List.member l1 a )\<or>(List.member l2 a) = (List.member (append l1 l2) a)"
nitpick
sorry

end
