package tp5


object HOL {

trait equal[A] {
  val `HOL.equal`: (A, A) => Boolean
}
def equal[A](a: A, b: A)(implicit A: equal[A]): Boolean = A.`HOL.equal`(a, b)

def eq[A : equal](a: A, b: A): Boolean = equal[A](a, b)

} /* object HOL */

object tp5 {

def exactSubSeq[A : HOL.equal](x0: List[A], uu: List[A]): Boolean = (x0, uu)
  match {
  case (Nil, uu) => true
  case (v :: va, Nil) => false
  case (x :: l1, y :: l2) =>
    (if (HOL.eq[A](x, y)) exactSubSeq[A](l1, l2)
      else exactSubSeq[A](x :: l1, l2))
}

def subSeq2[A : HOL.equal](x0: List[A], uu: List[A], b: Boolean): Boolean =
  (x0, uu, b) match {
  case (Nil, uu, b) => true
  case (List(a), Nil, false) => true
  case (v :: vb :: vc, Nil, b) => false
  case (v :: va, Nil, true) => false
  case (x :: l1, y :: l2, false) =>
    (if (exactSubSeq[A](x :: l1, y :: l2)) true
      else (if (HOL.eq[A](x, y)) subSeq2[A](l1, l2, false)
             else subSeq2[A](l1, y :: l2, true)))
  case (x :: l1, y :: l2, true) => exactSubSeq[A](x :: l1, y :: l2)
}

def subSeq[A : HOL.equal](l1: List[A], l2: List[A]): Boolean =  subSeq2[A](l1, l2, false)

} /* object tp5 */
