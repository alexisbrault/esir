theory tp67
imports Main "~~/src/HOL/Library/Code_Target_Int" "~~/src/HOL/Library/Code_Char"
begin

(* Types des expressions, conditions et programmes (statement) *)
datatype expression= Constant int | Variable string | Sum expression expression | Sub expression expression

datatype condition= Eq expression expression

datatype statement= Seq statement statement | 
                    Aff string expression | 
                    Read string | 
                    Print expression | 
                    Exec expression | 
                    If condition statement statement |
                    Skip
                    
datatype AbsInt = Undefined | AbsInts "int list"
type_synonym absSymTable= "(string * AbsInt) list"
(* Un exemple d'expression *)

(* expr1= (x-10) *)
definition "expr1= (Sub (Variable ''x'') (Constant 10))"


(* Des exemples de programmes *)

(* OFFENSIF p1= exec(0)  *)
definition "p1= Exec (Constant 0)"

(* OFFENSIF p2= {
        print(10)
        exec(0+0)
       }
*)

definition "p2= (Seq (Print (Constant 10)) (Exec (Sum (Constant 0) (Constant 0))))"

(* OFFENSIF p3= {
         x:=0
         exec(x)
       }
*)

definition "p3= (Seq (Aff ''x'' (Constant 0)) (Exec (Variable ''x'')))"

(* INOFFENSIF p4= {
         read(x)
         print(x+1)
       }
*)
definition "p4= (Seq (Read ''x'') (Print (Sum (Variable ''x'') (Constant 1))))"


(* Le type des evenements soit X: execute, soit P: print *)
datatype event= X int | P int

(* les flux de sortie, d'entree et les tables de symboles *)

type_synonym outchan= "event list"
definition "el1= [X 1, P 10, X 0, P 20]"                   (* Un exemple de flux de sortie *)

type_synonym inchan= "int list"           
definition "il1= [1,-2,10]"                                (* Un exemple de flux d'entree [1,-2,10]              *)

type_synonym symTable= "(string * int) list"
definition "(st1::symTable)= [(''x'',10),(''y'',12)]"      (* Un exemple de table de symbole *)


(* La fonction (partielle) de recherche dans une liste de couple, par exemple une table de symbole *)
datatype 'a option= None | Some 'a

fun assoc:: "'a \<Rightarrow> ('a * 'b) list \<Rightarrow> 'b option"
where
"assoc _ [] = None" |
"assoc x1 ((x,y)#xs)= (if x=x1 then Some(y) else (assoc x1 xs))"

value "assoc ''a'' [(''b'', 2)]"
value "assoc ''a'' [(''b'', 2), (''a'', 3::int)]"

(* Exemples de recherche dans une table de symboles *)

value "assoc ''x'' st1"     (* quand la variable est dans la table st1 *)
value "assoc ''z'' st1"     (* quand la variable n'est pas dans la table st1 *)


(* Evaluation des expressions par rapport a une table de symboles *)
fun evalE:: "expression \<Rightarrow> symTable \<Rightarrow> int"
where
"evalE (Constant s) e = s" |
"evalE (Variable s) e= (case (assoc s e) of None \<Rightarrow> -1 | Some(y) \<Rightarrow> y)" |
"evalE (Sum e1 e2) e= ((evalE e1 e) + (evalE e2 e))" |
"evalE (Sub e1 e2) e= ((evalE e1 e) - (evalE e2 e))" 

fun sumList2 :: "int list \<Rightarrow> int list \<Rightarrow> int list"
where
"sumList2 [] _ = []"|
"sumList2 l1 [] = l1"|
"sumList2 (x#xs) l2 = (append (List.map (\<lambda>y. y+x) l2)(sumList2 xs l2))"

fun sumList:: "AbsInt \<Rightarrow> AbsInt \<Rightarrow> AbsInt"
where
"sumList Undefined _ = Undefined"|
"sumList _ Undefined = Undefined"|
"sumList (AbsInts l1) (AbsInts l2) = AbsInts (sumList2 l1 l2)"

fun subList2 :: "int list \<Rightarrow> int list \<Rightarrow> int list"
where
"subList2 [] _ = []"|
"subList2 l1 [] = l1"|
"subList2 (x#xs) l2 = (append (List.map (\<lambda>y. x-y) l2)(subList2 xs l2))"

fun subList:: "AbsInt \<Rightarrow> AbsInt \<Rightarrow> AbsInt"
where
"subList Undefined _ = Undefined"|
"subList _ Undefined = Undefined"|
"subList (AbsInts l1) (AbsInts l2) = AbsInts (subList2 l1 l2)"

fun evalE2:: "expression \<Rightarrow> absSymTable \<Rightarrow> AbsInt"
where
"evalE2 (Constant s) e = AbsInts[s]" |
"evalE2 (Variable s) e = (case (assoc s e) of None \<Rightarrow> (AbsInts [-1]) | Some(y) \<Rightarrow> y)" |
"evalE2 (Sum e1 e2) e= (sumList(evalE2 e1 e)(evalE2 e2 e))" |
"evalE2 (Sub e1 e2) e= (subList (evalE2 e1 e)(evalE2 e2 e))"

(* Exemple d'évaluation d'expression *)

value "evalE expr1 st1"

(* Evaluation des conditions par rapport a une table de symboles *)
fun evalC:: "condition \<Rightarrow> symTable \<Rightarrow> bool"
where
"evalC (Eq e1 e2) t= ((evalE e1 t) = (evalE e2 t))"

(* Evaluation d'un programme par rapport a une table des symboles, a un flux d'entree et un flux de sortie. 
   Rend un triplet: nouvelle table des symboles, nouveaux flux d'entree et sortie *)
fun evalS:: "statement \<Rightarrow> (symTable * inchan * outchan) \<Rightarrow> (symTable * inchan * outchan)"
where
"evalS Skip x=x" |
"evalS (Aff s e)  (t,inch,outch)=  (((s,(evalE e t))#t),inch,outch)" |
"evalS (If c s1 s2)  (t,inch,outch)=  (if (evalC c t) then (evalS s1 (t,inch,outch)) else (evalS s2 (t,inch,outch)))" |
"evalS (Seq s1 s2) (t,inch,outch)= 
    (let (t2,inch2,outch2)= (evalS s1 (t,inch,outch)) in
        evalS s2 (t2,inch2,outch2))" |
"evalS (Read _) (t,[],outch)= (t,[],outch)" |
"evalS (Read s) (t,(x#xs),outch)= (((s,x)#t),xs,outch)" |
"evalS (Print e) (t,inch,outch)= (t,inch,((P (evalE e t))#outch))" |
"evalS (Exec e) (t,inch,outch)= 
  (let res= evalE e t in
   (t,inch,((X res)#outch)))"

(*Fonction qui fusionne deux aST en une seul*)
fun mixedAST::"absSymTable \<Rightarrow> absSymTable \<Rightarrow> absSymTable"
where
"mixedAST [] l = l "|
"mixedAST l [] = l "|
"mixedAST aST1 ((x,(AbsInts y))#xs) =
(case (assoc x aST1) of 
  None \<Rightarrow>  (x,(AbsInts (-1#y)))#(mixedAST aST1 xs) |
  Some(Undefined) \<Rightarrow> (x,Undefined)#(mixedAST aST1 xs) |
  Some(AbsInts z) \<Rightarrow> (x,(AbsInts (append y z)))#(mixedAST aST1 xs))" |
"mixedAST aST1 ((x,Undefined)#xs) = (x,Undefined)#(mixedAST aST1 xs)"

(* Test si une variable est inconnue ou égale à 0) *)
fun isValid:: "AbsInt \<Rightarrow> bool" 
where 
"isValid Undefined = False" |
"isValid (AbsInts x ) = (\<not>(List.member x 0))"

(* Test si une condition peut être évaluée à l'avance *)
fun canEvalCond::"(AbsInt*AbsInt)\<Rightarrow>bool"
where
"canEvalCond (AbsInts a ,AbsInts b) = ((List.length a = 1)\<and>(List.length b = 1))"|
"canEvalCond (_,_)  = False"
fun canEvalCond2::"condition\<Rightarrow>absSymTable\<Rightarrow>bool"
where
"canEvalCond2 (Eq e1 e2) aST  = canEvalCond ((evalE2 e1 aST),(evalE2 e2 aST))"

(* Test si le If est vrai ou faux en ayant au préalable vérifié que c'était possible de l'évalué*)
fun evalCond::"(AbsInt*AbsInt)\<Rightarrow>bool"
where
"evalCond (AbsInts a ,AbsInts b) = ((a=b))"|
"evalCond (_,_)  = False"

fun evalCond2::"condition\<Rightarrow>absSymTable\<Rightarrow>bool"
where
"evalCond2 (Eq e1 e2) aST  = evalCond ((evalE2 e1 aST),(evalE2 e2 aST))"

fun evalS2:: "statement \<Rightarrow> (absSymTable) \<Rightarrow> (bool * absSymTable)"
where
"evalS2 Skip aST =(True, aST)" |
"evalS2 (Aff s e) aST =   (True, (s, evalE2 e aST)#aST)" |
"evalS2 (If c s1 s2) aST = 
  (if(canEvalCond2 c aST )then 
    (if (evalCond2 c aST) then 
        evalS2 s1 aST
      else
        evalS2 s2 aST
    )
    else
    (let (b1, aST1) = (evalS2 s1 aST) in 
      (let(b2, aST2) = (evalS2 s2 aST) in 
        (if (b1\<and>b2) then (True,(mixedAST aST1 aST2)) else (False, aST))
   )
)) " |
"evalS2 (Seq s1 s2) aST = 
 (let (b1, aST1) = (evalS2 s1 aST) in 
  (let(b2, aST2) = (evalS2 s2 aST1) in 
  (if (b1\<and>b2) then (True, aST2) else (False, aST2)))) " |
"evalS2 (Read s) aST= (True, (s, Undefined)#aST)" |
"evalS2 (Print e) aST = (True, aST)" |
"evalS2 (Exec e) t= (let res= evalE2 e t in ( ((isValid res),t)))" 

(* Exemples d'évaluation de programmes *)
(* Les programmes p1, p2, p3, p4 ont été définis plus haut *)
(* p1= exec(0) *)

value "evalS p1 ([],[],[])"

(* ------------------------------------ *)
(* p2= {
        print(10)
        exec(0+0)
       }
*)

value "evalS p2 ([],[],[])"

(* ------------------------------------ *)
(* p3= {
         x:=0
         exec(x)
       }
*)

value "evalS p3 ([],[],[])"

(* ------------------------------------ *)
(* p4= {
         read(x)
         print(x+1)
       }
*)

value "evalS p4 ([],[10],[])"

definition "bad1= (Exec (Constant 0))"
definition "bad2= (Exec (Sub (Constant 2) (Constant 2)))"
definition "bad3= (Seq (Aff ''x'' (Constant 1)) (Seq (Print (Variable ''x'')) (Exec (Sub (Variable ''x'') (Constant 1)))))"
definition "bad4= (Seq (Read ''x'') (Seq (If (Eq (Variable ''x'') (Constant 0)) Skip (Aff ''y'' (Constant 1))) (Exec (Sum (Variable ''y'') (Constant 1)))))"
definition "bad5= (Seq (Read ''x'') (Seq (Aff ''y'' (Sum (Variable ''x'') (Constant 2))) (Seq (If (Eq (Variable ''x'') (Sub (Constant 0) (Constant 1))) (Seq (Aff ''x'' (Sum (Variable ''x'') (Constant 2))) (Aff ''y'' (Sub (Variable ''y'') (Variable ''x'')))) (Seq (Aff ''x'' (Sub (Variable ''x'') (Constant 2))) (Aff ''y'' (Sub (Variable ''y'') (Variable ''x''))))) (Exec (Variable ''y'')))))"
definition "bad6= (Seq (Read ''x'') (Seq (If (Eq (Variable ''x'') (Constant 0)) (Aff ''z'' (Constant 1)) (Aff ''z'' (Constant 0))) (Exec (Variable ''z''))))"
definition "bad7= (Seq (Read ''x'') (Seq (If (Eq (Variable ''x'') (Constant 0)) (Aff ''z'' (Constant 0)) (Aff ''z'' (Constant 1))) (Exec (Variable ''z''))))"
definition "bad8= (Seq (Read ''x'') (Seq (Read ''y'') (If (Eq (Variable ''x'') (Variable ''y'')) (Exec (Constant 1)) (Exec (Constant 0)))))"
definition "ok0= (Seq (Aff ''x'' (Constant 1)) (Seq (Read ''y'') (Seq (If (Eq (Variable ''y'') (Constant 0)) (Seq (Print (Sum (Variable ''y'') (Variable ''x'')))
(Print (Variable ''x''))
) (Print (Variable ''y''))
) (Seq (Aff ''x'' (Constant 1)) (Seq (Print (Variable ''x''))
 (Seq (Aff ''x'' (Constant 2)) (Seq (Print (Variable ''x''))
 (Seq (Aff ''x'' (Constant 3)) (Seq (Print (Variable ''x''))
 (Seq (Read ''y'') (Seq (If (Eq (Variable ''y'') (Constant 0)) (Aff ''z'' (Sum (Variable ''x'') (Variable ''x''))) (Aff ''z'' (Sub (Variable ''x'') (Variable ''y'')))) (Print (Variable ''z''))
)))))))))))"
definition "ok1= (Seq (Aff ''x'' (Constant 1)) (Seq (Print (Sum (Variable ''x'') (Variable ''x'')))
 (Seq (Exec (Constant 10)) (Seq (Read ''y'') (If (Eq (Variable ''y'') (Constant 0)) (Exec (Constant 1)) (Exec (Constant 2)))))))"
definition "ok2= (Exec (Variable ''y''))"
definition "ok3= (Seq (Read ''x'') (Exec (Sum (Variable ''y'') (Constant 2))))"
definition "ok4= (Seq (Aff ''x'' (Constant 0)) (Seq (Aff ''x'' (Sum (Variable ''x'') (Constant 20))) (Seq (If (Eq (Variable ''x'') (Constant 0)) (Aff ''z'' (Constant 0)) (Aff ''z'' (Constant 4))) (Seq (Exec (Variable ''z'')) (Exec (Variable ''x''))))))"
definition "ok5= (Seq (Read ''x'') (Seq (Aff ''x'' (Constant 4)) (Exec (Variable ''x''))))"
definition "ok6= (Seq (If (Eq (Constant 1) (Constant 2)) (Aff ''x'' (Constant 0)) (Aff ''x'' (Constant 1))) (Exec (Variable ''x'')))"
definition "ok7= (Seq (Read ''x'') (Seq (If (Eq (Variable ''x'') (Constant 0)) (Aff ''x'' (Constant 1)) (If (Eq (Variable ''x'') (Constant 4)) (Aff ''x'' (Constant 1)) (Aff ''x'' (Constant 1)))) (Exec (Variable ''x''))))"
definition "ok8= (Seq (Read ''x'') (Seq (If (Eq (Variable ''x'') (Constant 0)) (Aff ''x'' (Constant 1)) (Aff ''x'' (Constant 2))) (Exec (Sub (Variable ''x'') (Constant 3)))))"
definition "ok9= (Seq (Read ''x'') (Seq (Read ''y'') (If (Eq (Sum (Variable ''x'') (Variable ''y'')) (Constant 0)) (Exec (Constant 1)) (Exec (Sum (Variable ''x'') (Sum (Variable ''y'') (Sum (Variable ''y'') (Variable ''x''))))))))"
definition "ok10= (Seq (Read ''x'') (If (Eq (Variable ''x'') (Constant 0)) (Exec (Constant 1)) (Exec (Variable ''x''))))"
definition "ok11= (Seq (Read ''x'') (Seq (If (Eq (Variable ''x'') (Constant 0)) (Aff ''x'' (Sum (Variable ''x'') (Constant 1))) Skip) (Exec (Variable ''x''))))"
definition "ok12= (Seq (Aff ''x'' (Constant 1)) (Seq (Read ''z'') (If (Eq (Variable ''z'') (Constant 0)) (Exec (Variable ''y'')) (Exec (Variable ''z'')))))"
definition "ok13= (Seq (Aff ''z'' (Constant 4)) (Seq (Aff ''x'' (Constant 1)) (Seq (Read ''y'') (Seq (Aff ''x'' (Sum (Variable ''x'') (Sum (Variable ''z'') (Variable ''x'')))) (Seq (Aff ''z'' (Sum (Variable ''z'') (Variable ''x''))) (Seq (If (Eq (Variable ''y'') (Constant 1)) (Aff ''x'' (Sub (Variable ''x'') (Variable ''y''))) Skip) (Seq (If (Eq (Variable ''y'') (Constant 0)) (Seq (Aff ''y'' (Sum (Variable ''y'') (Constant 1))) (Exec (Variable ''x''))) Skip) (Exec (Variable ''y'')))))))))"
definition "ok14= (Seq (Read ''x'') (Seq (Read ''y'') (If (Eq (Sum (Variable ''x'') (Variable ''y'')) (Constant 0)) (Exec (Constant 1)) (Exec (Sum (Variable ''x'') (Variable ''y''))))))"


(* Le TP commence ici! *)

(*
3. 
1) P1, P2 et P3 sont OFFENSIF car il y a un 'exec(0)', P4 ne l'est pas.

3) On observe que le [X O] en sortie correpsondant à l'exec(0)' et donc au cas OFFENSANT
*)

fun BAD::"(symTable * inchan * outchan) \<Rightarrow> bool"
where
"BAD (_, _,outlist) = (List.member outlist (X 0))" 

value "BAD ([(''2'', 1)],[1],[X 1, P 0])"

fun san ::"statement \<Rightarrow> bool"
where 
"san (Exec _) = False"|
"san (Seq s1 s2) = (san s1 \<and> san s2)"|
"san (If _ s1 s2) = (san s1 \<and> san s2)"|
"san _ = True"

fun san2 ::"statement \<Rightarrow> bool"
where 
"san2 (Exec a) = (a \<noteq> (Constant 0))"|
"san2 (Seq s1 s2) = (san2 s1 \<and> san2 s2)"|
"san2 (If _ s1 s2) = (san2 s1 \<and> san2 s2)"|
"san2 _ = True"

fun san3 ::"statement \<Rightarrow> bool"
where 
"san3 statement = (let (b, aST) = (evalS2 statement []) in b)"

value "san3 ok1"
value "san3 ok2"
value "san3 ok3"
value "san3 ok4"
value "san3 ok5"
value "san3 ok6"
value "san3 ok7"
value "san3 ok8"
value "san3 ok9"
value "san3 ok10"
value "san3 ok11"
value "san3 ok12"
value "san3 ok13"
value "san3 ok14"

value "san3 bad1"
value "san3 bad2"
value "san3 bad3"
value "san3 bad4"
value "san3 bad5"
value "san3 bad6"
value "san3 bad7"
value "san3 bad8"

(* ----- Restriction de l'export Scala (Isabelle 2014) -------*)
(* ! ! !  NE PAS MODIFIER ! ! ! *)
(* Suppression de l'export des abstract datatypes (Isabelle 2014) *)

code_reserved Scala
  expression condition statement 
code_printing
  type_constructor expression \<rightharpoonup> (Scala) "expression"
  | constant Constant \<rightharpoonup> (Scala) "Constant"
  | constant Variable \<rightharpoonup> (Scala) "Variable"
  | constant Sum \<rightharpoonup> (Scala) "Sum"
  | constant Sub \<rightharpoonup> (Scala) "Sub"  

  | type_constructor condition \<rightharpoonup> (Scala) "condition"
  | constant Eq \<rightharpoonup> (Scala) "Eq"

  | type_constructor statement \<rightharpoonup> (Scala) "statement"
  | constant Seq \<rightharpoonup> (Scala) "Seq"
  | constant Aff \<rightharpoonup> (Scala) "Aff"
  | constant Read \<rightharpoonup> (Scala) "Read"
  | constant Print \<rightharpoonup> (Scala) "Print"
  | constant Exec \<rightharpoonup> (Scala) "Exec"
  | constant If \<rightharpoonup> (Scala) "If"
  | constant Skip \<rightharpoonup> (Scala) "Skip"
  | code_module "" \<rightharpoonup> (Scala) 

{*// Code generated by Isabelle
package tp67

import utilities.Datatype._
// automatic conversion of utilities.Datatype.Int.int to Int.int
object AutomaticConversion{ 
	implicit def int2int(i:utilities.Datatype.Int.int):Int.int =
			i match {
			case utilities.Datatype.Int.int_of_integer(i)=>Int.int_of_integer(i)
	}
}
import AutomaticConversion._
*}

(* Directive pour l'exportation de l'analyseur *)
export_code san3 in Scala file "./san.scala"


end

