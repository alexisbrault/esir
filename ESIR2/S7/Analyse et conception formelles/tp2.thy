theory tp2
imports Main
begin

(*Construction des ensembles*)
(*Exercice 1*)
fun member :: "'a \<Rightarrow> 'a list \<Rightarrow> bool"
where 
"member e [] = False" |
"member e (x#xs) = (if e=x then True else (member e xs))"

(*Exercice 2*)
fun isSet :: "'a list \<Rightarrow> bool"
where 
"isSet [] = True" |
"isSet (x#xs) = (if (member x xs) then False else (isSet xs))"

value "isSet [1,2,3]"

(*Exercice 3*)

fun clean :: "'a list \<Rightarrow> 'a list"
where 
"clean [] = []"|
"clean (x#xs) = (if (member x xs) then (clean xs) else x#(clean xs ))"

(*Exercice 4*)
lemma memberClean :"\<forall>x. (member x l) \<longleftrightarrow> (member x (clean l))"
apply (induct l)
apply (metis clean.simps(1))
by (metis clean.simps(2) tp2.member.simps(2))

(*Exercice 5*)
lemma isSetClean : "(isSet (clean l))"
apply (induct l)
apply (metis clean.simps(1) isSet.simps(1))
by (metis (full_types) clean.simps(2) isSet.simps(2) memberClean)

(*Exercice 6*)
fun delete :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" 
where
"delete e [] = []"|
"delete e (x#xs) = (if e=x then xs else x#(delete e xs))"

(*Exercice 7*)
lemma memberDeleteClean : "(member x (delete x (clean l))) = False"
apply (induct l)
apply (metis clean.simps(1) delete.simps(1) tp2.member.simps(1))
by (metis clean.simps(2) delete.simps(2) memberClean tp2.member.simps(2))

lemma memberDeleteClean2 : "(member x l ) \<and> (x\<noteq>y) \<longrightarrow>((member x (delete y (clean l))) = True)"
apply (induct l)
apply (metis tp2.member.simps(1))
by (metis clean.simps(2) delete.simps(2) memberClean tp2.member.simps(2))

(*Exercice 8*)
fun intersection :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"
where
"intersection [][] = []"|
"intersection [] y = []"|
"intersection (x#xs) y = (if(member x y) then x#(intersection xs y) else (intersection xs y))"

value "(intersection [(1::nat),2,4][1,3,5,2])"

(*Exercice 9*)
lemma memberIntersection : "\<forall>x. (member x l1) \<and> (member x l2) = (member x (intersection l1 l2))"
sorry

(*Exercice 10*)
lemma isSetIntersection : "(isSet (intersection l1 l2))"
apply (induct l1)
apply (metis intersection.simps(1) intersection.simps(2) isSet.cases isSet.elims(3) list.distinct(1))
by (metis memberDeleteClean memberIntersection)

(*Exercice 11*)
fun union :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"
where
"union [][] = []"|
"union [] y = y"|
"union (x#xs) y = (if(member x y) then (union xs y) else x#(union xs y))"

value "(union [(1::nat),2,4][1,3,5,2])"

(*Exercice 12*)
lemma memberUnion :  "\<forall>x. (member x l1) \<or> (member x l2) = (member x (union l1 l2))"
by (metis memberIntersection)


(*Exercice 13*)
lemma isSetUnion : "(isSet l1) \<and> (isSet l2) \<longrightarrow>  (isSet (union l1 l2))"
by (metis isSet.elims(2) memberIntersection union.simps(1))

(*Exercice 14*)
fun equal :: "'a list \<Rightarrow>'a list  \<Rightarrow> bool"
where
"equal [] [] = True "|
"equal [] l2 = False"|
"equal (x#l1) l2 = (if(append x l2) then equal l1 (delete x l2) else False)"

(*Exercice 15*)
lemma "(equal l1 l2) \<longrightarrow> (\<forall>x.(member x l1) \<and>  (member x l2))"
by (metis memberIntersection)

end
