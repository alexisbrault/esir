object HOL {

trait equal[A] {
  val `HOL.equal`: (A, A) => Boolean
}
def equal[A](a: A, b: A)(implicit A: equal[A]): Boolean = A.`HOL.equal`(a, b)

def eq[A : equal](a: A, b: A): Boolean = equal[A](a, b)

} /* object HOL */

object Num {

abstract sealed class num
final case class One() extends num
final case class Bit0(a: num) extends num
final case class Bit1(a: num) extends num

} /* object Num */

object Code_Numeral {

def integer_of_nat(x0: Nat.nat): BigInt = x0 match {
  case Nat.Nata(x) => x
}

} /* object Code_Numeral */

object Nat {

abstract sealed class nat
final case class Nata(a: BigInt) extends nat

def one_nat: nat = Nata(BigInt(1))

def plus_nat(m: nat, n: nat): nat =
  Nata(Code_Numeral.integer_of_nat(m) + Code_Numeral.integer_of_nat(n))

def zero_nat: nat = Nata(BigInt(0))

} /* object Nat */

object cm2 {

def index[A : HOL.equal](y: A, x1: List[A]): Nat.nat = (y, x1) match {
  case (y, x :: xs) =>
    (if (HOL.eq[A](x, y)) Nat.zero_nat
      else Nat.plus_nat(Nat.one_nat, index[A](y, xs)))
}

def member[A : HOL.equal](e: A, x1: List[A]): Boolean = (e, x1) match {
  case (e, Nil) => false
  case (e, x :: xs) => (if (HOL.eq[A](e, x)) true else member[A](e, xs))
}

} /* object cm2 */
