theory cm1
imports Main 
begin

lemma "(p1 \<and> p2) \<longrightarrow> p1"
apply auto
done

lemma "(p1 \<and> p2) \<longrightarrow> p3"
nitpick
oops


(* 2) Propositional logic *)

lemma "A \<or> B"
nitpick
oops

lemma "((( A \<and> B) \<longrightarrow> ~C) \<or> ( A \<longrightarrow> B))\<longrightarrow> A \<longrightarrow> C"
nitpick
oops

lemma "( pluie \<longrightarrow> parapluie)\<longrightarrow>(~ parapluie \<longrightarrow> ~ pluie)"
apply auto
done

lemma "(A\<longrightarrow>B) = (~A\<or>B)"
apply auto
done
 
 
(* 3) First order logic *)
lemma "\<forall>x. p(x) \<longrightarrow>(\<exists>x. p(x))"
apply auto
done

lemma "(\<exists>x. p(x)) \<longrightarrow> (\<forall>x. p(x))"
nitpick [card=2]
oops

lemma "\<forall>x. ev(x) \<longrightarrow> od(s(x))"
nitpick
oops

lemma "\<forall>(x::nat) y. x > y \<longrightarrow> x + 1 > y + 1"
nitpick
apply auto
done

lemma "(x::nat) > y \<longrightarrow> x + 1 > y + 1"
apply auto
done

end
