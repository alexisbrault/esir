theory cm3
imports Main Int "~~/src/HOL/Library/Code_Target_Nat"  (* to hide 0, Succ rep. of nats *)
begin

fun member:: "'a => 'a list => bool"
where
"member e []     = False" |
"member e (x#xs) = (if e=x then True else (member e xs))"


value "member (1::nat) [2,3,1,4]"(* True *)

fun even:: "nat \<Rightarrow> bool" and
odd::"nat \<Rightarrow> bool"
where
"even 0=True" |
"even (Suc x)=odd x" |
"odd 0=False" |
"odd (Suc x)= even x"

value "even 10"(* True *)
value "even 11"(* False *)
value "odd 11"(* True *)


(* Example 2 *)

function member2::"'a \<Rightarrow> 'a list \<Rightarrow> bool"
where
"member2 e []     = False" |
"member2 e (x#xs) = (if e=x then True else (member2 e xs))"
apply pat_completeness
apply auto
done
termination member2
apply (relation "measure (%(x,y). (length y))")
apply auto
done


(* Exercice 1 *)
fun f::"nat \<Rightarrow> nat"
where
"f 0= 56" |
"f x=f (x - 1)"


fun f2::"int \<Rightarrow> int"
where
"f2 x = (if x<=0 then 0 else f2 (x - 1))"


function f3::"nat \<Rightarrow> nat \<Rightarrow> nat"
where
"f3 x y= (if x\<ge>10 then 0 else f3 (x + 1) (y + 1))"
apply pat_completeness
apply auto
done
termination f3
apply (relation "measure (%(x,y). (10- x))")
apply auto
done


(* Exercice 2 *)
primrec sumList:: "nat list \<Rightarrow>nat"
where
"sumList [] = 0" |
"sumList (a#l) = a + sumList(l)"

value "sumList[1,3,5]" (* 9 *)

fun sumNat :: "nat \<Rightarrow> nat"
where "sumNat n = (if n=0 then 0 else (sumNat (n - 1)) +n)"

primrec sumNat2 :: "nat \<Rightarrow> nat"
where 
"sumNat2 0 = 0" |
"sumNat2 (Suc x) = (sumNat x) +(Suc x)"

value "sumNat 3" (* 6 *)

primrec makeList :: "nat \<Rightarrow> nat list"
where
  "makeList 0 = []" |
  "makeList (Suc n) = (Suc n) # makeList n"

value "makeList 4" (* [4, 3, 2, 1] *)

lemma "sumNat n = (sumList (makeList n))"
nitpick
sorry

(* Exercice 3 *)



(* Type abbreviations *)
type_synonym name="(string * string)"
type_synonym ('a,'b) pair="('a * 'b)"
type_synonym phoneBook= "(string,nat) map"

value "(''Leonard'',''Michalon'')::name" (* (''Leonard'', ''Michalon'') *)
value "(1,''toto'')::(nat,string) pair" (* (1, ''toto'') *)
value "EmptyMap::phoneBook" (* EmptyMap *)

end
