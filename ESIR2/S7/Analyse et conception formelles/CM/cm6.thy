theory cm6 
imports Main "~~/src/HOL/Library/Code_Target_Nat"  (* to hide 0, Succ rep. of nats *)
begin


(* Exercise 1 *)

(* Programs are sequences of assignements (x:=y) or reads ( read(x) )
   where x,y are variable names.

   In a program, when executing an assignement x:=y, an exception is
   raised if y is undefined (either by another assignement or by a
   read).

   The objective of the analyzer is to guarantee that the execution of
   a program will be safe, i.e. exception free.

   For instance:

   the program   read(x); y:= x                   is safe 
   the program   read(x); y:= x; z:= u            is not safe 
      (it will raise an exception during execution because u is undefined)

   the program   read(x); y:= x; read(u); z:= u   is safe
   the program   read(u); read(x); y:= x; z:= u   is safe
 *)


(* Statements *)
datatype statement= Read string | Aff string string

(* Programs are statement lists *)
type_synonym program= "statement list"

(* sample programs *)
definition "p1= [(Read ''x''),(Aff ''y'' ''x'')]"
definition "p2= [(Aff ''y'' ''x'')]"
definition "p3= [(Aff ''x'' ''x''),(Read ''x'')]"
definition "p4= [(Read ''x''),(Aff ''y'' ''x''),(Aff ''z'' ''y'')]"
definition "p5= [(Read ''x''),(Aff ''y'' ''x''),(Read ''z''),(Aff ''u'' ''z'')]" 


(* The type of environments *)
type_synonym symTable= "(string * int) list"

(* a sample environment st1= {x\<rightarrow>24, y \<rightarrow> 18} *)
definition "st1= [(''x'',(24::int)),(''y'',18)]"


(* The function returning the value associated to a variable in an environment *)
datatype 'a option= None | Some 'a

fun assoc:: "'a \<Rightarrow> ('a * 'b) list \<Rightarrow> 'b option"
where
"assoc _ []    = None" |
"assoc x ((y,z)#ys)= (if x=y then (Some z) else (assoc x ys))"


value "assoc ''x'' st1" (* cm6.option.Some 24 *)
value "assoc ''y'' st1" (* cm6.option.Some 18 *)
value "assoc ''z'' st1" (* cm6.option.None *)


(* The type of input channels (values read by the 'read' statements) *)
type_synonym inchan=  "int list"

(* The type of events *)
datatype event= ReadEvent | Assignement string int | Exception


(* Program state is a tuple: (symTable * inchan * event list) *)
type_synonym pgState= "(symTable * inchan * event list)"


(* Evaluators for statements *)
(* In an assignement v1:=v2 if v2 is undefined we add an "Exception" event in the event list *)


fun evalS:: "statement \<Rightarrow> pgState \<Rightarrow> pgState"
where
"evalS (Read v)    (st,(i#inch),evl) = ((v,i)#st,inch,ReadEvent#evl)" |
"evalS (Read _)    (st,[],evl) = (st,[],evl)" | 
"evalS (Aff v1 v2) (st,inch,evl) = 
  (case (assoc v2 st) of None \<Rightarrow> (st,inch,Exception#evl) |
                         Some(i) \<Rightarrow> ((v1,i)#st,inch,(Assignement v1 i)#evl))"


(* Evaluator for programs *)
fun evalP:: "program \<Rightarrow> pgState \<Rightarrow> pgState"
where
"evalP [] (st,inch,evl)         = (st,inch,evl)" |
"evalP ((Read _)#_) (st,[],evl) = (st,[],evl)" |
"evalP (s#xs) (st,inch,evl)     = (evalP xs (evalS s (st,inch,evl)))"

(* Recall that:
definition "p1= [(Read ''x''),(Aff ''y'' ''x'')]"
definition "p2= [(Aff ''y'' ''x'')]"
definition "p3= [(Aff ''x'' ''x''),(Read ''x'')]"
definition "p4= [(Read ''x''),(Aff ''y'' ''x''),(Aff ''z'' ''y'')]"
definition "p5= [(Read ''x''),(Aff ''y'' ''x''),(Read ''z''),(Aff ''u'' ''z'')]" 
*)

value "evalP p1 ([],[10],[])" (* ([(''y'', 10), (''x'', 10)], [], [Assignement ''y'' 10, ReadEvent]) *)
value "evalP p2 ([],[10],[])" (* ([], [10], [Exception]) *)
value "evalP p3 ([],[10],[])" (* ([(''x'', 10)], [], [ReadEvent, Exception]) *)
value "evalP p3 ([],[10],[])" (* ([(''x'', 10)], [], [ReadEvent, Exception]) *)
value "evalP p4 ([],[10],[])" (* ([(''z'', 10), (''y'', 10), (''x'', 10)], [], [Assignement ''z'' 10, Assignement ''y'' 10, ReadEvent]) *)
value "evalP p5 ([],[10,11],[])" (* ([(''u'', 11), (''z'', 11), (''y'', 10), (''x'', 10)], [], [Assignement ''u'' 11, ReadEvent, Assignement ''y'' 10, ReadEvent]) *)
value "evalP p5 ([],[10],[])" (* ([(''y'', 10), (''x'', 10)], [], [Assignement ''y'' 10, ReadEvent]) *)

(* Exercise 1: We want to define a static analyzer guaranteeing that the execution of the program will be exception-free

   fun san:: "program \<Rightarrow> bool"
*)

fun san1:: "program \<Rightarrow> bool"
where
"san1 _ = False"

fun san2:: "program \<Rightarrow> bool"
where
"san2 [] = True" |
"san2 ((Read _)#r) = san2 r" |
"san2 _ = False"

fun san3aux:: "program \<Rightarrow> string list \<Rightarrow> bool"
where
"san3aux [] s = True" |
"san3aux ((Read x)#r) s = san3aux r (x#s)" |
"san3aux ((Aff x y )#r) s = (if (List.member s y) then (san3aux r (x#s)) else False)"

fun san3::" program \<Rightarrow> bool"
where
"san3 p = san3aux p []"

value "san3 p1" (* True *)
value "san3 p2" (* False *)


(* Exercise 2: define the BAD predicate 

   fun BAD::"pgState \<Rightarrow> bool"
*)

fun BAD::"pgState \<Rightarrow> bool"
where
"BAD (_,_,l) = List.member l Exception"

(* Exercise 3: define the correctness lemma for the static analyzer san *)
lemma thCorrection: "san3 prog \<longrightarrow> (\<forall> input. \<not> BAD (evalP prog ([],input,[])))"
nitpick
sorry

(* Example 1 *)

lemma "((x::nat)+4)*(y+5) >= x*y"
using [[simp_trace=true]]
by (metis le_add1 mult_le_mono)

(* Example 2 *)

fun memb::"'a => 'a list => bool"
where 
"memb _ [] = False" |
"memb e (x # xs) = (if (x=e) then True else (memb e xs))"


fun intersection:: "'a list => 'a list => 'a list"
where
"intersection [] l  = []" |
"intersection (e1#l1) l2 = (if (memb e1 l2) then (e1#(intersection l1 l2)) else (intersection l1 l2))"

theorem interMemb: "((memb e l1) \<and> (memb e l2)) \<longrightarrow> (memb e (intersection l1 l2))"
apply (induct l1)
apply auto
done

(* Exercise 4 *)

fun reverse:: "'a list \<Rightarrow> 'a list"
where
"reverse []=[]" |
"reverse (x#xs)= (reverse xs)@[x]"

lemma app_rev:"reverse (l1@l2)=(reverse l2)@(reverse l1)"
apply (induct l1)
apply auto
done

lemma rev_rev:"reverse (reverse l)=l"
apply (induct l)
apply auto
by (metis Cons_eq_appendI app_rev reverse.simps(2) self_append_conv2)


fun fastReverseAux:: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"
where
"fastReverseAux [] y = y" |
"fastReverseAux (x#xs) y= fastReverseAux xs (x#y)"

fun fastReverse:: "'a list \<Rightarrow> 'a list"
where
"fastReverse l= fastReverseAux l []"

value "fastReverse [1,2,3]" (* [1 + 1 + 1, 1 + 1, 1] *)


lemma fast1: "\<forall> l2. (fastReverseAux l1 l2) = (reverse l1 @ l2)"
apply (induct l1)
apply auto
done

lemma "fastReverse (l1@l2)= (fastReverse l2)@(fastReverse l1)"
by (metis app_rev append_Nil2 fast1 fastReverse.simps)


lemma "fastReverse (fastReverse l)=l"
by (metis append_Nil2 fast1 fastReverse.simps rev_rev)



(* Exercise 5 *)
fun power:: "nat \<Rightarrow> nat \<Rightarrow> nat"
where
"power _ 0= 1" |
"power x (Suc 0)= x" |
"power x (Suc y)= x*(power x y)"

value "power 3 2" (* 9 *)

sledgehammer_params [provers= e z3 spass remote_vampire cvc3 remote_iprover]

lemma powerOne: "power 1 x= 1"
apply (induct x)
apply auto
by (metis Suc_eq_plus1 add_eq_if cm6.power.simps(2) cm6.power.simps(3) nat.exhaust nat_mult_1)

lemma powerSuc: "(power x (Suc y))= x * (power x y)"
apply (induct y)
apply auto
done

lemma powerAdd: "(power x y) * (power x z)= (power x (y + z))"
apply (induct y)
apply auto
by (metis comm_semiring_1_class.normalizing_semiring_rules(17) powerSuc)

lemma powerMult: "power (x * y) z= (power x z) * (power y z)"
apply (induct z)
apply auto
by (metis mult.assoc mult.commute powerSuc)

lemma powerPower: "(power (power x y) z)=(power x (y * z))"
apply (induct y)
apply auto
apply (metis One_nat_def powerOne)
by (metis powerAdd powerMult powerSuc)

lemma powerDiv2: "(y mod 2=1) --> x*(power (power x (y div 2)) 2) = power x y"
apply (induct y)
apply auto
apply (metis mod_div_equality mult.commute powerAdd powerPower cm6.power.simps(2))
by (metis add.commute mod_div_equality powerAdd powerPower cm6.power.simps(2))

value "(1::nat) mod 2" (* 1 *)
value "(1::nat) div 2" (* 0 *)
value "(3::nat) mod 2" (* 1 *)
value "(3::nat) div 2" (* 1 *)

function fastPower:: "nat \<Rightarrow> nat \<Rightarrow> nat"
where
"fastPower _ 0=1" |
"fastPower x (Suc 0)= x" |
"fastPower x (Suc (Suc 0))= x*x" |
"fastPower x (Suc (Suc (Suc y)))= (if ((Suc (Suc (Suc y))) mod 2)=0 
  then (fastPower (fastPower x ((Suc (Suc (Suc y))) div 2)) 2)
  else x*(fastPower (fastPower x ((Suc (Suc (Suc y))) div 2)) 2))"
apply pat_completeness
apply auto
done
termination fastPower
apply (relation "measure (%(x,y). y)")
apply auto
done


value "fastPower 2 10" (* 1024 *)

(* power 2 5      = 2 * (power 2 4) = 2 * (2 * power 2 3) = etc...  d'où 5 appels récursifs à power *)

(* fastPower 2 5  = 2 * (fastPower (fastPower 2 2) 2) = 2 * (4 * 4) d'où 2 apprels récursifs à fastPower *)



lemma equivPower: "power x y= fastPower x y"
apply (induct x y rule:fastPower.induct)
apply auto
apply (metis comm_semiring_1_class.normalizing_semiring_rules(13) comm_semiring_1_class.normalizing_semiring_rules(18) fastPower.simps(3) mult_2 numeral_2_eq_2 powerAdd powerSuc)
by (metis One_nat_def Suc_eq_plus1 add_Suc_right cm6.power.simps(2) distrib_right mod_div_equality mult.commute mult_2_right numeral_2_eq_2 powerAdd)

(* Prove the classical properties of exponentiation on fastPower *)


lemma fastPowerAdd: "(fastPower x y) * (fastPower x z)= (fastPower x (y + z))"
by (metis equivPower powerAdd)


lemma fastPowerMult: "fastPower (x * y) z= (fastPower x z) * (fastPower y z)"
by (metis equivPower powerMult)

lemma fastPowerPowerFast: "(fastPower (fastPower x y) z)=(fastPower x (y * z))"
by (metis equivPower powerPower)


(* Exercice 6*)


fun member::"'a => 'a list => bool"
where 
"member _ [] = False" |
"member e (x # xs) = (if (x=e) then True else (member e xs))"


fun inter:: "'a list => 'a list => 'a list"
where
"inter [] l = l" |
"inter (e1#l1) l2 = (if (member e1 l2) then e1#(inter l1 l2) else (inter l1 l2))"


lemma interMemb2: "((member e l1) \<and> (member e l2)) = (member e (inter l1 l2))"
nitpick
oops

(* Exercice 7 *)

fun nbOccur::"'a \<Rightarrow> 'a list \<Rightarrow> nat"
where
"nbOccur x [] = 0" |
"nbOccur x (y#ys) = (if x=y then 1+(nbOccur x ys) else (nbOccur x ys))"

fun clean::"'a list => 'a list"
where
"clean []= []"|
"clean (e#l)= (if (member e l) then l else (e#(clean l)))"

lemma clean_prop: "clean [x,y,x]= [y,x]"
nitpick
oops

lemma clean_prop2: "nbOccur e (clean l)\<le> 1"
nitpick
oops

value "clean [1,1,1]" (* [1, 1] *)
end
