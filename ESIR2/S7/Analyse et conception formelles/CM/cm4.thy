theory cm4
imports Main "~~/src/HOL/Library/Code_Target_Nat"  (* to hide 0, Succ rep. of nats *)
begin


(* Exercise 2.1 *)

lemma "rev l = l"
nitpick [show_all]
oops

(* Exercise 2.2 *)

fun nth:: "nat \<Rightarrow> 'a list \<Rightarrow> 'a"
where
"nth 0 (x#_)=x" |
"nth x (y#ys)= (nth (x - 1) ys)"

fun index:: "'a \<Rightarrow> 'a list \<Rightarrow> nat"
where
"index x (y#ys)= (if x=y then 1 else 1+(index x ys))"


lemma nth_index: "nth (index e l) l= e"
nitpick
oops

(* Exercise 2.4 *)
lemma append_commut: "([x,y,z] @ [z,y,x]) = ([z,y,x] @ [x,y,z])"
nitpick [card=0-100]
oops


fun cutFirst:: "'a list \<Rightarrow> nat \<Rightarrow> 'a list"
where
"cutFirst [] _= []" |
"cutFirst (x#_) (Suc 0)= [x]" |
"cutFirst (x#xs) y = x#(cutFirst xs (y - 1))"

fun cutEnd:: "'a list \<Rightarrow> nat \<Rightarrow> 'a list"
where
"cutEnd [] _ = []" |
"cutEnd (x#xs) (Suc 0)= xs" |
"cutEnd (x#xs) y = (cutEnd xs (y - 1))"

lemma cutEnd_decreasing: "l\<noteq>[] \<Longrightarrow> length(cutEnd l i) < (length l)"
sorry


function slice:: "'a list \<Rightarrow> ('a list) list"
where
"slice l1= 
  (if l1=[] then [] else 
      (let deb=(cutFirst l1 19) in
        let fin=(cutEnd l1 19) in
            (deb#(slice fin))))"
by pat_completeness auto
termination slice
apply (relation "measure (%i. (length i))")
apply auto
by (metis cutEnd_decreasing)

(* Exercise 3 *)
(* Counterexample is of size 20 *)      

lemma length_slice: "length(slice l)<=1"
quickcheck [tester=narrowing,size=25]
oops

(* Remark 2: Be careful with size/timeout with quickcheck !!!*)

lemma "List.rev l = l"
quickcheck      
quickcheck [tester=narrowing,size=1000000,timeout=2]  
oops

(* fails in finding the counter example: the 2s are spent in building all possible input data of size 1 to 1000000, 
   and NOT in testing the values of size 1, first then generate for size 2, etc... *) 


(* Example 2 *)

lemma "A"
apply (case_tac "F::bool")
oops

(* exercice 4 *)

lemma "(x::nat) < 4 \<longrightarrow> (x*x)<10"
apply auto
apply (case_tac "x=0")
apply auto
apply (case_tac "x=1")
apply auto
apply (case_tac "x=2")
apply auto
apply (case_tac "x=3")
apply auto
done


(* Example 3 *)

datatype color= Black | White | Grey 
fun notBlack:: "color \<Rightarrow> bool"
where
"notBlack Black= False" |
"notBlack _ = True"

lemma "P(x::color)"
apply (case_tac "x")
oops


(* Exercice 5 *)

lemma "notBlack x \<longrightarrow> (x=White \<or> x=Grey)"
apply (case_tac "x")
apply auto
done

(* Example 5 *)

fun append :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list"
where
"append [] l2 = l2" |
"append (x#xs) l2 = (x#(append xs l2))"


lemma "append l [] = l"
apply (induct l)
apply auto
done 

(* Example 6 *)


lemma "length (append l1 l2) \<ge> (length l2)"
apply (induct l2)
apply auto
sorry

lemma "length (append l1 l2) \<ge> (length l2)"
apply (induct l1)
apply auto
done


(* Exercise 6 *)

datatype 'a binTree= Leaf | Node 'a "'a binTree" "'a binTree"

fun numNodes:: "'a binTree \<Rightarrow> nat"
where
"numNodes Leaf=0" |
"numNodes (Node _ tg td)= 1+(numNodes tg)+(numNodes td)"

definition "t1= Node (1::nat) (Node 2 Leaf Leaf) (Node 3 Leaf Leaf)"

value "numNodes t1"

fun member:: "'a \<Rightarrow> 'a binTree \<Rightarrow> bool"
where
"member _ Leaf = False" |
"member x (Node y tg td)= (if x=y then True else ((member x tg) \<or> (member x td)))"

fun subTree:: "'a binTree \<Rightarrow> 'a binTree \<Rightarrow> bool"
where
"subTree Leaf Leaf = True" |
"subTree _ Leaf=False" |
"subTree x (Node y tg td)= (if x=(Node y tg td) then True else ((subTree x tg) \<or> (subTree x td)))"


value "subTree (Node 1 Leaf Leaf) (Node 2 (Node 1 Leaf Leaf) Leaf)" (* True *)

lemma "(member x t) \<longrightarrow> (numNodes t > 0)"
apply (induct t)
apply auto
done

lemma "subTree t3 t2 \<longrightarrow> (numNodes t3) \<le> (numNodes t2)"
nitpick
apply (induct t2)
apply auto
by (metis numNodes.cases numNodes.simps(1) subTree.simps(2))

(* Exercise 7 *)
fun sumList:: "nat list \<Rightarrow> nat"
where
"sumList []=0" |
"sumList (x#xs)= x+(sumList xs)"

value "sumList [1,2,3,4,5]" (* 15 *)

fun sumNat:: "nat \<Rightarrow> nat"
where
"sumNat 0= 0" |
"sumNat i= (i+(sumNat (i - 1)))"

value "sumNat 5" (* 15 *)

fun makeList:: "nat \<Rightarrow> nat list"
where
"makeList 0 =[]" |
"makeList i = (i#(makeList (i - 1)))"

value "makeList 30" (* [30, 29, 28, 27, 26...1] *)

lemma length_makeList: "length(makeList i)=i"   
apply (induct i)
apply auto
done

(* Example 7 and 8 *)
lemma ex:"P (x::nat) (y::nat)"
apply (induct x arbitrary: y)
oops

(* Exercice 8 *)

fun leq::"nat => nat => bool"   (infix "=<" 65)
where 
"leq 0 _ = True" |
"leq (Suc _) 0 = False" |
"leq (Suc x) (Suc y) = leq x y"

(* à laisser en exercice *)  
lemma sym: "((x=<y) \<and> (y=< x)) \<longrightarrow> (x=y)"
apply (induct x)  
prefer 2  (* choose the second subgoal as the first *)

(* The induction hypothesis is for a "fixed" y *)
(* we won't manage to prove it *)
sorry


lemma symb: "((x=<y) \<and> (y=< x)) \<longrightarrow> (x=y)"
apply (induct x arbitrary:y)  
prefer 2 
(* The induction hypothesis is for "any" y *)
apply auto
apply (case_tac y)
apply auto
apply (case_tac y)
apply auto
done

  

(* Exercice 9 *)

fun div2 :: "nat \<Rightarrow> nat" 
where
"div2 0 = 0" |
"div2 (Suc 0) = 0" |
"div2 (Suc(Suc n)) = Suc(div2 n)"

lemma "div2 n = n div 2"
sorry

(* Example 9 *)

lemma "(index (1::nat) [3,4,1,3])=2"
using [[simp_trace=true]]
apply auto
oops

(* Exercise 10 *)
lemma "List.member l e \<longrightarrow> nth (index2 e l) l= e"
oops

(* Exercise 11 *)


fun noDouble:: "'a list \<Rightarrow> bool"
where
"noDouble []=True" |
"noDouble (x#xs)=(if (List.member xs x) then False else (noDouble xs))"


(* Example 10 *)

lemma falseLemma: "A"
oops  (* oops et pas sorry, sinon la preuve du dessous se fait, alors que le lemme est incorrect *)

lemma "(1::nat)+1=0"
apply (insert falseLemma)
apply auto
done

end