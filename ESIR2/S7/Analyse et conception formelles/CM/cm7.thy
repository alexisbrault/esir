theory cm7
imports Main 
begin


(* -------- Model-checking explained in Isabelle/HOL ----------- *)
(* States of the automaton *)
datatype etat= Init | LuA | LuB | Final

(* Letter to type on the digicode *)
datatype lettre= A | B | C | D 

fun transition:: "lettre*etat \<Rightarrow> etat"
where
"transition(A,Init) = LuA" |
"transition(_,Init) = Init" |
"transition(B,LuA) = LuB" |
"transition(A,LuA) = LuA" |
"transition(_,LuA) = Init" |
"transition(C,LuB) = Final" |
"transition(A,LuB) = LuA" |
"transition(_,LuB) = Init" |
"transition(A,Final) = LuA" |
"transition(_,Final) = Init"


fun execution:: "lettre list * etat \<Rightarrow> etat"
where
"execution([],e) = e" |
"execution((x#xs),e) = execution(xs,transition(x,e))"


value "execution([A,B],Init)"
value "execution([A,B,C],Init)"


(* Exercise 1 *)
(* 1) Whatever the state e we start from, after typing  [A,B,C] we reach the Final State *)

lemma zeroLetter: "execution([A,B,C],x) = Final"
apply (case_tac x)
apply auto
done


(* 1) can be generalized to any sequence of letters if it ends by [A,B,C] *)

theorem execAppend: "\<forall> e. execution(l1@l2,e)= execution(l2,execution(l1,e))"
apply (induct l1)
apply auto
done

theorem "execution(l@[A,B,C],e)=Final"
by (metis execAppend zeroLetter)

(* Exercise 2 *)
(* 2) Whatever the state e we start from, the only way to reach the Final State is to type 
  [A,B,C] *)

lemma intermediate: "execution([x,y,z],s) = Final \<longrightarrow> x=A\<and>y=B\<and>z=C"
apply (case_tac s)
apply (case_tac [1-] x)
apply (case_tac [1-] y)
apply (case_tac [1-] z)
apply auto
done

theorem Letter: "execution(l@[x,y,z],Init) = Final \<longrightarrow> x=A\<and>y=B\<and>z=C"
by (metis execAppend intermediate)


(* ----- Static analyzer explained in Isabelle/HOL -------*)

(* Exercice 3 *)

(* We have D=int and we choose the following abstract domain: D# *)

datatype absInt= Neg | Zero | Pos | Undef | Any   

(*       Any
       /  |  \ 
     Neg Zero Pos
       \  |  /
        Undef   *)

fun infeq:: "absInt \<Rightarrow> absInt \<Rightarrow> bool" (infix "<#" 65)  
where
"infeq Undef _   = True"|
"infeq _     Any = True"|
"infeq x     y   = (x=y)"


fun abs:: "int \<Rightarrow> absInt"
where
"abs x = (if(x<0) then Neg else (if (x=0) then Zero else Pos)) "

(* Exercice 4 *)

fun absPlus:: "absInt \<Rightarrow> absInt \<Rightarrow> absInt" (infix "+#" 65)  
where
"absPlus Undef _ = Undef  " |
"absPlus _ Undef = Undef" |
"absPlus Any _ = Any" |
"absPlus _ Any = Any" |
"absPlus Zero Zero = Zero"|
"absPlus Pos Neg = Any"|
"absPlus Neg Pos = Any"|
"absPlus Pos _ = Pos"|
"absPlus Neg _ = Neg"|
"absPlus _ Pos = Pos"|
"absPlus _ Neg = Neg"

(* Exercice 5 *)

lemma abs_correct: "\<forall> x y.  abs(x+y) <#  (abs(x) +# abs(y)) "
apply (case_tac "(abs x)")
apply (case_tac [1-] "(abs y)")
apply auto
done

end