theory cm2
imports Main Int Rat  "~~/src/HOL/Library/Code_Target_Nat"  (* to hide 0, Succ rep. of nats *)
begin


(* Terms and types *)

value "True"
value "1"
value "2" (* 1+1 *)
value "2::nat" (* 2 *)
value "[True,False]" (* bool list *)
value "[2,6,10]" (* 1+1+.... *)
value "[(2::nat),6,10]" (* [2,6,10] *)
value "[x,y,z]" (* 'a list *)
value "(x,y,z)" (* 'a x 'b x 'c *)
value "(x,x,x)" (* 'a x 'a x 'a *)
value "append" (* 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list *)
value "\<lambda>x. x" (* 'a \<Rightarrow> 'a *)
value "\<lambda>(x,y). y" (* 'a x 'b \<Rightarrow> 'a *)
value "f x y" (* 'c *)



(* Exercise 1 *)

value "append [True] [False,True,~False]" (* [T,F,T,T]  *)
value "append [1::nat,2,3] [4,5,6]" (* [1, 2, 3, 4, 5, 6] *)
value "append [1::nat,2,3] (append [2,3,4] [])"(* [1, 2, 3, 2, 3, 4] *)

(* Exercice 2 *)

definition "addNc = (\<lambda>(a::nat, b). a + b)" 
value "addNc (5, 6)" (* 11 *)

definition "add = (\<lambda>(a::nat) b. a + b)"
value "add 5 6"(* 11 *)

definition "incr = (add 1)"

value "incr 124"(* 125 *)

definition "app1= append [1::nat]"
 
value "app1 [576,789]"(* [1, 576, 789] *)

(* Exercise 3 *)

definition "triple= (\<lambda> f x. f (f (f x)))"

value "triple incr 0" (* 3 *)

value "triple app1 [2]" (* [1, 1, 1, 2] *)

value "map" (* ('a \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b list *)

value "map incr [1,2,3,4]" (* [2, 3, 4, 5 *)

(* Exercise 4 *)

(* Every data has a (hidden) constructor representation *)

lemma "3 = Suc(Suc(Suc(0)))"
apply auto
done

lemma "[1,1,1] = ( Cons( Suc 0) ( Cons( Suc 0) ( Cons (Suc 0) Nil) ) )"
apply auto
done

lemma "( [1,2] = (Cons x y) \<longrightarrow> x = (Suc 0)) "
apply auto
done


value "(-65.76::rat) + 56" (* - (244 / 25) *)

(* Integers are represented by couples of nat (x,y), where the integer 
   value is x-y *)

lemma "(Abs_Integ(1,4) + Abs_Integ(3,0)) = 0"
by (smt plus_int.abs_eq nat.abs_eq split_conv)     (* found by hand :/ *)

(* Exercise 4 *)

value "rev [1::nat, 2, 3]" (* [3, 2, 1] *)
value "rev [x, y, z]" (* [z, y, x] *)

lemma "[x] = (rev [x])"
apply auto
done

lemma "(append [1,2] (append [3] [4])) = (append(append [1,2] [3]) [4])"
apply auto
done

lemma "(append x (append y z)) = (append(append x y) z)"
apply auto
done

lemma "(append x y) = (append y x)"
nitpick
oops

(* Example 14 *)

fun member:: "'a => 'a list => bool"
where
"member e []     = False" |
"member e (x#xs) = (if e=x then True else (member e xs))"

thm "member.simps"

find_theorems  "append" "_ + _"

fun member3:: "'a => 'a list => bool"
where
"member3 e []     = False" |
"member3 e (x#[]) = (if e=x then True else (member e []))"


fun member2:: "'a => 'a list => bool"
where
"member2 e []     = False" |
"member2 e (x#xs) = (e=x \<or> (member2 e xs))"

(* Exercise 7 *)


lemma "member (2::nat) [1,2,3] = True"
apply (subst member.simps)
apply (simp del:member.simps)
apply (subst member.simps)
apply (simp del:member.simps)
done

lemma "member y [x,y,z]"
apply (subst member.simps)
apply (subst member.simps)
apply simp
done

lemma "member u (append [u] v)"
apply (subst append.simps)
apply (subst member.simps)
apply simp
done

(* Exercise 8 *)
lemma "member v (append u [v])"
apply (subst append.simps)
apply (subst member.simps)
sorry


(* Example 17 *)
(* A function that is not sufficiently defined, i.e. incomplete or not total *)

fun secondElt:: "'a list \<Rightarrow> 'a"
where
"secondElt (x#(y#ys)) = y"


(* Exercise 6 *)

(*
fun nth :: "'a list => nat => 'a" 
where
"nth (x # xs) n = (case n of 0 \<Rightarrow> x | Suc k \<Rightarrow> nth xs  k)"
*)


lemma "member (2::nat) [1,2,3] = True"
apply (subst member.simps)
apply (simp del:member.simps)
apply (subst member.simps)
apply (simp del:member.simps)
done


lemma "member y [x,y,z]"
apply (subst member.simps)
apply (subst member.simps)
apply (simp del:member.simps)
done

(* Exercise 9 *)

fun index:: "'a \<Rightarrow> 'a list => nat"
where
"index y (x#xs) = (if x=y then 0 else 1+(index y xs))"


value "index (4::nat) [1,2,3,4,5,6]" (* 3 *)
value "index (3::nat) [1,2,4]" (* Code_Target_Nat.Nat (1 + (1 + (1 + of_nat (index 3 [])))) *)

export_code member index in Scala file "test.scala"



end
